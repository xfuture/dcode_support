process.env.DISABLE_NOTIFIER = true; // Disables elixir notifications
var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

// Set elixir asset path to correct location
elixir.config.assetsDir = 'resources/assets/';

var paths = {
    fontAwesome: 'resources/assets/bower_components/font-awesome/',
    foundation: 'resources/assets/bower_components/foundation-sites/',
    jquery: 'resources/assets/bower_components/jquery/dist/',
    select2: 'resources/assets/bower_components/dist/js/',
    whatInput: 'resources/assets/bower_components/what-input/',
    dropzone: 'resources/assets/bower_components/dropzone/',
    moment:'resources/assets/bower_components/moment/'
}

elixir(function(mix) {
    mix
        .copy(paths.fontAwesome + "fonts/**", "public/fonts")
        
        .sass([
                'app.scss'
            ], 'public/css/app.css',
            {
                includePaths: [
                    paths.fontAwesome + 'scss/',
                    paths.foundation + 'scss/',
                    'resources/assets/sass/'
                ]
            })

        .sass([
                'admin.scss'
            ], 'public/css/admin.css',
            {
                includePaths: [
                    paths.fontAwesome + 'scss/',
                    paths.foundation + 'scss/',
                    'resources/assets/sass/'
                ]
            })
        
        // Compile scripts
        .scripts([
            paths.jquery + 'jquery.min.js'
        ], 'public/js/app-start.js', './')
        
        .scripts([
            paths.whatInput + 'what-input.js',
            paths.foundation + 'dist/foundation.js',
            paths.dropzone + 'dist/dropzone.js',
            // paths.foundation + 'js/foundation.offcanvas.js',
            // paths.motionUI + 'motion-ui.js',
            // paths.jqueryUITouch + 'jquery.ui.touch-punch.js',
            // paths.select2 + 'js/select2.full.min.js',
            paths.moment+'moment.js',

            './resources/assets/js/bootstrap/modal.js',
            './resources/assets/js/bootstrap/jquery-ui-timepicker.js',
            './resources/assets/js/app.js',
            './resources/assets/bower_components/moment/moment.js'
        ], 'public/js/app-stop.js', './')

        .version([
            "css/app.css",
            "css/admin.css",
            "js/app-start.js",
            "js/app-stop.js"
        ])
        //.task(elixir.config.assetsDir + 'sass/**/*.+(sass|scss)');
    // Fix watcher location
    // elixir.config.watchers.default.sass = elixir.config.assetsDir + 'scss/**/*.+(sass|scss)';
});
