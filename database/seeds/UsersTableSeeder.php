<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DCODESupport\User::create([
            'first_name' => 'Andrew',
            'last_name' => 'Sirianni',
            'email' => 'andrew@dcode.com.au',
            'password' => \Hash::make('Lick Mar'),
            'admin' => 1
                                   ]);
    }
}
