<?php

use Illuminate\Database\Seeder;

class ProjectInternalStatusesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('project_internal_statuses')->insert(['name' => 'Requested']);
        DB::table('project_internal_statuses')->insert(['name' => 'Quoting']);
        DB::table('project_internal_statuses')->insert(['name' => 'Awaiting Acceptance']);
        DB::table('project_internal_statuses')->insert(['name' => 'In Progress']);
        DB::table('project_internal_statuses')->insert(['name' => 'Complete']);
        DB::table('project_internal_statuses')->insert(['name' => 'Finalised']);
    }
}
