<?php

use Illuminate\Database\Seeder;

class NoteTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('note_types')->insert(['name' => 'Meeting']);
        DB::table('note_types')->insert(['name' => 'Phone Call']);
        DB::table('note_types')->insert(['name' => 'Email']);
    }
}
