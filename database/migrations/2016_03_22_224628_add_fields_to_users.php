<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->unsignedInteger('client_id')->nullable()->after('password');
            $table->foreign('client_id')->references('id')->on('clients');

            $table->tinyInteger('access')->after('client');
            $table->tinyInteger('manager')->after('access');

            $table->softDeletes()->after('remember_token');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropForeign('users_client_id_foreign');
            $table->dropColumn('client_id');

            $table->dropColumn('access')->after('client');
            $table->dropColumn('manager')->after('access');

            $table->dropSoftDeletes();
        });
    }
}
