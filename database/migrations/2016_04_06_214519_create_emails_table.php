<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('emails', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('to_user_id');
            $table->foreign('to_user_id')->references('id')->on('users');

            $table->unsignedInteger('emailable_id');
            $table->string('emailable_type');

            $table->string('to');
            $table->text('subject');
            $table->text('body');

            $table->string('sent_id');
            $table->tinyInteger('opened');

            $table->unsignedInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users');

            $table->softDeletes();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('emails');
    }
}
