<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

if (App::environment('production')) {
    URL::forceSchema('https');
}

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/
//Route::auth();

Auth::routes();

Route::get('/', ['uses' => 'HomeController@index']);

Route::get('logout', 'Auth\LoginController@logout');

// Account
Route::group(['middleware' => ['auth', 'roles'], 'roles' => ['access'], 'namespace' => 'Account', 'prefix' => 'account'], function () {
    Route::get('/', ['as' => 'account', 'uses' => 'AccountController@index']);
    Route::post('update', ['as' => 'account.update', 'uses' => 'AccountController@update']);
});

// Client
Route::group(['middleware' => ['auth', 'roles'], 'roles' => ['access'], 'namespace' => 'Client', 'prefix' => 'client', 'as' => 'client.'], function () {
    Route::get('/', ['as' => 'dashboard', 'uses' => 'ClientController@index']);

    // Projects
    Route::resource('project', 'ProjectController', [
        'names' => [
            'index' => 'project',
            'show' => 'project.show'
        ]
    ]);

    // Task
    Route::group(['prefix' => 'task'], function () {
        Route::get('/', ['as' => 'task', 'uses' => 'TaskController@index']);
    });

    // Ticket
    Route::post('ticket/comment/{ticket}', ['as' => 'ticket.comment', 'uses' => 'TicketController@comment']);
    Route::get('ticket/preview', ['as' => 'ticket.preview', 'uses' => 'TicketController@preview']);
    Route::post('ticket/status', ['as' => 'ticket.status', 'uses' => 'TicketController@status']);

    Route::resource('ticket', 'TicketController', [
        'names' => [
            'index' => 'ticket',
            'create' => 'ticket.create',
            'store' => 'ticket.store',
            'show' => 'ticket.show',
            'edit' => 'ticket.edit',
            'update' => 'ticket.update',
            'destroy' => 'ticket.destroy'
        ]
    ]);

    // Idea
    Route::get('ticket/preview', ['as' => 'ticket.preview', 'uses' => 'TicketController@preview']);
    Route::post('idea/comment/{idea}', ['as' => 'idea.comment', 'uses' => 'IdeaController@comment']);

    Route::resource('idea', 'IdeaController', [
        'names' => [
            'index' => 'idea',
            'create' => 'idea.create',
            'store' => 'idea.store',
            'show' => 'idea.show',
            'edit' => 'idea.edit',
            'update' => 'idea.update',
            'destroy' => 'idea.destroy'
        ]
    ]);

    //IdeaVote
    Route::post('ideavote', 'IdeaVoteController@vote')->name('ideavote.vote');
});

// Admin
Route::group(['middleware' => ['auth', 'roles'], 'roles' => ['access', 'internal'], 'namespace' => 'Admin', 'prefix' => 'admin', 'as' => 'admin.'], function () {
    Route::get('/', ['as' => 'dashboard', 'uses' => 'AdminController@index']);

    Route::group(['middleware' => ['auth', 'roles'], 'roles' => ['admin']], function () {
        // Client
        Route::resource('client', 'ClientController', [
            'names' => [
                'index' => 'client',
                'create' => 'client.create',
                'store' => 'client.store',
                'show' => 'client.show',
                'edit' => 'client.edit',
                'update' => 'client.update',
                'destroy' => 'client.destroy'
            ]
        ]);
    });

    // Projects
    Route::get('project/add/{client}', ['as' => 'project.add', 'uses' => 'ProjectMilestoneController@add']);

    Route::resource('project', 'ProjectController', [
        'names' => [
            'index' => 'project',
            'create' => 'project.create',
            'store' => 'project.store',
            'show' => 'project.show',
            'edit' => 'project.edit',
            'update' => 'project.update',
            'destroy' => 'project.destroy'
        ]
    ]);
    Route::get('project/access/{project}', ['as' => 'project.access', 'uses' => 'ProjectController@access']);
    Route::post('project/projectcode', ['as' => 'project.projectcode', 'uses' => 'ProjectController@genProjectCode']);
    Route::post('project/user/{project}', ['as' => 'project.user', 'uses' => 'ProjectController@user']);
    Route::delete('project/cancel-access/{project_user}', ['as' => 'project.cancel-access', 'uses' => 'ProjectController@cancelAccess']);

    // Project Milestones
    Route::get('project-milestone/{project}', ['as' => 'project-milestone', 'uses' => 'ProjectMilestoneController@index']);
    Route::get('project-milestone/add/{project}', ['as' => 'project-milestone.add', 'uses' => 'ProjectMilestoneController@add']);
    Route::post('project-milestone/save/{project}', ['as' => 'project-milestone.save', 'uses' => 'ProjectMilestoneController@save']);
    Route::resource('project-milestone', 'ProjectMilestoneController', [
        'names' => [
            'show' => 'project-milestone.show',
            'edit' => 'project-milestone.edit',
            'update' => 'project-milestone.update',
            'destroy' => 'project-milestone.destroy'
        ]
    ]);

    // Project Updates
    Route::get('project-update/{project}', ['as' => 'project-update', 'uses' => 'ProjectUpdateController@index']);
    Route::get('project-update/add/{project}', ['as' => 'project-update.add', 'uses' => 'ProjectUpdateController@add']);
    Route::post('project-update/save/{project}', ['as' => 'project-update.save', 'uses' => 'ProjectUpdateController@save']);
    Route::get('project-update/show/{project_update}', ['as' => 'project-update.show', 'uses' => 'ProjectUpdateController@show']);
    Route::post('project-update/comment/{project_update}', ['as' => 'project-update.comment', 'uses' => 'ProjectUpdateController@comment']);
    Route::resource('project-update', 'ProjectUpdateController', [
        'only'=>['edit', 'update', 'destroy', 'store']
    ]);

    // Task
    Route::group(['prefix' => 'task'], function () {
        Route::get('/', ['as' => 'task', 'uses' => 'TaskController@index']);
    });

    // Ticket
    Route::post('ticket/comment/{ticket}', ['as' => 'ticket.comment', 'uses' => 'TicketController@comment']);
    Route::get('ticket/preview', ['as' => 'ticket.preview', 'uses' => 'TicketController@preview']);
    Route::post('ticket/status', ['as' => 'ticket.status', 'uses' => 'TicketController@status']);

    Route::resource('ticket', 'TicketController', [
        'names' => [
            'index' => 'ticket',
            'create' => 'ticket.create',
            'store' => 'ticket.store',
            'show' => 'ticket.show',
            'edit' => 'ticket.edit',
            'update' => 'ticket.update',
            'destroy' => 'ticket.destroy'
        ]
    ]);

    Route::group(['middleware' => ['auth', 'roles'], 'roles' => ['admin']], function () {
        // Idea
        Route::post('idea/comment/{idea}', ['as' => 'idea.comment', 'uses' => 'IdeaController@comment']);

        Route::resource('idea', 'IdeaController', [
            'names' => [
                'index' => 'idea',
                'create' => 'idea.create',
                'store' => 'idea.store',
                'show' => 'idea.show',
                'edit' => 'idea.edit',
                'update' => 'idea.update',
                'destroy' => 'idea.destroy'
            ]
        ]);

        //IdeaVote
        Route::post('ideavote', 'IdeaVoteController@vote')->name('ideavote.vote');
        /*
        Route::resource('ideavote', 'IdeaVoteController', [
            'names' => [
                'store' => 'ideavote.store',
                'destroy' => 'ideavote.destroy'
            ]
        ]);
        */
    });

    // Timesheet
    Route::resource('timesheet', 'TimesheetController', [
        'names' => [
            'index' => 'timesheet',
            'store' => 'timesheet.store',
            'show' => 'timesheet.show',
            'edit' => 'timesheet.edit',
            'update' => 'timesheet.update',
            'destroy' => 'timesheet.destroy'
        ]
    ]);

    //File
    Route::resource('file', 'FileController', [
        'names' => [
            'index' => 'file',
            'store' => 'file.store',
            'show' => 'file.show',
            'edit' => 'file.edit',
            'update' => 'file.update',
            'destroy' => 'file.destroy'
        ]
    ]);

    Route::get('create/{ticket?}', ['as' => 'timesheet.create', 'uses' => 'TimesheetController@create']);

    Route::group(['middleware' => ['auth', 'roles'], 'roles' => ['admin']], function () {
        // User
        Route::resource('user', 'UserController', [
            'names' => [
                'index' => 'user',
                'create' => 'user.create',
                'store' => 'user.store',
                'show' => 'user.show',
                'edit' => 'user.edit',
                'update' => 'user.update',
                'destroy' => 'user.destroy'
            ]
        ]);
    });

    //Notes

    Route::get('note/create/{user}', ['as' => 'note.create', 'uses' => 'NoteController@create']);
    Route::post('note/store/{user}', ['as' => 'note.store', 'uses' => 'NoteController@store']);
    Route::get('note/edit/{note}', ['as' => 'note.edit', 'uses' => 'NoteController@edit']);
    Route::post('note/update/{note}', ['as' => 'note.update', 'uses' => 'NoteController@update']);
    Route::get('note/{user}', ['as' => 'note', 'uses' => 'NoteController@index']);
    Route::resource('note', 'NoteController', [
        'only'=>['destroy']
    ]);
});

//Webhook
Route::group(['prefix' => 'webhook', 'as' => 'webhook.'], function () {
    Route::any('bugsnag', 'WebhookController@bugsnag')->name('bugsnag');

});
