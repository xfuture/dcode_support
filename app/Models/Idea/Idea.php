<?php

namespace DCODESupport\Models\Idea;

use DCODESupport\User;
use Illuminate\Database\Eloquent\Model;

class Idea extends Model
{
	
	// Belongs to ...
	public function client()
	{
		return $this->belongsTo('DCODESupport\Models\Client\Client');
	}
	
	public function project()
	{
		return $this->belongsTo('DCODESupport\Models\Project\Project');
	}
	
	public function user()
	{
		return $this->belongsTo('DCODESupport\User');
	}
	
	// Has many ...
	public function ideaVotes()
	{
		return $this->hasMany('DCODESupport\Models\Idea\IdeaVote');
	}
	
	// Polymorphic ...
	public function comments()
	{
		return $this->morphMany('DCODESupport\Models\Comment\Comment', 'commentable');
	}
	
	// General ...
	public function access(User $user)
	{
		if ($user->admin == '1')
		{
			return true;
		}
		
		if ($this->user_id == $user->id)
		{
			// User owns the ticket ...
			return true;
		}
		
		if ($this->internal == '1' && $user->internal != '1')
		{
			// Only allow user to see internal tickets if they are internal ...
			return false;
		}
		
		if ($this->project != null && $user->access($this->project) == true)
		{
			// User has access to the project ...
			return true;
		}
		
		return false;
	}
	
	public function hasUserVoted()
	{
		$voted = $this->ideaVotes()
		              ->where('user_id', auth()->id())
		              ->count();
		
		if ($voted > 0)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
}
