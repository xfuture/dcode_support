<?php

namespace DCODESupport\Models\Ticket;

use DCODESupport\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Ticket extends Model
{
    use SoftDeletes;
	// Belongs to ...
	public function assignedUser()
	{
		return $this->belongsTo('DCODESupport\User', 'assigned_user_id', 'id');
	}
	
	public function client()
	{
		return $this->belongsTo('DCODESupport\Models\Client\Client');
	}
	
	public function project()
	{
		return $this->belongsTo('DCODESupport\Models\Project\Project');
	}
	
	public function user()
	{
		return $this->belongsTo('DCODESupport\User');
	}
	
	// Has many ...
	public function ticketStatuses()
	{
		return $this->hasMany('DCODESupport\Models\Ticket\TicketStatus');
	}
	
	public function files()
	{
		return $this->hasMany('DCODESupport\Models\File\File');
	}
	
	public function timesheets()
	{
		return $this->hasMany('DCODESupport\Models\Timesheet\Timesheet');
	}
	
	// Polymorphic ...
	public function comments()
	{
		return $this->morphMany('DCODESupport\Models\Comment\Comment', 'commentable');
	}
	
	// General ...
	public function access(User $user)
	{
		if ($user->admin == '1')
		{
			return true;
		}
		
		if ($this->user_id == $user->id)
		{
			// User owns the ticket ...
			return true;
		}
		
		if ($this->internal == '1' && $user->internal != '1')
		{
			// Only allow user to see internal tickets if they are internal ...
			return false;
		}
		
		if ($this->project != null && $user->access($this->project) == true)
		{
			// User has access to the project ...
			return true;
		}
		
		return false;
	}
	
	public function calculateTimeSpend()
	{
		return $this->timesheets()
		            ->sum('hours');
	}
	
	public function currentStatus()
	{
		$result = $this->ticketStatuses()
		               ->where('current', '1')
		               ->first();
	
		if ($result == null)
		{
		
		}
		else
		{
			return $result;
		}
		
	}
	
	public function workingTicket()
	{
		
		//(1)First enable this
		// \DB::enableQueryLog();
		//(2) Then a $variable or query
		$result = $this->timesheets()
		               ->whereNull('stop')
		               ->select('start')
		               ->first();
		//(3) then following three lines
		// dd(\DB::getQueryLog());
		
		if ($result != null)
		{
			return $result->start;
		}
		else
		{
			return null;
		}
		
	}
	
}
