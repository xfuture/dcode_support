<?php

namespace DCODESupport\Models\Ticket;

use Illuminate\Database\Eloquent\Model;

class TicketStatus extends Model
{
    protected $guarded = ['id'];

	// Belongs to ...
	public function status()
	{
		return $this->belongsTo('DCODESupport\Models\Status');
	}

	public function ticket()
	{
		return $this->belongsTo('DCODESupport\Models\Ticket\Ticket');
	}

	public function user()
	{
		return $this->belongsTo('DCODESupport\User');
	}
}
