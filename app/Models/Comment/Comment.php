<?php

namespace DCODESupport\Models\Comment;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
	// Polymorphic ...
	public function commentable()
	{
		return $this->morphTo();
	}

	// Belongs to ...
	public function user()
	{
		return $this->belongsTo('DCODESupport\User');
	}
}
