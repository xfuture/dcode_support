<?php

namespace DCODESupport\Models\Timesheet;

use DCODESupport\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Timesheet extends Model
{
	use SoftDeletes;

	// Belongs to ...
	public function project()
	{
		return $this->belongsTo('DCODESupport\Models\Project\Project')
            ->withTrashed();
	}

	public function user()
	{
		return $this->belongsTo('DCODESupport\User');
	}

	// General ...
	public function access(User $user)
	{
		if ($user->admin == '1')
		{
			return true;
		}

		if ($this->user_id == $user->id)
		{
			// User owns the timesheet ...
			return true;
		}

		return false;
	}
}
