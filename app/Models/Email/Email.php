<?php

namespace DCODESupport\Models\Email;

use Illuminate\Database\Eloquent\Model;

class Email extends Model
{
	// Polymorphic ...
	public function emailable()
	{
		return $this->morphTo();
	}

	// Belongs to ...
	public function user()
	{
		return $this->belongsTo('DCODESupport\User');
	}
}
