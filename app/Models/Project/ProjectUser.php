<?php

namespace DCODESupport\Models\Project;

use Illuminate\Database\Eloquent\Model;

class ProjectUser extends Model
{
	protected $guarded = ['id'];

    // Belongs to ...
	public function accessUser()
	{
		return $this->belongsTo('DCODESupport\User', 'access_user_id', 'id');
	}

	public function project()
	{
		return $this->belongsTo('DCODESupport\Models\Project\Project');
	}

	public function user()
	{
		return $this->belongsTo('DCODESupport\User');
	}
}
