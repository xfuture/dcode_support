<?php

namespace DCODESupport\Models\Project;

use Illuminate\Database\Eloquent\Model;

class ProjectMilestone extends Model
{
    // Belongs to ...
	public function project()
	{
		return $this->belongsTo('DCODESupport\Models\Project\Project');
	}

	public function user()
	{
		return $this->belongsTo('DCODESupport\User');
	}
}
