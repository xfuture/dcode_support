<?php

namespace DCODESupport\Models\Project;

use DCODESupport\Models\Ticket\Ticket;
use DCODESupport\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Project extends Model
{
	
	use SoftDeletes;
	
	// Belongs to ...
	public function client()
	{
		return $this->belongsTo('DCODESupport\Models\Client\Client');
	}
	
	public function user()
	{
		return $this->belongsTo('DCODESupport\User');
	}
	
	// Has one ...
	public function projectInternalStatus()
	{
		return $this->hasOne('DCODESupport\Models\Project\ProjectInternalStatus');
	}
	
	// Has many ...
	public function projectMilestones()
	{
		return $this->hasMany('DCODESupport\Models\Project\ProjectMilestone');
	}
	
	public function projectUpdates()
	{
		return $this->hasMany('DCODESupport\Models\Project\ProjectUpdate');
	}
	
	public function projectUsers()
	{
		return $this->hasMany('DCODESupport\Models\Project\ProjectUser');
	}
	
	public function tickets()
	{
		return $this->hasMany('DCODESupport\Models\Ticket\Ticket');
	}
	
	public function timesheets()
	{
		return $this->hasMany('DCODESupport\Models\Timesheet\Timesheet');
	}

    public function activeMilestones()
    {
        return $this->projectMilestones()
            ->whereNotIn('id', function ($query)
            {
                $query->select('project_id')
                    ->from('project_milestones')
                    ->whereNull('complete')
                    ->whereNull('deleted_at');
            });
    }

    //Join Project and project milestones
    //public function getProjectMilestones(){
	 //   return $this->projectMilestones();
    //}

	// General ...
	public function activeTickets()
	{
		return $this->projectMilestones()
		            ->whereNotIn('id', function ($query)
		            {
			            $query->select('project_id')
			                  ->from('project_milestones')
			                  ->whereNull('complete')
			                  ->whereNull('deleted_at');
		            });
	}
	
	public function clientUsers()
	{
		return User::where('client_id', $this->client->id)->get();
	}
	
	public function completedTickets()
	{
		return $this->tickets()
		            ->whereIn('tickets.id', function ($query)
		            {
			            $query->select('ticket_id')
			                  ->from('ticket_statuses')
			                  ->where('status_id', '2')
			                  ->where('current', '1')
			                  ->whereNull('deleted_at');
		            });
	}
	
	public function internalUsers()
	{
		return User::where('internal', '1')->get();
	}
	
	public function latestUpdate()
	{
		return $this->projectUpdates()
		            ->orderBy('created_at', 'desc')
		            ->first();
	}
	
	public function myTickets()
	{
		return $this->tickets()
		            ->where('assigned_user_id', \Auth::id());
	}
	
	public function nextMilestone()
	{
		return $this->projectMilestones()
		            ->whereNull('complete')
		            ->orderBy('scheduled', 'asc')
		            ->first();
	}
	
	public function outstandingTickets()
	{
		return $this->tickets()
		            ->whereIn('tickets.id', function ($query)
		            {
			            $query->select('ticket_id')
			                  ->from('ticket_statuses')
			                  ->where('status_id', '1')
			                  ->where('current', '1')
			                  ->whereNull('deleted_at');
		            });
	}
	
	public function outstandingTimesheetHours()
	{
		return $this->timesheets()
		            ->whereNotNull('stop')
		            ->where('billed', '0')
		            ->sum('hours');
	}
	
	public function pendingTickets()
	{
		return $this->tickets()
		            ->whereNotIn('tickets.id', function ($query)
		            {
			            $query->select('ticket_id')
			                  ->from('ticket_statuses')
			                  ->where('current', '1')
			                  ->whereNull('deleted_at');
		            });
	}
	
	
}
