<?php

namespace DCODESupport\Models\Project;

use Illuminate\Database\Eloquent\Model;

class ProjectUpdate extends Model
{
    // Belongs to ...
	public function project()
	{
		return $this->belongsTo('DCODESupport\Models\Project\Project');
	}

	public function projectStatus()
	{
		return $this->belongsTo('DCODESupport\Models\Project\ProjectStatus');
	}

	public function user()
	{
		return $this->belongsTo('DCODESupport\User');
	}

	// Polymorphic ...
	public function comments()
	{
		return $this->morphMany('DCODESupport\Models\Comment\Comment', 'commentable');
	}

}
