<?php

namespace DCODESupport\Models\File;

use Illuminate\Database\Eloquent\Model;

class File extends Model
{
    public function ticket()
    {
        return $this->belongsTo('DCODESupport\Models\Ticket\Ticket');
    }
}
