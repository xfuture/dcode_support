<?php

namespace DCODESupport\Models\Note;

use Illuminate\Database\Eloquent\Model;

class Note extends Model
{
    // Belongs to ...
    public function user()
    {
        return $this->belongsTo('DCODESupport\User', 'user_id', 'id');
    }

    // Has one ...
    public function ticket()
    {
        return $this->hasOne('DCODESupport\Models\Ticket\Ticket', 'id', 'ticket_id');
    }
}
