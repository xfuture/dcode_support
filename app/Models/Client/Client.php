<?php

namespace DCODESupport\Models\Client;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Client extends Model
{
    use SoftDeletes;

	// Belongs to ...
	public function user()
	{
		return $this->belongsTo('DCODESupport\User');
	}

	// Has many ...
	public function projects()
	{
		return $this->hasMany('DCODESupport\Models\Project\Project');
	}

    public function clientUsers()
    {
        return $this->hasMany('DCODESupport\User', 'client_id', 'id');
    }
}
