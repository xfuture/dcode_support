<?php

namespace DCODESupport;

use DCODESupport\Models\Client\Client;
use DCODESupport\Models\Note\NoteType;
use DCODESupport\Models\Project\Project;
use DCODESupport\Models\Project\ProjectInternalStatus;
use DCODESupport\Models\Project\ProjectStatus;
use DCODESupport\Models\Status;
use DCODESupport\Models\Ticket\Ticket;
use DCODESupport\Services\DisplayService;

use \Form;
use \Html;
use \Request;

// Form Macros (Models)

Form::macro('assignedUserSelect', function ($name, $selected = null, Project $project = null) {
    $data[] = '-- Select a User --';

    // Internal ...
    if (\Auth::user()->admin == '1') {
        $users = User::where('internal', '1')
            ->orderBy('last_name', 'asc')
            ->get();
        foreach ($users as $user) {
            $data[$user->id] = DisplayService::displayUser($user);
        }
    } else {
        $users = User::where('admin', '1')
            ->orderBy('last_name', 'asc')
            ->get();
        foreach ($users as $user) {
            $data[$user->id] = DisplayService::displayUser($user);
        }
    }

    if ($project != null) {
        // Project Users ...
        foreach ($project->projectUsers()->get() as $projectUser) {
            $data[$projectUser->accessUser->id] = DisplayService::displayUser($projectUser->accessUser);
        }
    }

    //Generate select element
    $selectOption = "";
    if (\Auth::user()->id == $selected) {
        $selectOption = "selected='selected'";
    }
    $select = "<select id='$name' name='$name'>";
    $select .= "<option value=\"0\">-- Select a User --</option>";
    $select .= "<option $selectOption style='font-weight : bold' value='" . \Auth::user()->id . "'>" . \Auth::user()->first_name . ' ' . \Auth::user()->last_name . " (Me)</option>";

    foreach ($users as $user) {
        if (\Auth::user()->id != $user->id) {
            if ($user->id == $selected) {
                $selectOption = "selected='selected'";
            } else {
                $selectOption = "";
            }

            $select .= "<option value='$user->id' $selectOption>" . DisplayService::displayUser($user) . "</option>";
        }
    }

    $select .= "</select>";

    return $select;

//	return Form::select($name, $data, $selected);
});

Form::macro('clientSelect', function ($name, $selected = null, $disable = null) {
    $data[] = '-- Select a Client --';

    if (\Auth::user()->admin == '1') {
        $clients = Client::orderBy('name', 'asc')->get();
    } else {
        $clients = Client::whereIn('id', function ($query) {
            $query->select('client_id')
                ->from('projects')
                ->whereIn('id', function ($query) {
                    $query->select('project_id')
                        ->from('project_users')
                        ->where('access_user_id', \Auth::user()->id)
                        ->whereNull('deleted_at');
                });

        })
            ->orderBy('name', 'asc')
            ->get();
    }

    foreach ($clients as $client) {
        $data[$client->id] = $client->name;
    }

    return Form::select($name, $data, $selected, ['class' => 'select2', empty($disable) ? '': 'disabled']);
});

Form::macro('projectSelect', function ($name, $selected = null) {
    $data[] = '-- Select a Project --';

    $projects = new Project();

    if (\Auth::user()->admin != '1') {
        $projects = $projects->whereIn('id', function ($query) {
            $query->select('project_id')
                ->from('project_users')
                ->where('access_user_id', \Auth::user()->id)
                ->whereNull('deleted_at');
        });
    }

    $projects = $projects->where('active', '1');

    $projects = $projects->orderBy('code', 'asc')
        ->get();

    foreach ($projects as $project) {
        $data[$project->id] = $project->code . ': ' . $project->name;
    }

    return Form::select($name, $data, $selected);
});

Form::macro('projectStatusSelect', function ($name, $selected = null) {
    $data[] = '-- Select a Project Status --';

    $projectStatuses = ProjectStatus::get();
    foreach ($projectStatuses as $projectStatus) {
        $data[$projectStatus->id] = $projectStatus->name;
    }

    return Form::select($name, $data, $selected);
});

Form::macro('noteTypeSelect', function ($name, $selected = null) {
    $data[] = '-- Select a Note Type --';

    $noteTypes = NoteType::get();
    foreach ($noteTypes as $noteType) {
        $data[$noteType->id] = $noteType->name;
    }

    return Form::select($name, $data, $selected);
});

Form::macro('projectInternalStatusSelect', function ($name, $selected = null) {
    $data[] = '-- Select an Internal Project Status --';

    $projectInternalStatuses = ProjectInternalStatus::get();
    foreach ($projectInternalStatuses as $projectInternalStatus) {
        $data[$projectInternalStatus->id] = $projectInternalStatus->name;
    }

    return Form::select($name, $data, $selected);
});

Form::macro('statusSelect', function ($name, $selected = null) {
    $data[] = '-- Select a Status --';

    $statuses = Status::orderBy('order', 'asc')->get();
    foreach ($statuses as $status) {
        $data[$status->id] = $status->name;
    }

    return Form::select($name, $data, $selected);
});

Form::macro('userSelect', function ($name, $selected = null) {
    $data[] = '-- Select a User --';

    if (\Auth::user()->admin == '1') {
        $users = User::orderBy('last_name', 'asc')->get();
    } else {
        $users = User::where('internal', '1')
            ->orderBy('last_name', 'asc')->get();
    }

    //Generate select element
    $selectOption = "";
    if (\Auth::user()->id == $selected) {
        $selectOption = "selected='selected'";
    }
    $select = "<select id='$name' name='$name'>";
    $select .= "<option value=\"0\">-- Select a User --</option>";
    $select .= "<option $selectOption style='font-weight : bold' value='" . \Auth::user()->id . "'>" . \Auth::user()->first_name . ' ' . \Auth::user()->last_name . " (Me)</option>";

    foreach ($users as $user) {
        if (\Auth::user()->id != $user->id) {
            if ($user->id == $selected) {
                $selectOption = "selected='selected'";
            } else {
                $selectOption = "";
            }

            $select .= "<option value='$user->id' $selectOption>" . DisplayService::displayUser($user) . "</option>";
        }
    }

    $select .= "</select>";

    return $select;
});

Form::macro('billStatusPicker', function ($name, $selected = null) {
    $data[''] = '-- Select Bill Status --';
    $data[1] = 'Billed';
    $data[0] = 'Un-billed';

    return Form::select($name, $data, $selected);
});

// Form Macros (General)

Form::macro('datePicker', function ($name, $selected = null) {
    if ($selected != null && $selected != '0') {
        $selected = date("d-m-Y", $selected);
    }

    return Form::text($name, $selected, ['class' => 'datepicker', 'readonly' => 'true', 'placeholder' => 'dd-mm-YYYY']);
});


// Html Macros

Html::macro('active', function ($route, $params = null) {
    return preg_match('~^' . route($route, $params) . '~', Request::url()) ? 'active' : '';
});

Html::macro('delete', function ($route, $params = null, $label = 'Delete') {
    $form = Form::open(['route' => [$route, $params], 'method' => 'delete', 'id' => 'deleteForm']);
    $form .= '<a class="confirm">' . $label . '</a>';
    $form .= Form::close();

    return $form;
});

Html::macro('statusSelect', function (Ticket $ticket) {
    $form = view('_partials.task.statusDropdown')
        ->with('ticket', $ticket)
        ->render();

    return $form;
});