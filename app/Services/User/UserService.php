<?php

namespace DCODESupport\Services\User;

use DCODESupport\Http\Requests\User\UserRequest;
use DCODESupport\User;
use \Request;

class UserService {

	public function delete(User $user)
	{
		$user->delete();
	}

	public function save(UserRequest $request, User $user = null)
	{
		if ($user == null)
		{
			$user = new User();
		}

		$user->first_name = $request->get('first_name');
		$user->last_name = $request->get('last_name');
		$user->email = $request->get('email');

		if ($request->get('password') != '')
		{
			$user->password = \Hash::make($request->get('password'));
		}

		$user->position = $request->get('position');
		$user->location = $request->get('location');
        $user->phone = $request->get('phone');
        $user->internal = $request->get('internal');
		$user->client_id = ($request->get('client_id') != '' && $request->get('client_id') != '0' ? $request->get('client_id') : null);
		$user->access = $request->get('access');
		$user->manager = $request->get('manager');
		$user->admin = $request->get('admin');

		$user->save();

		return $user;
	}

	public function search()
	{
		$users = new User();

        // Search
        if (Request::get('name') !== null && Request::get('name') !== '') {
            $users = $users->whereRaw("concat(users.first_name, ' ', users.last_name) like '%" . Request::get('name') . "%'");
        }

        //Sort
        if (Request::get('sort') !== null && Request::get('sort') !== '' && Request::get('sortDir') !== null && Request::get('sortDir') !== '') {
            $sort = Request::get('sort') == 'due_date' ? 'date_due' : Request::get('sort');
            $sortDir = Request::get('sortDir');

            if($sort == 'company'){
                $users = $users->leftjoin('clients', 'clients.id', '=', 'users.client_id')->select('users.*')->orderBy('clients.name', $sortDir);
            }else{
                $users = $users->orderBy('users.' . $sort, $sortDir);
            }
        } else {
            $users = $users->orderBy('users.created_at', 'desc');
        }

		$users = $users->paginate(20);

		return $users;
	}
}