<?php

namespace DCODESupport\Services\Email;

use DCODESupport\Models\Email\Email;
use DCODESupport\User;

class EmailService {
	
	public static function save($type, $id, User $toUser, $subject, $body)
	{
		// Prepare email ...
		$email = new Email();

		$email->to_user_id = $toUser->id;
		$email->emailable_id = $id;
		$email->emailable_type = $type;
		$email->to = $toUser->email;
		$email->subject = $subject;
		$email->body = $body;
		
		$email->user_id = \Auth::id();
		
		$email->save();

		// Send email ...
		$res = \Mail::send('emails.general', ['body' => $body], function ($message) use ($email)
		{
			$message->to($email->to)->subject($email->subject);
		});

		// Save result ...
		$email->sent_id = @$res['MessageId'];
		$email->save();
		
		return $email;
	}
	
}