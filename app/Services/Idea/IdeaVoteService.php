<?php

namespace DCODESupport\Services\Idea;

use \Auth;
use DCODESupport\Http\Requests\Idea\IdeaVoteRequest;
use DCODESupport\Models\Idea\Idea;
use DCODESupport\Models\Idea\IdeaVote;
use DCODESupport\User;
use \Request;


class IdeaVoteService {

	public static function delete(IdeaVote $ideaVote)
	{
        $ideaVote->delete();
	}

    public static function save(Idea $idea, IdeaVote $ideaVote = null)
    {
        if ($ideaVote == null)
        {
            $ideaVote = new IdeaVote();
        }
        //Check for project_id null
        $ideaVote->idea_id = $idea->id;
        $ideaVote->user_id = Auth::user()->id;

        $ideaVote->save();

        return $ideaVote;
    }

	public static function vote(Idea $idea, User $user = null)
    {
        $ideaVote = IdeaVote::where('idea_id', $idea->id)->where('user_id', $user->id);

        if ($ideaVote->exists())
        {
            // Delete
            self::delete($ideaVote->first());
            return IdeaVote::where('idea_id', $idea->id)->count();
        }
        else
        {
            // Vote
            self::save($idea);
            return IdeaVote::where('idea_id', $idea->id)->count();
        }
    }




}