<?php

namespace DCODESupport\Services\Idea;

use \Auth;
use DCODESupport\Http\Requests\Idea\IdeaRequest;
use DCODESupport\Models\Idea\Idea;
use \Request;


class IdeaService {

	public static function delete(Idea $idea)
	{
		$idea->delete();
	}

	public static function save(IdeaRequest $request, Idea $idea = null)
	{
		if ($idea == null)
		{
			$idea = new Idea();
		}
        //Check for project_id null
        if($request->get('project_id') == '' || $request->get('project_id') == 0){
            $idea->project_id = null;
        }else{
            $idea->project_id = $request->get('project_id');
        }

		$idea->client_id = $request->get('client_id');
		$idea->name = $request->get('name');
		$idea->comment = $request->get('comment');
		$idea->user_id = Auth::user()->id;

		$idea->save();

		return $idea;
	}

	public static function search()
	{
		$ideas = new Idea();

		// Search
		if (Request::get('client_id') != null && Request::get('client_id') != '' && Request::get('client_id') != '0')
		{
			$ideas = $ideas->where('client_id', Request::get('client_id'));
		}

		if (Request::get('project_id') != null && Request::get('project_id') != '' && Request::get('project_id') != '0')
		{
			$ideas = $ideas->where('project_id', Request::get('project_id'));
		}

		if (Request::get('name') != null && Request::get('name') != '')
		{
			$ideas = $ideas->where('name', 'LIKE', '%'.Request::get('name').'%');
		}

		if (Request::get('sort') !== null && Request::get('sort') !== '')
		{
			$ideas = $ideas->orderBy(Request::get('sort'), Request::get('sortDir'));
		}
		else
		{
			$ideas = $ideas->orderBy('name', 'asc');
		}

		$ideas = $ideas->paginate(20);

		return $ideas;
	}
}