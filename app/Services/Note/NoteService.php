<?php

namespace DCODESupport\Services\Note;

use \Auth;
use DCODESupport\Models\Note\Note;
use \Request;

use DCODESupport\Http\Requests\Note\NoteRequest;
use DCODESupport\Models\Client\Client;
use DCODESupport\Models\Project\Project;
use DCODESupport\Models\Status;
use DCODESupport\Models\Ticket\Ticket;
use DCODESupport\Models\Ticket\TicketStatus;
use DCODESupport\User;

class NoteService
{

    public static function destroy(Note $note)
    {
        $note->delete();
    }

    public static function save(NoteRequest $request, User $user = null, Note $note = null, Ticket $ticket = null)
    {
        if ($note == null) {
            $note = new Note();
        }

        //Save task
        if($request->get('add_task') || $ticket !== null){
            //Add task
            if($ticket == null){
                $ticket = new Ticket();

                $ticket->client_id = ($request->get('client_id') != '' && $request->get('client_id') != '0' ? $request->get('client_id') : null);
                $ticket->project_id = ($request->get('project_id') != '' && $request->get('project_id') != '0' ? $request->get('project_id') : null);
                $ticket->name = $request->get('name');
                $ticket->comment = $request->get('comment');
                $ticket->date_due = ($request->get('date_due') != '' && $request->get('date_due') != '0' ? strtotime($request->get('date_due')) : null);
                $ticket->assigned_user_id = ($request->get('assigned_user_id') != '' && $request->get('assigned_user_id') != '0' ? $request->get('assigned_user_id') : null);
                $ticket->internal = $request->get('internal');
                $ticket->estimated_time = $request->get('estimated_time');
                $ticket->user_id = $user->id;

                $ticket->save();
            }else{
                $ticket->save();
            }
        }else{
            //Delete task
            if(!empty($note->ticket)){
                $note->ticket->delete();
            }elseif (!empty($note->$ticket)){
                $ticket->delete();
            }
        }

        //Save Note
        $note->user_id = $user->id;
        $note->project_id = ($request->get('project_id') != '' && $request->get('project_id') != '0' ? $request->get('project_id') : null);
        $note->ticket_id = empty($ticket) ? null : $ticket->id;
        $note->message = $request->get('message');
        $note->note_type_id = empty($request->get('note_type_id')) ? null : $request->get('note_type_id');

        $note->save();

        return $note;
    }

    /**
     * @param User|null $user
     * @return Note
     */
    public static function search(User $user = null)
    {

        $notes = new Note();

        //Search
        if($user != null){
            $notes = $notes->where('notes.user_id', $user->id);
        }
        // Search
        if (Request::get('message') !== null && Request::get('message') !== '') {
            $notes = $notes->where('notes.message', 'LIKE', '%' . Request::get('message') . '%');
        }

        if (Request::get('note_type_id') !== null && Request::get('note_type_id') !== '' && Request::get('note_type_id') !== '0') {
            $notes = $notes->where('notes.note_type_id', Request::get('note_type_id'));
        }

        if (Request::get('project_id') !== null && Request::get('project_id') !== '' && Request::get('project_id') !== '0') {
            $notes = $notes->where('notes.project_id', Request::get('project_id'));
        }

        // Sort
        if (Request::get('sort') !== null && Request::get('sort') !== '' && Request::get('sortDir') !== null && Request::get('sortDir') !== '') {
            $sort = Request::get('sort') == 'due_date' ? 'date_due' : Request::get('sort');
            $sortDir = Request::get('sortDir');

            if ($sort == 'project') {
                $notes = $notes->leftjoin('projects', 'projects.id', '=', 'notes.project_id')->select('notes.*')->orderBy('projects.name', $sortDir);
            } elseif ($sort == 'note_type') {
                $notes = $notes->leftjoin('note_types', 'note_types.id', '=', 'notes.note_type_id')->select('notes.*')->orderBy('note_types.name', $sortDir);
            } else{
                $notes = $notes->orderBy('notes.' . $sort, $sortDir);
            }
        } else {
            $notes = $notes->orderBy('notes.created_at', 'desc');
        }

        $notes = $notes->paginate(40);

        return $notes;
    }
}