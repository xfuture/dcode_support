<?php

namespace DCODESupport\Services\File;

use \Auth;
use DCODESupport\Http\Requests\File\FileRequest;
use DCODESupport\Models\File\File;
use \Request;


class FileService
{
	
	protected static $baseUrl               = 'http://cdn.defex.co/';
	protected static $placeholderImage      = '';
	protected static $taskAttachedFilesLink = 'task-attached-files/';
	
	public static function getBaseURL()
	{
		return 'http://cdn.support.dcode.com.au/';
	}
	
	public static function getTaskAttachedFilesURL()
	{
        return self::$taskAttachedFilesLink;
		//return self::getBaseURL() . self::$taskAttachedFilesLink;
	}
	
	public static function delete(File $file)
	{
		if (Storage::disk('s3')->exists(''))
		{
			Storage::disk('s3')->delete('');
		}
		$file->delete();
	}
	
	public static function save(FileRequest $request, File $file = null)
	{
		if ($file == null)
		{
			$file = new File();
		}
		$fileUpload = $request->file('uploadFile');
		
		$file->original_name = $fileUpload->getClientOriginalName();
		$file->hash_name     = $fileUpload->hashName();
		$file->size          = $fileUpload->getClientSize();
		$file->file_type     = $fileUpload->guessClientExtension();
		$file->user_id       = Auth::user()->id;
		$file->ticket_id     = $request->get('ticket_id');
		
		//Store image into hard disk
		$file->save();
		
		//Store image into cloud
		$fileUpload->storeAs('task-attached-files/' . $fileUpload->hashName(), $fileUpload->getClientOriginalName(),'s3');

        return $file;
    }
}