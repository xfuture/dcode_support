<?php

namespace DCODESupport\Services\AWS;

use \Auth;
use Aws\CloudFront\CloudFrontClient;
class AWSService
{
    private static $baseURL = 'http://cdn.support.dcode.com.au/';

    public static function getSignedURL($resource, $timeout)
    {
        $expires = time() + $timeout;
        // ToDo: Add SSL variant ...
        $resource =  self::$baseURL . urlencode($resource);

        $cloudFront = new CloudFrontClient([
            'private_key' => app_path() . '/keypairs/pk-APKAJRWQOWX3MUOVE6MQ.pem',
            'key_pair_id' => 'APKAJRWQOWX3MUOVE6MQ',
            'region'      => 'ap-southeast-2',
            'version'     => '2014-11-06'
        ]);

        $signedUrlCannedPolicy = $cloudFront->getSignedUrl([
            'url'         => $resource,
            'expires'     => $expires,
            'private_key' => app_path() . '/keypairs/pk-APKAJRWQOWX3MUOVE6MQ.pem',
            'key_pair_id' => 'APKAJRWQOWX3MUOVE6MQ',
        ]);

        return $signedUrlCannedPolicy;
    }
}