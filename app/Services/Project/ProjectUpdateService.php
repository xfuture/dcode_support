<?php

namespace DCODESupport\Services\Project;

use \Auth;
use DCODESupport\Http\Requests\Project\ProjectUpdateRequest;
use DCODESupport\Models\Project\Project;
use DCODESupport\Models\Project\ProjectUpdate;
use \Request;

class ProjectUpdateService {
	
	public function delete(ProjectUpdate $projectUpdate)
	{
		$projectUpdate->delete();
	}
	
	public function save(ProjectUpdateRequest $request, Project $project, ProjectUpdate $projectUpdate = null)
	{
		if ($projectUpdate == null)
		{
			$projectUpdate = new ProjectUpdate();
		}
		
		$projectUpdate->project_id = $project->id;
		$projectUpdate->project_status_id = $request->get('project_status_id');
		$projectUpdate->comment = $request->get('comment');
		$projectUpdate->user_id = Auth::user()->id;
		
		$projectUpdate->save();
		
		return $projectUpdate;
	}
	
	public function search(Project $project = null)
	{
		$projectUpdates = new ProjectUpdate();
		
		if ($project != null)
		{
			$projectUpdates = $projectUpdates->where('project_id', $project->id);
		}
		
		// Search

		
		// Sort
		if (Request::get('sort') !== null && Request::get('sort') !== '')
		{
			$projectUpdates = $projectUpdates->orderBy(Request::get('sort'), Request::get('sortDir'));
		}
		else
		{
			$projectUpdates = $projectUpdates->orderBy('created_at', 'desc');
		}
		
		$projectUpdates = $projectUpdates->paginate(20);
		
		return $projectUpdates;
	}
}