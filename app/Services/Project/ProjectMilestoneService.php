<?php

namespace DCODESupport\Services\Project;

use DCODESupport\Http\Requests\Project\ProjectMilestoneRequest;
use DCODESupport\Models\Project\Project;
use DCODESupport\Models\Project\ProjectMilestone;

use \Auth;
use \Request;

class ProjectMilestoneService
{
	
	public function delete(ProjectMilestone $projectMilestone)
	{
		$projectMilestone->delete();
	}
	
	public static function getIncompleteMilestones()
	{
		$incompleteMilestones = ProjectMilestone::whereNull('complete');
		
		if (Auth::user()->admin == '0')
		{
			$incompleteMilestones = $incompleteMilestones->whereIn('project_id', function ($query)
			{
				$query->select('project_id')
				      ->from('project_users')
				      ->where('access_user_id', Auth::id())
				      ->whereNull('deleted_at');
			});
		}
		
		$incompleteMilestones = $incompleteMilestones->whereNull('deleted_at')
		                                             ->orderBy('scheduled', 'asc')
		                                             ->get();
		
		return $incompleteMilestones;
	}

	public function save(ProjectMilestoneRequest $request, Project $project, ProjectMilestone $projectMilestone = null)
	{
		if ($projectMilestone == null)
		{
			$projectMilestone = new ProjectMilestone();
		}
		
		$projectMilestone->project_id = $project->id;
		$projectMilestone->name       = $request->get('name');
		$projectMilestone->scheduled  = ($request->get('scheduled') != '' && $request->get('scheduled') > 0 ? strtotime($request->get('scheduled')) : strtotime("+1 week"));
		$projectMilestone->complete   = ($request->get('complete') != '' && $request->get('complete') > 0 ? strtotime($request->get('complete')) : null);;
		$projectMilestone->user_id = Auth::user()->id;
		
		$projectMilestone->save();
		
		return $projectMilestone;
	}
	
	public function search(Project $project = null)
	{
		$projectMilestones = new ProjectMilestone();
		
		if ($project != null)
		{
			$projectMilestones = $projectMilestones->where('project_id', $project->id);
		}
		
		// Search
		if (Request::get('name') !== null && Request::get('name') !== '')
		{
			$projectMilestones = $projectMilestones->where('project_milestones.name', 'LIKE', '%' . Request::get('name') . '%');
		}
		
		// Sort
		if (Request::get('sort') !== null && Request::get('sort') !== '')
		{
			$projectMilestones = $projectMilestones->orderBy(Request::get('sort'), Request::get('sortDir'));
		}
		else
		{
			$projectMilestones = $projectMilestones->orderBy('created_at', 'desc');
		}
		
		$projectMilestones = $projectMilestones->paginate(20);
		
		return $projectMilestones;
	}

    //public static function testGetMilestones(){

        // $testGetMilestones=Project::getProjectMilestones();
        // dd( $testGetMilestones);

   // }



/*
	public static function getProjectMilestones(){

        //$incomplete = ProjectMilestone::whereNull('complete')->where('project_id','IS',$project->id);
        $incomplete = ProjectMilestone::whereNull('complete');
            $incomplete = $incomplete->whereIn('project_id', function ($query)
            {
                $query->select('project_id')
                    ->from('project_milestones')
                    ->whereNull('deleted_at');
            });

        $incomplete = $incomplete->whereNull('deleted_at')
            ->orderBy('scheduled', 'asc')
            ->get();

        return $incomplete;
    }
*/

}