<?php

namespace DCODESupport\Services\Project;

use \Auth;
use \Request;

use DCODESupport\Models\Client\Client;
use DCODESupport\Models\Project\Project;
use DCODESupport\Models\Project\ProjectUser;
use DCODESupport\User;

class ProjectService {

	public function delete(Project $project)
	{
		$project->delete();
	}

	public function save($request, Client $client, Project $project = null)
	{
		if ($project == null)
		{
			$project = new Project();
		}

		$project->client_id = $client->id;
		$project->code = ($request->get('code') == null ? self::generateProjectCode($client->id) : $request->get('code'));
		$project->name = $request->get('name');
        $project->bugsnag_id = $request->get('bugsnag_id');
		$project->active = $request->get('active');
		$project->internal_status = $request->get('internal_status');
		$project->user_id = Auth::user()->id;

		$project->save();

		return $project;
	}

	public static function search(User $accessUser = null, Client $client = null)
	{
		$projects = new Project();
		
		// Only show active ...
		if (\Request::get('active') == null)
		{
			$projects = $projects->where('active', '1');
		}

		if ($accessUser != null || \Auth::user()->admin == '0')
		{
			if ($accessUser == null)
			{
				$accessUser = \Auth::user();
			}

			$projects = $projects->whereIn('projects.id', function($query) use ($accessUser)
			{
				$query->select('project_id')
					->from('project_users')
					->where('access_user_id', $accessUser->id)
					->whereNull('deleted_at');
			});

			// Only show active projects ...
			$projects = $projects->where('active', '1');
		}

		// Search
		if (Request::get('code') !== null && Request::get('code') !== '')
		{
			$projects = $projects->where('projects.code', 'LIKE', '%'.Request::get('code').'%');
		}

		if (Request::get('name') !== null && Request::get('name') !== '')
		{
			$projects = $projects->where('projects.name', 'LIKE', '%'.Request::get('name').'%');
		}

		if (Request::get('client_id') !== null && Request::get('client_id') != '0')
		{
			$projects = $projects->where('projects.client_id', Request::get('client_id'));
		}elseif(isset($client)){
            $projects = $projects->where('projects.client_id', $client->id);
        }

		// Sort
		if (Request::get('sort') !== null && Request::get('sort') !== '')
		{
			if (Request::get('sort') == 'client')
			{
				$projects = $projects->join('clients', 'projects.client_id', '=', 'clients.id')
				                 ->orderBy('clients.name', Request::get('sortDir'));

				$projects = $projects->select('projects.*', 'clients.name');
			}
			else
			{
				$projects = $projects->orderBy(Request::get('sort'), Request::get('sortDir'));
			}
		}
		else
		{
			$projects = $projects->orderBy('projects.active', 'desc')
			                     ->orderBy('projects.created_at', 'desc');
		}

		$projects = $projects->paginate(20);

		return $projects;
	}

	public function cancelAccess(ProjectUser $projectUser)
	{
		$projectUser->delete();
	}

	public function userAccess(Project $project, User $user)
	{
		$projectUser = ProjectUser::firstOrCreate([
			'project_id' => $project->id,
			'access_user_id' => $user->id,
			'user_id' => \Auth::id()
		]);

		return $projectUser;
	}

    /**
     * Generate Project Code for a client
     *
     * @param $clientId
     * @return string
     */
	public static function generateProjectCode($clientId){
	    $projectCode = '';
	    $client = Client::find($clientId);
	    //Count number of project
	    if($client){

            $projectCode = $client->short_name.'-'.date("Y").'-'.sprintf( '%04d', $client->projects()->whereRaw("YEAR(created_at) = ". date("Y"))->count() + 1);
        }
	    return $projectCode;
    }

    public static function getActiveProjects(){
        $activeProjects = Project::where('active',1);

        if (Auth::user()->admin == '0')
        {
            $activeProjects =  $activeProjects->whereIn('project_id', function ($query)
            {
                $query->select('project_id')
                    ->from('projects')
                    ->where('access_user_id', Auth::id())
                    ->whereNull('deleted_at');
            });
        }

        $activeProjects =  $activeProjects->whereNull('deleted_at')
            ->orderBy('created_at', 'asc')
            ->get();

        return  $activeProjects;

    }

}