<?php
namespace DCODESupport\Services;

use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Session;

class NavigationService
{

    public static function setPosition($name)
    {
        Session::put('backURL_' . $name, Request::fullUrl());
    }

    public static function returnPosition($name, $defaultRoute, $message)
    {
        return ($url = Session::pull('backURL_' . $name, null))
            ? redirect()
                ->to($url)
                ->with('flashNotice', $message)
            : redirect()
                ->to($defaultRoute)
                ->with('flashNotice', $message);
    }
}