<?php

namespace DCODESupport\Services;

use DCODESupport\Models\Client\Client;
use DCODESupport\Models\Project\Project;
use DCODESupport\Models\Project\ProjectStatus;
use DCODESupport\Models\Status;
use DCODESupport\Models\Ticket\TicketStatus;
use DCODESupport\User;

class DisplayService {

	// Models ...

	public static function displayClient(Client $client = null)
	{
		if ($client != null)
		{
			return $client->name;
		}
	}

	public static function displayProject(Project $project = null)
	{
		if ($project != null)
		{
			return $project->name;
		}
	}

	public static function displayProjectCode(Project $project = null)
	{
		if ($project != null)
		{
			return $project->code;
		}
	}

	public static function displayProjectStatus(ProjectStatus $projectStatus = null)
	{
		if ($projectStatus != null)
		{
			return $projectStatus->name;
		}
	}

	public static function displayStatus(Status $status = null)
	{
		return $status->name;
	}

	public static function displayTicketStatus(TicketStatus $ticketStatus = null)
	{
		if ($ticketStatus != null)
		{
			return self::displayStatus($ticketStatus->status);
		}
		else
		{
			return 'Pending';
		}
	}

	public static function displayUser(User $user = null)
	{
		if ($user != null)
		{
			return $user->first_name.' '.$user->last_name;
		}
	}

	// General ...
	public static function displayCheckbox($value)
	{
		return $value;
	}

	public static function displayDate($date)
	{
		if ($date != '' && $date != null)
		{
			return date("j F Y", $date);
		}
	}

	public static function displayExcerpt($string)
	{
		if (strlen($string) > 100)
		{
			return substr($string, 0, 100) . '...';
		}

		return $string;
	}
}