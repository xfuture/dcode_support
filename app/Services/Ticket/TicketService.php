<?php

namespace DCODESupport\Services\Ticket;

use \Auth;
use DCODESupport\Services\Email\EmailService;
use \Request;

use DCODESupport\Http\Requests\Ticket\TicketRequest;
use DCODESupport\Models\Client\Client;
use DCODESupport\Models\Project\Project;
use DCODESupport\Models\Status;
use DCODESupport\Models\Ticket\Ticket;
use DCODESupport\Models\Ticket\TicketStatus;
use DCODESupport\User;

class TicketService
{

    public function delete(Ticket $ticket)
    {
        $ticket->delete();
    }

    public static function projects(Client $client = null, Project $project = null, User $accessUser = null)
    {
        $tickets = new Ticket();

        if ($accessUser != null || \Auth::user()->admin == '0') {
            if ($accessUser == null) {
                $accessUser = \Auth::user();
            }

            $tickets = $tickets->where(function ($query) use ($accessUser) {
                $query->whereIn('project_id', function ($query) use ($accessUser) {
                    $query->select('project_id')
                        ->from('project_users')
                        ->where('access_user_id', $accessUser->id)
                        ->whereNull('deleted_at');
                })
                    ->orWhere('user_id', $accessUser->id);
            });

            if ($accessUser->internal != '1') {
                $tickets = $tickets->where('internal', '0');
            }
        }

        // Search
        if (Request::get('name') !== null && Request::get('name') !== '') {
            $tickets = $tickets->where('tickets.name', 'LIKE', '%' . Request::get('name') . '%');
        }

        if (Request::get('project_id') !== null && Request::get('project_id') !== '' && Request::get('project_id') !== '0') {
            $tickets = $tickets->where('tickets.project_id', Request::get('project_id'));
        }

        if (Request::get('status_id') !== null && Request::get('status_id') !== '' && Request::get('status_id') !== '0') {
            $tickets = $tickets->whereIn('tickets.id', function ($query) {
                $query->select('ticket_id')
                    ->from('ticket_statuses')
                    ->where('current', '1')
                    ->whereNull('deleted_at')
                    ->where('status_id', Request::get('status_id'));
            });
        }


        $tickets = $tickets->select('project_id')->get();

        return $projects = Project::whereIn('id', $tickets->toArray())->get();
    }

    public static function notify(Ticket $ticket)
    {
        // Notify users ...
        $type = get_class($ticket);
        $id = $ticket->id;
        if (\App::environment('local')) {
            $toUser = User::where('email', 'andrew@dcode.com.au')->first();
        } else {
            if ($ticket->assignedUser != null) {
                $toUser = $ticket->assignedUser;
            } else {
                $toUser = User::where('email', 'andrew@dcode.com.au')->first();
            }
        }

        $subject = 'Updated Task - ' . ($ticket->project != null ? $ticket->project->name : 'General') . ' - ' . $ticket->name;
        $body = view('emails.ticket.updated')
            ->with('ticket', $ticket)
            ->with('admin', true)
            ->render();

        EmailService::save($type, $id, $toUser, $subject, $body);
//		EmailService::save($type, $id, User::where('email', 'lucas@dcode.com.au')->first(), $subject, $body);
        //EmailService::save($type, $id, User::where('email', 'keshawa@dcode.com.au')->first(), $subject, $body);
    }

    public function save(TicketRequest $request, Ticket $ticket = null)
    {
        $changed = false;

        if ($ticket == null) {
            $ticket = new Ticket();

            $changed = true;
        }


        $ticket->client_id = ($request->get('client_id') != '' && $request->get('client_id') != '0' ? $request->get('client_id') : null);
        $ticket->project_id = ($request->get('project_id') != '' && $request->get('project_id') != '0' ? $request->get('project_id') : null);
        $ticket->name = $request->get('name');
        $ticket->comment = $request->get('comment');
        $ticket->date_due = ($request->get('date_due') != '' && $request->get('date_due') != '0' ? strtotime($request->get('date_due')) : null);
        $ticket->assigned_user_id = ($request->get('assigned_user_id') != '' && $request->get('assigned_user_id') != '0' ? $request->get('assigned_user_id') : null);
        $ticket->internal = $request->get('internal');
        $ticket->estimated_time = $request->get('estimated_time');
        $ticket->user_id = \Auth::id();


        /*
                if('client_id' ==0||'project_id' ==0||'assigned_user_id'==0){
                    //dd($request->all());
                    $users = new User();
                    $users = $users->whereIn('id', function ($query)
                    {
                        $query->select('id')
                            ->from('users')
                            ->where('email','andrew@dcode.com');
                    });




                }*/

        if ($ticket->isDirty()) {
            $changed = true;
        }

        $ticket->save();

        if ($changed == true) {
            self::notify($ticket);
        }

        return $ticket;
    }

    public function search(Client $client = null, Project $project = null, $statusId = null, User $accessUser = null)
    {

        $tickets = new Ticket();

        //Show archived tickets
        if (Request::get('archived') !== null && Request::get('archived') !== '') {
            $archived = Request::get('archived');
        }

        //Check status tickets to show archived tickets
        if ($statusId == '0') {
            $tickets = $tickets->whereNotIn('tickets.id', function ($query) use ($statusId) {
                $query->select('ticket_id')
                    ->from('ticket_statuses')
                    ->where('current', '1')
                    ->whereNull('deleted_at');
            });
        } elseif (!isset($archived) && empty(Request::get('status_id'))) {
            $tickets = $tickets->whereNotIn('tickets.id', function ($query) {
                $query->select('ticket_id')
                    ->from('ticket_statuses')
                    ->where('status_id', Status::where('name', 'Archived')->first()->id)
                    ->where('current', '1')
                    ->whereNull('deleted_at');
            });
        } elseif($statusId != null) {
            $tickets = $tickets->whereIn('tickets.id', function ($query) use ($statusId) {
                $query->select('ticket_id')
                    ->from('ticket_statuses')
                    ->where('status_id', $statusId)
                    ->where('current', '1')
                    ->whereNull('deleted_at');
            });
        }


        //Check ticket status_id and statususes table refrences
        if($statusId==1 ||$statusId==2||$statusId==3){
            $tickets = $tickets->whereIn('tickets.id', function ($query) use ($statusId) {
                $query->select('ticket_id')
                    ->from('ticket_statuses')
                    ->where('status_id', $statusId)
                    ->where('current', '1')
                    ->whereNull('deleted_at');
            });
        }
        // End code

        if (Request::get('status_id') !== null && Request::get('status_id') !== '' && Request::get('status_id') !== '0') {
            $tickets = $tickets->whereIn('tickets.id', function ($query) {
                $query->select('ticket_id')
                    ->from('ticket_statuses')
                    ->where('current', '1')
                    ->whereNull('deleted_at')
                    ->where('status_id', Request::get('status_id'));

            });
        }


        if ($accessUser != null || \Auth::user()->admin == '0') {
            if ($accessUser == null) {
                $accessUser = \Auth::user();
            }

            $tickets = $tickets->where(function ($query) use ($accessUser) {
                $query->whereIn('project_id', function ($query) use ($accessUser) {
                    $query->select('project_id')
                        ->from('project_users')
                        ->where('access_user_id', $accessUser->id)
                        ->whereNull('deleted_at');
                })
                    ->orWhere('user_id', $accessUser->id);
            });

            if ($accessUser->internal != '1') {
                $tickets = $tickets->where('internal', '0');
            }
        }

        // Search
        if (Request::get('name') !== null && Request::get('name') !== '') {
            $tickets = $tickets->where('tickets.name', 'LIKE', '%' . Request::get('name') . '%');
        }

        if (Request::get('client_id') !== null && Request::get('client_id') !== '' && Request::get('client_id') !== '0') {
            $tickets = $tickets->where('tickets.client_id', Request::get('client_id'));
        }

        if (Request::get('project_id') !== null && Request::get('project_id') !== '' && Request::get('project_id') !== '0') {
            $tickets = $tickets->where('tickets.project_id', Request::get('project_id'));
        }


        if (Request::get('assigned_user_id') !== null && Request::get('assigned_user_id') !== '' && Request::get('assigned_user_id') !== '0') {
            if (Request::get('assigned_user_id') == 'null') {
                $tickets = $tickets->whereNull('assigned_user_id');
            } else {
                $tickets = $tickets->where('assigned_user_id', Request::get('assigned_user_id'));
            }
        }

        // Sort
        if (Request::get('sort') !== null && Request::get('sort') !== '' && Request::get('sortDir') !== null && Request::get('sortDir') !== '') {
            $sort = Request::get('sort') == 'due_date' ? 'date_due' : Request::get('sort');
            $sortDir = Request::get('sortDir');

            if ($sort == 'project') {
                $tickets = $tickets->leftjoin('projects', 'projects.id', '=', 'tickets.project_id')->select('tickets.*')->orderBy('projects.name', $sortDir);
            } elseif ($sort == 'assigned_user') {
                $tickets = $tickets->leftjoin('users', 'users.id', '=', 'tickets.assigned_user_id')->select('tickets.*')->orderBy('users.first_name', $sortDir)->orderBy('users.last_name', $sortDir);
            } elseif($sort == 'status'){
//                $tickets = $tickets->leftjoin('ticket_statuses', 'ticket_statuses.ticket_id', '=', 'tickets.id')->where('current', 1)->select('tickets.*')->orderBy('ticket_statuses.status_id', $sortDir);
            } else{
                $tickets = $tickets->orderBy('tickets.' . $sort, $sortDir);
            }
        } else {
            $tickets = $tickets->orderBy('tickets.created_at', 'desc');
        }

        $tickets = $tickets->paginate(40);
        //TESTING CODE
       // if($statusId==1){
           // dd(\DB::getQueryLog());
       //  }
        //END TESTING CODE
        return $tickets;
    }

    public function updateStatus(Ticket $ticket, Status $status = null)
    {
        TicketStatus::where('ticket_id', $ticket->id)
            ->update(['current' => '0']);

        if ($status != null) {
            $ticketStatus = TicketStatus::create([
                'ticket_id' => $ticket->id,
                'status_id' => $status->id,
                'current' => 1,
                'user_id' => \Auth::id()
            ]);

            return $ticketStatus;
        } else {
            return null;
        }
    }

    public static function users(Client $client = null, Project $project = null, User $accessUser = null)
    {
        $tickets = new Ticket();

        if ($accessUser != null || \Auth::user()->admin == '0') {
            if ($accessUser == null) {
                $accessUser = \Auth::user();
            }

            $tickets = $tickets->where(function ($query) use ($accessUser) {
                $query->whereIn('project_id', function ($query) use ($accessUser) {
                    $query->select('project_id')
                        ->from('project_users')
                        ->where('access_user_id', $accessUser->id)
                        ->whereNull('deleted_at');
                })
                    ->orWhere('user_id', $accessUser->id);
            });

            if ($accessUser->internal != '1') {
                $tickets = $tickets->where('internal', '0');
            }
        }

        // Search
        if (Request::get('name') !== null && Request::get('name') !== '') {
            $tickets = $tickets->where('tickets.name', 'LIKE', '%' . Request::get('name') . '%');
        }

        if (Request::get('project_id') !== null && Request::get('project_id') !== '' && Request::get('project_id') !== '0') {
            $tickets = $tickets->where('tickets.project_id', Request::get('project_id'));
        }

        if (Request::get('status_id') !== null && Request::get('status_id') !== '' && Request::get('status_id') !== '0') {
            $tickets = $tickets->whereIn('tickets.id', function ($query) {
                $query->select('ticket_id')
                    ->from('ticket_statuses')
                    ->where('current', '1')
                    ->whereNull('deleted_at')
                    ->where('status_id', Request::get('status_id'));
            });
        }


        $tickets = $tickets->select('assigned_user_id')->get();

        return $users = User::whereIn('id', $tickets->toArray())->get();
    }


}