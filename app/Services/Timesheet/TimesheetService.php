<?php

namespace DCODESupport\Services\Timesheet;

use \Auth;
use DCODESupport\Http\Requests\Timesheet\TimesheetRequest;
use DCODESupport\Models\Project\Project;
use DCODESupport\Models\Timesheet\Timesheet;


class TimesheetService
{
	
	public function delete(Timesheet $timesheet)
	{
		$timesheet->delete();
	}
	
	public function save(TimesheetRequest $request, Project $project, Timesheet $timesheet = null)
	{
		if ($timesheet == null)
		{
			$timesheet = new Timesheet();
			
			// Only add the user on creation
			$timesheet->user_id = Auth::user()->id;
		}
		
		$timesheet->project_id = $project->id;
		
		if ($request->get('ticket_id') != null)
		{
			$timesheet->ticket_id = $request->get('ticket_id');
		}
		
		$timesheet->comment = $request->get('comment');
		$timesheet->start   = ($request->get('start') != '' ? strtotime($request->get('start')) : null);
		$timesheet->stop    = ($request->get('stop') != '' ? strtotime($request->get('stop')) : null);
		$timesheet->billed  = $request->get('billed');
		
		$hours = ($timesheet->stop != null ? gmdate("H:i", ($timesheet->stop - $timesheet->start)) : '');
		if ($hours != '')
		{
			$part = explode(':', $hours);
			$h    = $part[0];
			$m    = $part[1];
			
			$hours = number_format($h + ($m / 60), 2);
		}
		$timesheet->hours = $hours;
		
		$timesheet->save();
		
		return $timesheet;
	}
	
	public function search($count = false)
	{
        $timesheets = new Timesheet();

		//Search by user
		if (\Auth::user()->admin == '0')
		{
			$timesheets = $timesheets->where('user_id', \Auth::id());
		}elseif(\Request::get('user_id') != null && \Request::get('user_id') != '' && \Request::get('user_id') != '0'){
            $timesheets = $timesheets->where('user_id', \Request::get('user_id'));
        }

        //Search by project
		if (\Request::get('project_id') != null && \Request::get('project_id') != '' && \Request::get('project_id') != '0')
		{
			$timesheets = $timesheets->where('project_id', \Request::get('project_id'));
		}

		//Search by status
        if (\Request::get('billed') != null && \Request::get('billed') != '')
        {
            $timesheets = $timesheets->where('billed', \Request::get('billed'));
        }

        if (\Request::get('sort') !== null && \Request::get('sort') !== '' && \Request::get('sortDir') !== null && \Request::get('sortDir') !== '')
        {
            $sort = \Request::input('sort');
            $sortDir = \Request::input('sortDir');

            if($sort == 'project'){
                $timesheets = $timesheets->leftjoin('projects', 'projects.id', '=', 'timesheets.project_id')->select('timesheets.*')->orderBy('projects.name', $sortDir);
            }elseif($sort == 'user'){
                $timesheets = $timesheets->leftjoin('users', 'users.id', '=', 'timesheets.user_id')->select('timesheets.*')->orderBy('users.first_name', $sortDir)->orderBy('users.last_name', $sortDir);
            }else{
                $timesheets = $timesheets->orderBy('timesheets.' . $sort, $sortDir);
            }
        }
        else
        {
            $timesheets = $timesheets->orderBy('created_at', 'desc');
        }

		if ($count == false) {
            $timesheets = $timesheets->paginate(20);
        }

		return $timesheets;
	}
}