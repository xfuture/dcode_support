<?php

namespace DCODESupport\Services\Comment;

use DCODESupport\Http\Requests\Comment\CommentRequest;
use DCODESupport\Models\Comment\Comment;
use DCODESupport\Services\Email\EmailService;
use DCODESupport\User;
use DCODESupport\Models\Project;


class CommentService {
	
	public static function save(CommentRequest $request, $type, $id, Comment $comment = null)
	{
		$changed = false;

		if ($comment == null)
		{
			$comment = new Comment();

			$changed = true;
		}
		
		$comment->comment = $request->get('comment');
		$comment->commentable_type = $type;
		$comment->commentable_id = $id;
		$comment->internal = $request->get('internal');
		$comment->user_id = \Auth::id();
		
		$comment->save();

        if ($changed == true)
		{
			// Notify users ...
			$type = get_class($comment);
			$id = $comment->id;
			$toUser = User::where('email', 'andrew@dcode.com.au')->first();
			//$toUser = User::where('email', 'keshawa@dcode.com.au')->first();
			$subject = 'New Comment';

			$body = '';

			switch($comment->commentable_type)
			{
				case('DCODESupport\Models\Project\ProjectUpdate'):
					$body = view('emails.comment.new-project-update')
						->with('comment', $comment)
						->with('admin', true)
						->render();
					break;
				case('DCODESupport\Models\Ticket\Ticket'):
        			$body = view('emails.comment.new-ticket')
						->with('comment', $comment)
						->with('admin', true)
						->render();
					break;
			}

			EmailService::save($type, $id, $toUser, $subject, $body);
		}
		
		return $comment;
	}

}