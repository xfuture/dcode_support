<?php

namespace DCODESupport\Services\Client;

use \Auth;
use \Request;

use DCODESupport\Http\Requests\Client\ClientRequest;
use DCODESupport\Models\Client\Client;

class ClientService {

	public function delete(Client $client)
	{
		$client->delete();
	}

	public function save(ClientRequest $request, Client $client = null)
	{
		if ($client == null)
		{
			$client = new Client();
		}

		$client->name = $request->get('name');
		$client->short_name = $request->get('short_name');
		$client->user_id = Auth::user()->id;

		$client->save();

		return $client;
	}

	public function search()
	{
		$clients = new Client();

		// Search
		if (Request::get('short_name') != null && Request::get('short_name') != '')
		{
			$clients = $clients->where('short_name', 'LIKE', '%'.Request::get('short_name').'%');
		}

		if (Request::get('name') != null && Request::get('name') != '')
		{
			$clients = $clients->where('name', 'LIKE', '%'.Request::get('name').'%');
		}

		if (Request::get('sort') !== null && Request::get('sort') !== '')
		{
			$clients = $clients->orderBy(Request::get('sort'), Request::get('sortDir'));
		}
		else
		{
			$clients = $clients->orderBy('name', 'asc');
		}

		$clients = $clients->paginate(20);

		return $clients;
	}
}