<?php

namespace DCODESupport;

use DCODESupport\Models\Project\Project;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use SoftDeletes;
    use Notifiable;
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    // Belongs to ...
    public function client()
    {
        return $this->belongsTo('DCODESupport\Models\Client\Client');
    }

    // Has many ...
    public function projectUsers()
    {
        return $this->hasMany('DCODESupport\Models\Project\ProjectUser', 'access_user_id', 'id');
    }

    public function projects()
    {
        return $this->hasMany('DCODESupport\Models\Project\Project', 'user_id', 'id');
    }

    public function notes()
    {
        return $this->hasMany('DCODESupport\Models\Note\Note', 'user_id', 'id');
    }


    // General ...
    public function access(Project $project)
    {
        if ($this->projectUsers()->where('project_id', $project->id)->count() > 0)
        {
            return true;
        }

        return false;
    }

    public function hasRole($roles)
    {
        if(is_array($roles))
        {
			foreach($roles as $need_role)
            {
                if($this->{$need_role} != '1') {
                    return false;
                }
            }
            return true;
		} else{
            return ($this->{$need_role} == '1' ? true : false);
        }

		return false;
    }
}
