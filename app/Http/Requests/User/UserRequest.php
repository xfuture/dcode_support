<?php

namespace DCODESupport\Http\Requests\User;

use DCODESupport\Http\Requests\Request;
use DCODESupport\User;
use Illuminate\Support\Facades\Auth;

class UserRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return (auth()->check() && Auth::user()->admin == '1');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $user = null;
        if ($this->user !== null)
        {
            $user = User::find($this->user->id);
        }


        switch ($this->method()){
            case 'GET':
            case 'DELETE':
            {
                return [];
            }
            case 'POST':
            {
                return [
                    'first_name' => 'required|max:255',
                    'last_name'  => 'required|max:255',
                    'email'      => 'required|max:255|unique:users,email,NULL,id,deleted_at,NULL',
                    'password'   => 'required|min:6'
                ];
            }
            case 'PUT':
            case 'PATCH':
            {
                return [
                    'first_name' => 'required|max:255',
                    'last_name'  => 'required|max:255',
                    'email'      => 'required|max:255|unique:users,email,' . $user->id . ',id,deleted_at,NULL',
                    'password'   => 'min:6|confirmed'
                ];
            }
            default:
                break;
        }
    }
}
