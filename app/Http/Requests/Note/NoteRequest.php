<?php

namespace DCODESupport\Http\Requests\Note;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Input;

class NoteRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return \Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = $this->rules;
        $rules['message'] = 'required';

        if(Input::get('name') !== null){
            $rules['name'] = 'required';
        }

        if(Input::get('comment') !== null){
            $rules['comment'] = 'required';
        }

        return $rules;
    }
}
