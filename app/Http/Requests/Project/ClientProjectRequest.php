<?php

namespace DCODESupport\Http\Requests\Project;

use Illuminate\Foundation\Http\FormRequest;

class ClientProjectRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return \Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'client_id' => 'required|exists:clients,id',
            'code' => 'max:255',
            'name' => 'required|max:255'
        ];
    }
}
