<?php

namespace DCODESupport\Http\Requests\Project;

use DCODESupport\Http\Requests\Request;

class ProjectRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return \Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'client_id' => 'required|exists:clients,id',
            'code' => 'required|max:255',
            'name' => 'required|max:255'
        ];
    }
}
