<?php

namespace DCODESupport\Http\Requests\Ticket;

use DCODESupport\Http\Requests\Request;

class TicketRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return \Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'client_id'=>'required | not_in:0',
            'name' => 'required',
            'comment' => 'required'
        ];
    }

    /**
     * Customize the validation messages
     *
     * @return array
     */
    public function messages(){

        return[
            'client_id.required'=>'Client is Required',
            'name.required'=>'Ticket Name is Required',
            'comment.required'=>'Ticket Comment cannot be blank'
        ];
    }
}
