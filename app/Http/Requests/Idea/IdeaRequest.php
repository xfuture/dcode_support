<?php

namespace DCODESupport\Http\Requests\Idea;

use DCODESupport\Http\Requests\Request;

class IdeaRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return \Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'client_id' => 'required | not_in: 0',
            'name' => 'required',
            'comment' => 'required'
        ];
    }

    /**
     * Customize the validation messages
     *
     * @return array
     */
    public function messages(){

        return[
            'client_id.required'=>'Client is Required',
            'name.required'=>'Idea Name is Required',
            'comment.required'=>'Idea Comment cannot be blank'
        ];
    }
}
