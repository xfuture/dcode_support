<?php

namespace DCODESupport\Http\Controllers;

use Illuminate\Http\Request;

use DCODESupport\Http\Requests;

class HomeController extends Controller
{
    public function index()
    {
	    return view('site.home');
    }
}
