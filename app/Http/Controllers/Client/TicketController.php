<?php

namespace DCODESupport\Http\Controllers\Client;

use DCODESupport\Models\Status;
use DCODESupport\Models\Ticket\Ticket;
use DCODESupport\Services\Comment\CommentService;
use DCODESupport\Services\Ticket\TicketService;
use Illuminate\Http\Request;

use DCODESupport\Http\Requests;
use DCODESupport\Http\Requests\Comment\CommentRequest;
use DCODESupport\Http\Requests\Ticket\TicketRequest;
use DCODESupport\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;

class TicketController extends Controller
{
	protected $service;

	public function __construct()
	{
		$this->service = new TicketService();
	}

	public function index()
	{

        $pendingTickets = $this->service->search(null, null, '0', \Auth::user());
        $outstandingTickets = $this->service->search(null, null, '1', \Auth::user());
        $completedTickets = $this->service->search(null, null, '2', \Auth::user());
        $approvedTickets = $this->service->search(null, null, '3', \Auth::user());

        return view('client.task.list')
            ->with('pendingTickets', $pendingTickets)
            ->with('outstandingTickets', $outstandingTickets)
            ->with('completedTickets', $completedTickets)
            ->with('approvedTickets', $approvedTickets);
	}

	public function create()
	{
		return view('client.ticket.edit');
	}

	public function store(TicketRequest $request)
	{
		$this->service->save($request, null);

        if(request()->ajax() | $request->wantsJson()){
            //Ajax response
            $request->session()->flash('flashNotice', 'Ticket saved ...');
            return response()->json(['url'=>'task']);

        }else{
            return redirect()
                ->route('client.task')
                ->with('flashNotice', 'Ticket saved ...');
        }
	}

	public function show(Ticket $ticket)
	{
		if (!$ticket->access(\Auth::user()))
		{
			return redirect()
				->route('client.ticket')
				->with('flashNoticeError', 'Unable to access ticket ...');
		}

		return view('client.ticket.show')
			->with('ticket', $ticket);
	}

	public function edit(Ticket $ticket)
	{
		if (!$ticket->access(\Auth::user()))
		{
			return redirect()
				->route('client.ticket')
				->with('flashNoticeError', 'Unable to access ticket ...');
		}

		return view('client.ticket.edit')
			->with('ticket', $ticket);
	}

	public function update(TicketRequest $request, Ticket $ticket)
	{
		if (!$ticket->access(\Auth::user()))
		{
			return redirect()
				->route('client.ticket')
				->with('flashNoticeError', 'Unable to access ticket ...');
		}

		$this->service->save($request, $ticket);

		return redirect()
			->route('client.ticket')
			->with('flashNotice', 'Ticket saved ...');
	}

	public function destroy(Ticket $ticket)
	{
		if (!$ticket->access(\Auth::user()))
		{
			return redirect()
				->route('client.ticket')
				->with('flashNoticeError', 'Unable to access ticket ...');
		}

		$this->service->delete($ticket);

		return redirect()
			->route('client.ticket')
			->with('flashNotice', 'Ticket deleted ...');
	}

	// General ...
	public function comment(CommentRequest $request, Ticket $ticket)
	{
		if (!$ticket->access(\Auth::user()))
		{
			return redirect()
				->route('client.ticket')
				->with('flashNoticeError', 'Unable to access ticket ...');
		}

		CommentService::save($request, get_class($ticket), $ticket->id, null);

		return redirect()
			->route('client.ticket.show', $ticket->id)
			->with('flashNotice', 'Comment added to ticket ...');
	}

	public function preview()
	{
		$ticket = Ticket::find(\Request::get('ticketId'));

		if (!$ticket->access(\Auth::user()))
		{
			return redirect()
				->route('client.ticket')
				->with('flashNoticeError', 'Unable to access ticket ...');
		}

		return view('client.ticket.preview')
			->with('ticket', $ticket);
	}

	public function status()
	{
	    //Taking the selected ticket ID
		$ticket = Ticket::find(\Request::get('id'));

        //Taking the drop down selected value
       	$status = Status::find(\Request::get('statusSelect'));
		if($status==null){

            return redirect()
                ->route('client.task');
        }

		$this->service->updateStatus($ticket, $status);

		//return response()
		//	->json(['id' => $ticket->id, 'currentStatus' => $status->name]);
        return redirect()
            ->route('client.task')
            ->with('flashNotice', 'Ticket status updated ...');
	}
}
