<?php

namespace DCODESupport\Http\Controllers\Client;

use DCODESupport\Services\Project\ProjectService;

use Illuminate\Http\Request;

use DCODESupport\Http\Requests;
use DCODESupport\Models\Client\Client;
use DCODESupport\Models\Project\Project;
use DCODESupport\Models\Project\ProjectUser;
use DCODESupport\Services\NavigationService;
use DCODESupport\User;
use DCODESupport\Http\Requests\Project\ClientProjectRequest;
use DCODESupport\Http\Controllers\Controller;

class ProjectController extends Controller
{
	protected $service;
	
	public function __construct()
	{
		$this->service = new ProjectService();
	}
	
	public function index()
	{
		$projects = $this->service->search(\Auth::user());
		
		return view('client.project.list')
			->with('projects', $projects);
	}

    public function create()
    {
        return view('client.project.edit');
    }

    public function store(ClientProjectRequest $request)
    {

        $client = Client::find($request->get('client_id'));

        $request->code=ProjectService::generateProjectCode($client->id);

       // $newProject = $this->service->save($request, $client, null);

        // Add permission for client to save a project ...
        $newProject = new Project();
        $newProject->client_id = $request->client_id;
        $newProject->code = ProjectService::generateProjectCode($client->id);
        $newProject->name = $request->name;
        $newProject->active = $request->active;
        // client is not the user
        $newProject->user_id = auth()->id(); // $request->client_id;

        $newProject->save();

        $projectUser = ProjectUser::firstOrNew([
            'project_id' => $newProject->id,
            'access_user_id' => auth()->id()
        ]);
        $projectUser->subscribe = 0;
        $projectUser->user_id = auth()->id();
        $projectUser->save();

        //Check if the request is ajax
        if ($request->ajax() || $request->wantsJson()){
            //Ajax request response
            $request->session()->flash('flashNotice', 'Project saved ...');
            return response()->json(['url'=>'project']);

        }else{
            $defaultRoute = route('client.project');
            return NavigationService::returnPosition('projects', $defaultRoute, 'Project saved.');
        }
    }

}
