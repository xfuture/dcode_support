<?php

namespace DCODESupport\Http\Controllers\Client;

use Illuminate\Http\Request;

use DCODESupport\Http\Requests;
use DCODESupport\Http\Controllers\Controller;

class ClientController extends Controller
{
	public function index()
	{
		return view('client.dashboard');
	}
}
