<?php

namespace DCODESupport\Http\Controllers\Client;

use DCODESupport\Services\Ticket\TicketService;
use Illuminate\Http\Request;

use DCODESupport\Http\Requests;
use DCODESupport\Http\Controllers\Controller;

class TaskController extends Controller
{
	protected $service;
	
	public function __construct()
	{
		$this->service = new TicketService();
	}
	
	public function index()
	{
		$pendingTickets = $this->service->search(null, null, '0', \Auth::user());
		$outstandingTickets = $this->service->search(null, null, '1', \Auth::user());
		$completedTickets = $this->service->search(null, null, '2', \Auth::user());
		$approvedTickets = $this->service->search(null, null, '3', \Auth::user());
		
		return view('client.task.list')
			->with('pendingTickets', $pendingTickets)
			->with('outstandingTickets', $outstandingTickets)
			->with('completedTickets', $completedTickets)
			->with('approvedTickets', $approvedTickets);
	}
}
