<?php

namespace DCODESupport\Http\Controllers\Client;

use DCODESupport\Models\Status;
use DCODESupport\Models\Idea\Idea;
use DCODESupport\Services\Comment\CommentService;
use DCODESupport\Services\Idea\IdeaService;

use Illuminate\Http\Request;

use DCODESupport\Http\Requests;
use DCODESupport\Http\Requests\Comment\CommentRequest;
use DCODESupport\Http\Requests\Idea\IdeaRequest;
use DCODESupport\Http\Controllers\Controller;

class IdeaController extends Controller
{
    protected $service;

    public function __construct()
    {
        $this->service = new IdeaService();
    }

    public function index()
    {
        // search for idea
        $ideas = $this->service->search(\Auth::user());

        return view('client.idea.list')
            ->with('ideas', $ideas);

    }

    public function create()
    {
        // Views the client/idea/edit form
       return view('client.idea.edit');
    }


    public function update(IdeaRequest $request, Idea $idea)
    {
        if (!$idea->access(\Auth::user()))
        {
            return redirect()
                ->route('client.idea')
                ->with('flashNoticeError', 'Unable to access idea ...');
        }
        IdeaService::save($request, $idea);

        //Check if the request is ajax
        if(request()->ajax() | $request->wantsJson()){
            //Ajax response
            $request->session()->flash('flashNotice', 'Idea saved ...');
            return response()->json('');
          /*  return response()->json('');  */
        }else{
            return redirect()
                ->route('client.idea',$idea->id)
                ->with('flashNotice', 'Idea saved ...');
        }
    }


    public function store(IdeaRequest $request)
    {
        IdeaService::save($request, null);
        //Check if the request is ajax
        if(request()->ajax() | $request->wantsJson()){
            //Ajax response
            $request->session()->flash('flashNotice', 'Idea saved ...');

            return response()->json(['url'=>'idea']);

        }else{
            return redirect()
                ->route('client.idea')
                ->with('flashNotice', 'Idea saved ...');
        }

    }
    public function preview()
    {
        $idea = Idea::find(\Request::get('ideaId'));

        if (!$idea->access(\Auth::user()))
        {
            return redirect()
                ->route('client.idea')
                ->with('flashNoticeError', 'Unable to access idea ...');
        }

        return view('client.idea.preview')
            ->with('idea', $idea);
    }


    public function edit(Idea $idea)
    {

        if (!$idea->access(\Auth::user()))
        {
            return redirect()
                ->route('client.idea')
                ->with('flashNoticeError', 'Unable to access ida ...');
        }

        return view('client.idea.edit')
            ->with('idea', $idea);

    }

    public function destroy(Idea $idea)
    {
        IdeaService::delete($idea);

        return redirect()
            ->route('client.idea')
            ->with('flashNotice', 'Idea deleted ...');
    }

    public function show(Idea $idea)
    {
        return view('client.idea.show')
            ->with('idea', $idea);
    }
    public function comment(CommentRequest $request, Idea $idea)
    {
        CommentService::save($request, get_class($idea), $idea->id, null);

        return redirect()
            ->route('client.idea', $idea->id)
            ->with('flashNotice', 'Comment added to idea ...');
    }
}


