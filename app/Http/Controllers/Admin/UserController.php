<?php

namespace DCODESupport\Http\Controllers\Admin;

use DCODESupport\Services\User\UserService;
use DCODESupport\User;
use Illuminate\Http\Request;

use DCODESupport\Http\Requests;
use DCODESupport\Http\Requests\User\UserRequest;
use DCODESupport\Http\Controllers\Controller;

class UserController extends Controller
{
    protected $service;

	public function __construct()
	{
		$this->service = new UserService();
	}

	public function index()
	{
		$users = $this->service->search();

		return view('admin.user.list')
			->with('users', $users);
	}

	public function create()
	{
		return view('admin.user.edit');
	}

	public function store(UserRequest $request)
	{
		$this->service->save($request, null);

        //Check if the request is ajax
        if ($request->ajax() || $request->wantsJson()){
            //Ajax request response
            return response()->json('');
        }else{
            return redirect()
                ->route('admin.user')
                ->with('flashNotice', 'User saved ...');
        }
	}

	public function edit(User $user)
	{
		return view('admin.user.edit')
			->with('user', $user);
	}

	public function update(UserRequest $request, User $user)
	{
		$this->service->save($request, $user);

        //Check if the request is ajax
        if ($request->ajax() || $request->wantsJson()){
            //Ajax request response
            return response()->json('');
        }else{
            return redirect()
                ->route('admin.user')
                ->with('flashNotice', 'User saved ...');
        }
	}

	public function destroy(User $user)
	{
		$this->service->delete($user);

		return redirect()
			->route('admin.user')
			->with('flashNotice', 'User deleted ...');
	}
}
