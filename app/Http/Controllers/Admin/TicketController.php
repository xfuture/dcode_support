<?php

namespace DCODESupport\Http\Controllers\Admin;

use DCODESupport\Models\Status;
use DCODESupport\Models\Ticket\Ticket;
use DCODESupport\Services\Comment\CommentService;
use DCODESupport\Services\Ticket\TicketService;
//use Illuminate\Http\Request;
use Illuminate\Support\Facades\Request;

use DCODESupport\Http\Requests;
use DCODESupport\Http\Requests\Comment\CommentRequest;
use DCODESupport\Http\Requests\Ticket\TicketRequest;
use DCODESupport\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;

class TicketController extends Controller
{
	
	protected $service;
	
	public function __construct()
	{
		$this->service = new TicketService();
	}
	
	public function index()
	{
		$tickets = $this->service->search();
		
		//        dd($tickets);
		return view('admin.ticket.list')
			->with('tickets', $tickets);
	}
	
	public function create()
	{
		$client_id  = Request::input('client_id');
		$project_id = Request::input('project_id');
		
		return view('admin.ticket.edit', ['client_id' => $client_id, 'project_id' => $project_id]);
	}
	
	/**
	 * @param TicketRequest $request
	 *
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function store(TicketRequest $request)
	{
		
		$this->service->save($request, null);
		//Check if the request is ajax
		if ($request->ajax() || $request->wantsJson())
		{
			//Ajax request response
            $request->session()->flash('flashNotice', 'Ticket saved ...');
            return response()->json(['url'=>'ticket']);
		}
		else
		{
			
			// Save the ticket (new ticket)
			$this->service->save($request, null);
			
			if ($request->ajax())
			{
				// Request is AJAX
				return response()
					->json(['result' => 'OK']);
			}
			else
			{
				// Request is via Laravel
				
				return redirect()
					->route('admin.ticket')
					->with('flashNotice', 'Ticket saved ...');
			}
		}
	}
	
	public function show(Ticket $ticket)
	{
		
		if (!$ticket->access(\Auth::user()))
		{
			return redirect()
				->route('admin.ticket')
				->with('flashNoticeError', 'Unable to access ticket ...');
		}
		
		return view('admin.ticket.show')
			->with('ticket', $ticket);
	}
	
	public function edit(Ticket $ticket)
	{
		if (!$ticket->access(\Auth::user()))
		{
			return redirect()
				->route('admin.ticket')
				->with('flashNoticeError', 'Unable to access ticket ...');
		}
		
		return view('admin.ticket.edit')
			->with('ticket', $ticket);
	}
	
	public function update(TicketRequest $request, Ticket $ticket)
	{
		if (!$ticket->access(\Auth::user()))
		{
			return redirect()
				->route('admin.ticket')
				->with('flashNoticeError', 'Unable to access ticket ...');
		}
		
		$this->service->save($request, $ticket);
		
		//Check if the request is ajax
		if ($request->ajax() || $request->wantsJson())
		{
			//Ajax request response
            $request->session()->flash('flashNotice', 'Ticket saved ...');
            return response()->json(['url'=>'ticket']);
		}
		else
		{
			return redirect()
				->route('admin.ticket')
				->with('flashNotice', 'Ticket saved ...');
		}
	}
	
	public function destroy(Ticket $ticket)
	{
		if (!$ticket->access(\Auth::user()))
		{
			return redirect()
				->route('admin.ticket')
				->with('flashNoticeError', 'Unable to access ticket ...');
		}
		
		$this->service->delete($ticket);
		
		return redirect()
			->route('admin.ticket')
			->with('flashNotice', 'Ticket deleted ...');
	}
	
	// General ...
	public function comment(CommentRequest $request, Ticket $ticket)
	{
		if (!$ticket->access(\Auth::user()))
		{
			return redirect()
				->route('admin.ticket')
				->with('flashNoticeError', 'Unable to access ticket ...');
		}
		
		CommentService::save($request, get_class($ticket), $ticket->id, null);
		
		return redirect()
			->route('admin.ticket.show', $ticket->id)
			->with('flashNotice', 'Comment added to ticket ...');
	}
	
	public function preview()
	{
		$ticket = Ticket::find(\Request::get('ticketId'));
		
		if (!$ticket->access(\Auth::user()))
		{
			return redirect()
				->route('admin.ticket')
				->with('flashNoticeError', 'Unable to access ticket ...');
		}
		
		return view('admin.ticket.preview')
			->with('ticket', $ticket);
	}
	
	public function status()
	{
		$ticket = Ticket::find(\Request::get('ticketId'));
		$status = Status::find(\Request::get('statusId'));
		
		$this->service->updateStatus($ticket, $status);
		
		return response()->json(['id' => $ticket->id, 'currentStatus' => isset($status) ? $status->name : '']);
	}
}
