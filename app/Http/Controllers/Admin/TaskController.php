<?php

namespace DCODESupport\Http\Controllers\Admin;

use DCODESupport\Services\Ticket\TicketService;
use Illuminate\Http\Request;

use DCODESupport\Http\Requests;
use DCODESupport\Http\Controllers\Controller;

class TaskController extends Controller
{
	
	protected $service;
	
	public function __construct()
	{
		$this->service = new TicketService();
	}
	
	public function index()
	{
		
		$pendingTickets = $this->service->search(null, null, '0');
		//dd($pendingTickets);
		//dd(\DB::getQueryLog());
		//\DB::enableQueryLog();
		$outstandingTickets = $this->service->search(null, null, '1');
		//	dd($outstandingTickets);
		$completedTickets = $this->service->search(null, null, '2');
		//	dd($completedTickets);
		$approvedTickets = $this->service->search(null, null, '3');
		// dd($approvedTickets);
		
		$projects      = TicketService::projects();
		$assignedUsers = TicketService::users();
		
		return view('admin.task.list')
			->with('projects', $projects)
			->with('assignedUsers', $assignedUsers)
			->with('pendingTickets', $pendingTickets)
			->with('outstandingTickets', $outstandingTickets)
			->with('completedTickets', $completedTickets)
			->with('approvedTickets', $approvedTickets);
	}
}
