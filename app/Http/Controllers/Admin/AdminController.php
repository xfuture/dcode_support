<?php

namespace DCODESupport\Http\Controllers\Admin;

use DCODESupport\Services\Project\ProjectMilestoneService;
use DCODESupport\Services\Project\ProjectService;
use Illuminate\Http\Request;

use DCODESupport\Http\Requests;
use DCODESupport\Http\Controllers\Controller;

class AdminController extends Controller
{
 /*   public function index()
    {
        $incompleteMilestones = ProjectMilestoneService::getIncompleteMilestones();

        return view('admin.dashboard')
            ->with('incompleteMilestones', $incompleteMilestones);
    }
*/
    public function index()
    {
        $incompleteMilestones = ProjectMilestoneService::getIncompleteMilestones();
        $activeProjects=ProjectService::getActiveProjects();

        return view('admin.testDashboard')
            ->with('incompleteMilestones', $incompleteMilestones)
            ->with('activeProjects',$activeProjects);
    }
}
