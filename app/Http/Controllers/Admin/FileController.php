<?php

namespace DCODESupport\Http\Controllers\Admin;

use DCODESupport\Http\Requests\File\FileRequest;
use DCODESupport\Services\AWS\AWSService;
use DCODESupport\Services\File\FileService;
use Illuminate\Http\Request;
use DCODESupport\Http\Controllers\Controller;

class FileController extends Controller
{
    protected $service;

    public function __construct()
    {
        $this->service = new FileService();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * @param FileRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(FileRequest $request)
    {

        $file = $this->service->save($request);
        if(request()->ajax() | $request->wantsJson()){
            //Ajax response
            $signedUrl = AWSService::getSignedURL(FileService::getTaskAttachedFilesURL() . $file->hash_name . '/' . $file->original_name, 1200);
            return response()->json(['file' => $file, 'signedUrl' => $signedUrl]);
        }else{
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
