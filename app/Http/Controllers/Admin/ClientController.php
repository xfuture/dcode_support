<?php

namespace DCODESupport\Http\Controllers\Admin;

use DCODESupport\Models\Client\Client;
use DCODESupport\Models\Project\Project;
use DCODESupport\Services\Client\ClientService;
use DCODESupport\Services\Project\ProjectService;
use Illuminate\Http\Request;

use DCODESupport\Http\Requests;
use DCODESupport\Http\Requests\Client\ClientRequest;
use DCODESupport\Http\Controllers\Controller;


class ClientController extends Controller
{
    protected $service;

	public function __construct()
	{
		$this->service = new ClientService();
	}

	public function index()
	{
		$clients = $this->service->search();

		return view('admin.client.list')
			->with('clients', $clients);
	}

	public function show(Client $client){
        $projects = ProjectService::search(null, $client);

	    return view('admin.client.show')
            ->with('client', $client)
            ->with('projects', $projects)
            ->with('panel2', '');
    }

	public function create()
	{
		return view('admin.client.edit');
	}

	public function store(ClientRequest $request)
	{
		$this->service->save($request, null);

        //Check if the request is ajax
        if ($request->ajax() || $request->wantsJson()){
            //Ajax request response
            return response()->json('');
        }else{
            return redirect()
                ->route('admin.client')
                ->with('flashNotice', 'Client saved ...');
        }
	}

	public function edit(Client $client)
	{
		return view('admin.client.edit')
			->with('client', $client);
	}

	public function update(ClientRequest $request, Client $client)
	{
		$this->service->save($request, $client);

        //Check if the request is ajax
        if ($request->ajax() || $request->wantsJson()){
            //Ajax request response
            return response()->json('');
        }else{
            return redirect()
                ->route('admin.client')
                ->with('flashNotice', 'Client saved ...');
        }
	}

	public function destroy(Client $client)
	{
		$this->service->delete($client);

		return redirect()
			->route('admin.client')
			->with('flashNotice', 'Client deleted ...');
	}
}
