<?php

namespace DCODESupport\Http\Controllers\Admin;

use DCODESupport\Models\Idea\Idea;
use DCODESupport\Services\Comment\CommentService;
use DCODESupport\Services\Idea\IdeaService;
use Illuminate\Http\Request;

use DCODESupport\Http\Requests;
use DCODESupport\Http\Requests\Comment\CommentRequest;
use DCODESupport\Http\Requests\Idea\IdeaRequest;
use DCODESupport\Http\Controllers\Controller;

class IdeaController extends Controller
{
    public function index()
    {
	    $ideas = IdeaService::search();

	    return view('admin.idea.list')
		    ->with('ideas', $ideas);
    }

	public function create()
	{
		return view('admin.idea.edit');
	}

	public function store(IdeaRequest $request)
	{
		IdeaService::save($request, null);
        //Check if the request is ajax
        if(request()->ajax() | $request->wantsJson()){
            //Ajax response
            $request->session()->flash('flashNotice', 'Idea saved ...');
            return response()->json(['url'=>'idea']);
        }else{
            return redirect()
                ->route('admin.idea')
                ->with('flashNotice', 'Idea saved ...');
        }
	}

	public function show(Idea $idea)
	{
		return view('admin.idea.show')
			->with('idea', $idea);
	}

	public function edit(Idea $idea)
	{
		return view('admin.idea.edit')
			->with('idea', $idea);
	}

	public function update(IdeaRequest $request, Idea $idea)
	{
		IdeaService::save($request, $idea);

        //Check if the request is ajax
        if(request()->ajax() | $request->wantsJson()){
            //Ajax response
            $request->session()->flash('flashNotice', 'Idea saved ...');
            return response()->json(['url'=>'idea']);
        }else{
            return redirect()
                ->route('admin.idea')
                ->with('flashNotice', 'Idea saved ...');
        }
	}

	public function destroy(Idea $idea)
	{
		IdeaService::delete($idea);

		return redirect()
			->route('admin.idea')
			->with('flashNotice', 'Idea deleted ...');
	}

	public function comment(CommentRequest $request, Idea $idea)
	{
		CommentService::save($request, get_class($idea), $idea->id, null);

		return redirect()
			->route('admin.idea.show', $idea->id)
			->with('flashNotice', 'Comment added to idea ...');
	}
}
