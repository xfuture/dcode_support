<?php

namespace DCODESupport\Http\Controllers\Admin;

use DCODESupport\Models\Project\Project;
use DCODESupport\Models\Ticket\Ticket;
use DCODESupport\Models\Timesheet\Timesheet;
use DCODESupport\Services\Timesheet\TimesheetService;
use Illuminate\Http\Request;

use DCODESupport\Http\Requests;
use DCODESupport\Http\Requests\Timesheet\TimesheetRequest;
use DCODESupport\Http\Controllers\Controller;

class TimesheetController extends Controller
{
	
	protected $service;
	
	public function __construct()
	{
		$this->service = new TimesheetService();
	}
	
	public function index()
	{
		$timesheets = $this->service->search();
		
		$billedHours = $this->service->search(true)->where('billed', 1)->sum('hours');
		$totalHours  = $this->service->search(true)->sum('hours');
		
		return view('admin.timesheet.list')
			->with('timesheets', $timesheets)
			->with('billedHours', $billedHours)
			->with('totalHours', $totalHours);
		
	}
	
	public function create(Ticket $ticket = null)
	{
		return view('admin.timesheet.edit')
			->with('ticket', $ticket);
	}
	
	public function store(TimesheetRequest $request)
	{
		$project = Project::find($request->get('project_id'));
		
		$this->service->save($request, $project, null);
		
		//Check if the request is ajax
		if ($request->ajax() || $request->wantsJson())
		{
			//Ajax request response
            $request->session()->flash('flashNotice', 'Timesheet saved ...');
            return response()->json(['url'=>'timesheet']);
		}
		else
		{
			return redirect()
				->route('admin.timesheet')
				->with('flashNotice', 'Timesheet saved ...');
		}
	}
	
	public function edit(Timesheet $timesheet)
	{
		if (!$timesheet->access(\Auth::user()))
		{
			return redirect()
				->route('admin.timesheet')
				->with('flashNoticeError', 'Unable to access timesheet ...');
		}
		
		return view('admin.timesheet.edit')
			->with('timesheet', $timesheet);
	}
	
	public function update(TimesheetRequest $request, Timesheet $timesheet)
	{
		if (!$timesheet->access(\Auth::user()))
		{
			return redirect()
				->route('admin.timesheet')
				->with('flashNoticeError', 'Unable to access timesheet ...');
		}
		
		$project = $timesheet->project;
		
		$this->service->save($request, $project, $timesheet);
		
		//Check if the request is ajax
		if ($request->ajax() || $request->wantsJson())
		{
			//Ajax request response
            $request->session()->flash('flashNotice', 'Timesheet saved ...');
            return response()->json(['url'=>'timesheet']);
		}
		else
		{
			return redirect()
				->route('admin.timesheet')
				->with('flashNotice', 'Timesheet saved ...');
		}
	}
	
	public function destroy(Timesheet $timesheet)
	{
		if (!$timesheet->access(\Auth::user()))
		{
			return redirect()
				->route('admin.timesheet')
				->with('flashNoticeError', 'Unable to access timesheet ...');
		}
		
		$this->service->delete($timesheet);
		
		return redirect()
			->route('admin.timesheet')
			->with('flashNotice', 'Timesheet deleted ...');
	}
}
