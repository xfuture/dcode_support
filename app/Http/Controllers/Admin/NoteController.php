<?php

namespace DCODESupport\Http\Controllers\Admin;

use DCODESupport\Http\Requests\Note\NoteRequest;
use DCODESupport\Models\Note\Note;
use DCODESupport\User;
use Illuminate\Http\Request;
use DCODESupport\Http\Controllers\Controller;
use DCODESupport\Services\Note\NoteService;

class NoteController extends Controller
{
    public function index(User $user = null)
    {
        $notes = NoteService::search($user);

        return view('admin.note.list')
            ->with('panel2', '')
            ->with('notes', $notes)
            ->with('user', $user);
    }

    public function create(User $user)
    {
        return view('admin.note.edit')
            ->with('user', $user);
    }

    public function store(NoteRequest $request, User $user)
    {
        NoteService::save($request, $user);
        //Check if the request is ajax
        if(request()->ajax() | $request->wantsJson()){
            //Ajax response
            $request->session()->flash('flashNotice', 'Note saved ...');
            return response()->json(['url'=>'note']);
        }else{
            return redirect()
                ->route('admin.note')
                ->with('user', $user)
                ->with('flashNotice', 'Note saved ...');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    public function edit(Note $note)
    {
        return view('admin.note.edit')
            ->with('note', $note);
    }

    public function update(NoteRequest $request, Note $note)
    {
        NoteService::save($request, $note->user ,$note);
        //Check if the request is ajax
        if(request()->ajax() | $request->wantsJson()){
            //Ajax response
            $request->session()->flash('flashNotice', 'Note updated ...');
            return response()->json(['url'=>'note']);
        }else{
            return redirect()
                ->route('admin.note')
                ->with('flashNotice', 'Note saved ...');
        }
    }

    public function destroy(Note $note)
    {
        $user = $note->user;
        NoteService::destroy($note);

        return redirect()
            ->route('admin.note', $user->id)
            ->with('flashNotice', 'Note deleted ...');
    }
}
