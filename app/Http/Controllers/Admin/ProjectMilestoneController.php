<?php

namespace DCODESupport\Http\Controllers\Admin;

use DCODESupport\Models\Project\Project;
use DCODESupport\Models\Project\ProjectMilestone;
use DCODESupport\Services\Project\ProjectMilestoneService;
use DCODESupport\Http\Requests\Project\ProjectMilestoneRequest;
use DCODESupport\Http\Controllers\Controller;

class ProjectMilestoneController extends Controller
{
    protected $service;

	public function __construct()
	{
		$this->service = new ProjectMilestoneService();
	}

	public function index(Project $project)
	{
		$projectMilestones = $this->service->search($project);

		return view('admin.project-milestone.list')
			->with('project', $project)
			->with('projectMilestones', $projectMilestones);
	}

	public function add(Project $project)
	{
		return view('admin.project-milestone.edit')
			->with('project', $project);
	}

	public function save(ProjectMilestoneRequest $request, Project $project)
	{
		$this->service->save($request, $project, null);

        //Check if the request is ajax
        if ($request->ajax() || $request->wantsJson()){
            //Ajax request response
            return response()->json('');
        }else{
            return redirect()
                ->route('admin.project-milestone', $project->id)
                ->with('flashNotice', 'Project Milestone added ...');
        }
	}

	public function edit(ProjectMilestone $projectMilestone)
	{
		$project = $projectMilestone->project;

		return view('admin.project-milestone.edit')
			->with('project', $project)
			->with('projectMilestone', $projectMilestone);
	}

	public function update(ProjectMilestoneRequest $request, ProjectMilestone $projectMilestone)
	{
		$project = $projectMilestone->project;

		$this->service->save($request, $project, $projectMilestone);

        //Check if the request is ajax
        if ($request->ajax() || $request->wantsJson()){
            //Ajax request response
            return response()->json('');
        }else{
            return redirect()
                ->route('admin.project-milestone', $project->id)
                ->with('flashNotice', 'Project Milestone updated ...');
        }
	}

	public function destroy(ProjectMilestone $projectMilestone)
	{
		$project = $projectMilestone->project;

		$this->service->delete($projectMilestone);

		return redirect()
			->route('admin.project-milestone', $project->id)
			->with('flashNotice', 'Project Milestone deleted ...');
	}
}
