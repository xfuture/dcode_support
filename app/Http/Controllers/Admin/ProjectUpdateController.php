<?php

namespace DCODESupport\Http\Controllers\Admin;

use DCODESupport\Models\Project\Project;
use DCODESupport\Models\Project\ProjectUpdate;
use DCODESupport\Services\Comment\CommentService;
use DCODESupport\Services\Project\ProjectUpdateService;
use Illuminate\Http\Request;

use DCODESupport\Http\Requests;
use DCODESupport\Http\Requests\Comment\CommentRequest;
use DCODESupport\Http\Requests\Project\ProjectUpdateRequest;
use DCODESupport\Http\Controllers\Controller;

class ProjectUpdateController extends Controller
{
	protected $service;
	
	public function __construct()
	{
		$this->service = new ProjectUpdateService();
	}
	
	public function index(Project $project)
	{
		$projectUpdates = $this->service->search($project);
		
		return view('admin.project-update.list')
			->with('project', $project)
			->with('projectUpdates', $projectUpdates);
	}
	
	public function add(Project $project)
	{
		return view('admin.project-update.edit')
			->with('project', $project);
	}
	
	public function save(ProjectUpdateRequest $request, Project $project)
	{
		$this->service->save($request, $project, null);

        //Check if the request is ajax
        if ($request->ajax() || $request->wantsJson()){
            //Ajax request response
            return response()->json('');
        }else{
            return redirect()
                ->route('admin.project-update', $project->id)
                ->with('flashNotice', 'Project Update added ...');
        }
	}

	public function show(ProjectUpdate $projectUpdate)
	{
		$project = $projectUpdate->project;

		return view('admin.project-update.show')
			->with('project', $project)
			->with('projectUpdate', $projectUpdate);
	}
	
	public function edit(ProjectUpdate $projectUpdate)
	{
		$project = $projectUpdate->project;
		
		return view('admin.project-update.edit')
			->with('project', $project)
			->with('projectUpdate', $projectUpdate);
	}
	
	public function update(ProjectUpdateRequest $request, ProjectUpdate $projectUpdate)
	{
		$project = $projectUpdate->project;
		
		$this->service->save($request, $project, $projectUpdate);

        //Check if the request is ajax
        if ($request->ajax() || $request->wantsJson()){
            //Ajax request response
            return response()->json('');
        }else{
            return redirect()
                ->route('admin.project-update', $project->id)
                ->with('flashNotice', 'Project Update updated ...');
        }
	}
	
	public function destroy(ProjectUpdate $projectUpdate)
	{
		$project = $projectUpdate->project;
		
		$this->service->delete($projectUpdate);
		
		return redirect()
			->route('admin.project-update', $project->id)
			->with('flashNotice', 'Project Update deleted ...');
	}

	public function comment(CommentRequest $request, ProjectUpdate $projectUpdate)
	{
		CommentService::save($request, get_class($projectUpdate), $projectUpdate->id, null);

		return redirect()
			->route('admin.project-update.show', $projectUpdate->id)
			->with('flashNotice', 'Comment added to project update ...');
	}
}
