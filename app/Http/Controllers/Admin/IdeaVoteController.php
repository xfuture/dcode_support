<?php

namespace DCODESupport\Http\Controllers\Admin;

use DCODESupport\Http\Requests\Idea\IdeaVoteRequest;
use DCODESupport\Models\Idea\Idea;
use DCODESupport\Models\Idea\IdeaVote;
use DCODESupport\Services\Idea\IdeaVoteService;
use DCODESupport\Http\Controllers\Controller;
use \Illuminate\Support\Facades\Request;

class IdeaVoteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param IdeaVoteRequest $request
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse
     */
    public function store(IdeaVoteRequest $request)
    {
        IdeaVoteService::save($request, null);
        //Check if the request is ajax
        if(request()->ajax() | $request->wantsJson()){
            //Ajax response
            return response()->json($request->get('idea_id'));
        }else{
            return redirect()
                ->route('admin.idea');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \DCODESupport\Models\Idea\IdeaVote  $ideaVote
     * @return \Illuminate\Http\Response
     */
    public function show(IdeaVote $ideaVote)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \DCODESupport\Models\Idea\IdeaVote  $ideaVote
     * @return \Illuminate\Http\Response
     */
    public function edit(IdeaVote $ideaVote)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \DCODESupport\Models\Idea\IdeaVote  $ideaVote
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, IdeaVote $ideaVote)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param IdeaVoteRequest $request
     * @param IdeaVote $ideaVote
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse
     */
    public function destroy(IdeaVoteRequest $request, IdeaVote $ideaVote)
    {
        IdeaVoteService::delete($ideaVote);

        if(request()->ajax() | $request->wantsJson()){
            //Ajax response
            return response()->json('');
        }else{
            return redirect()
                ->route('admin.idea');
        }
    }

    /**
     * @param IdeaVoteRequest $request
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse
     */
    public function vote(IdeaVoteRequest $request)
    {
        $idea = Idea::find($request->get('idea_id'));

        $vote = IdeaVoteService::vote($idea, auth()->user());
        //Check if the request is ajax
        if(request()->ajax() | $request->wantsJson()){
            //Ajax response
            return response()->json($vote);
        }else{
            return redirect()
                ->route('admin.idea');
        }
    }
}
