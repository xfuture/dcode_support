<?php

namespace DCODESupport\Http\Controllers\Admin;

//use DCODESupport\Http\Requests\Request;
use \Illuminate\Support\Facades\Request;

use DCODESupport\Models\Client\Client;
use DCODESupport\Models\Project\Project;
use DCODESupport\Models\Project\ProjectUser;
use DCODESupport\Services\NavigationService;
use DCODESupport\Services\Project\ProjectService;
use DCODESupport\User;
use DCODESupport\Http\Requests\Project\ProjectRequest;
use DCODESupport\Http\Controllers\Controller;

class ProjectController extends Controller
{
	protected $service;
	
	public function __construct()
	{
		$this->service = new ProjectService();
	}
	
	public function index()
	{
        NavigationService::setPosition('projects');

		$projects = $this->service->search();
		
		return view('admin.project.list')
			->with('projects', $projects);
	}
	
	public function create()
	{
		return view('admin.project.edit');
	}
	
	public function store(ProjectRequest $request)
	{
		$client = Client::find($request->get('client_id'));

		$this->service->save($request, $client, null);

        //Check if the request is ajax
        if ($request->ajax() || $request->wantsJson()){
            //Ajax request response
            return response()->json('');
        }else{
            $defaultRoute = route('admin.project');
            return NavigationService::returnPosition('projects', $defaultRoute, 'Project saved.');
        }
	}

	public function show(Project $project)
	{
        NavigationService::setPosition('projects');

		return view('admin.project.show')
			->with('project', $project);
	}
	
	public function edit(Project $project)
	{
		return view('admin.project.edit')
			->with('project', $project);
	}
	
	public function update(ProjectRequest $request, Project $project)
	{
		$client = $project->client;

		$this->service->save($request, $client, $project);

        //Check if the request is ajax
        if ($request->ajax() || $request->wantsJson()){
            //Ajax request response
            return response()->json('');
        }else{
            $defaultRoute = route('admin.project');
            return NavigationService::returnPosition('projects', $defaultRoute, 'Project saved.');
        }
	}
	
	public function destroy(Project $project)
	{
		$this->service->delete($project);

        $defaultRoute = route('admin.project');

        return NavigationService::returnPosition('projects', $defaultRoute, 'Project deleted.');
	}

	// General ...
	public function access(Project $project)
	{
		$internalUsers = $project->internalUsers();
		$clientUsers = $project->clientUsers();
		$projectUsers = $project->projectUsers();

		return view('admin.project.access')
			->with('project', $project)
			->with('internalUsers', $internalUsers)
			->with('clientUsers', $clientUsers)
			->with('projectUsers', $projectUsers);
	}

	public function cancelAccess(ProjectUser $projectUser)
	{
		$project = $projectUser->project;
		
		$this->service->cancelAccess($projectUser);

		return redirect()
			->route('admin.project.access', $project->id)
			->with('flashNotice', 'User provided access ...');
	}

	public function user(Project $project)
	{
		$user = User::find(\Request::get('userId'));

		$this->service->userAccess($project, $user);

		return redirect()
			->route('admin.project.access', $project->id)
			->with('flashNotice', 'User provided access ...');
	}

    /**
     * The project code to be the short code of the client, the year
     * and then an integer (with 4 digits) of the number of project
     * this is for the client
     *
     * @return string
     */
	public function genProjectCode(){
        if(Request::ajax()){
            //Get client ID
            $clientId = request('clientId');
            //Get project code
            $projectCode = ProjectService::generateProjectCode($clientId);

            return response()->json($projectCode);
        }
    }

    public function add(Client $client)
    {
        return view('admin.project.edit')
            ->with('client', $client);
    }
}
