<?php

namespace DCODESupport\Http\Controllers\Account;

use Illuminate\Http\Request;

use DCODESupport\Http\Requests;
use DCODESupport\Http\Controllers\Controller;
use DCODESupport\Http\Requests\User\UserRequest;

class AccountController extends Controller
{
    public function index()
    {
	    return view('account.edit');
    }

	public function update()
	{
		//dd($request->all());

	    if (\Request::get('password') != '' && \Request::get('first_name') != ''&& \Request::get('last_name') != '')
		{
			$user = \Auth::user();

            $user->first_name = \Request::get('first_name');
            $user->last_name = \Request::get('last_name');
			$user->password = \Hash::make(\Request::get('password'));

            $user->save();
                return redirect()
                    ->route('account')
                    ->with('flashNotice', 'Password updated ...');


		}


	}
}
