@extends('layouts.client')

@section('controlPanelHeader')
	<a class="buttonAction edit" href="{{ route('client.ticket.create') }}"><span>+</span></a>
	<h3>Tasks</h3>
@endsection

@section('controlPanelBody')
	{!! Form::open(['route' => 'client.task', 'method' => 'GET', 'id' => 'searchForm']) !!}
	{!! Form::hidden('export', '0') !!}
	{!! Form::hidden('sort', (\Request::get('sort') !== null ? \Request::get('sort') : 'created_at')) !!}
	{!! Form::hidden('sortDir', (\Request::get('sortDir') == 'asc' ? 'asc' : 'desc')) !!}

	<!-- hr / -->

	<div class="options">
		{!! Form::label('project_id', 'Project') !!}
		{!! Form::projectSelect('project_id', \Request::get('project_id')) !!}
	</div>

	<br />

	<div class="search">
		{!! Form::text('name', \Request::get('name'), ['placeholder' => 'Search']) !!}
	</div>

	<div>
		{!! Form::submit('Search', ['class' => 'buttonAction']) !!}
		<a class="buttonClear" href="{{ route('client.task') }}">Clear</a>
	</div>
	{!! Form::close() !!}
@endsection

@section('content')
	<script>
		$(function() {
			$( ".taskBoard__column" ).sortable({
				connectWith: ".taskBoard__column",
				handle: ".taskBoard__ticket--container",
				cancel: ".portlet-toggle",
				placeholder: "portlet-placeholder ui-corner-all",

				receive: function( event, ui ) {
					var ticketId = ui.item.context.id;
					var statusId = event.target.id;

					$.ajax({
						type: "POST",
						url : "/client/ticket/status",
						data: { ticketId: ticketId, statusId: statusId },
						success : function(data){
							$('#notice .message').html('Saved ...');

							$('#notice').slideDown();

							setTimeout(function(){
								$('#notice').slideUp();
							}, 2000);
						},
						async:true
					});
				}
			});

			$( ".portlet" )
					.addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" )
					.find( ".portlet-header" )
					.addClass( "ui-widget-header ui-corner-all" )
					.prepend( "<span class='ui-icon ui-icon-minusthick portlet-toggle'></span>");

			$( ".portlet-toggle" ).click(function() {
				var icon = $( this );
				icon.toggleClass( "ui-icon-minusthick ui-icon-plusthick" );
				icon.closest( ".portlet" ).find( ".portlet-content" ).toggle();
			});
		});
	</script>

	<div class="row" style="width:100%;max-width:100%;">
		<div class="large-12 columns">
			<div class="columns">
				<div class="taskBoard__header">
					<div class="taskBoard__header--column">Pending {{ $pendingTickets->count() }}</div>
					<div class="taskBoard__header--column">Outstanding {{ $outstandingTickets->count() }}</div>
					<div class="taskBoard__header--column">Completed {{ $completedTickets->count() }}</div>
					<div class="taskBoard__header--column">Approved {{ $approvedTickets->count() }}</div>
				</div>

				<div class="taskBoard">
					<div class="taskBoard__column pending" id="0">
						@foreach ($pendingTickets as $ticket)
							@include('_partials.task.card', ['ticket' => $ticket])
						@endforeach

					</div>

					<div class="taskBoard__column outstanding" id="{{ \DCODESupport\Models\Status::where('name', 'Outstanding')->first()->id }}">
						@foreach ($outstandingTickets as $ticket)
							@include('_partials.task.card', ['ticket' => $ticket])
						@endforeach

					</div>

					<div class="taskBoard__column completed" id="{{ \DCODESupport\Models\Status::where('name', 'Completed')->first()->id }}">
						@foreach ($completedTickets as $ticket)
							@include('_partials.task.card', ['ticket' => $ticket])
						@endforeach

					</div>

					<div class="taskBoard__column approved" id="{{ \DCODESupport\Models\Status::where('name', 'Approved')->first()->id }}">
						@foreach ($approvedTickets as $ticket)
							@include('_partials.task.card', ['ticket' => $ticket])
						@endforeach

					</div>
				</div>
			</div>
		</div>
	</div>
@endsection

@section('javascript')
	<script>
		var $modal = $('#ticketModal');

		$(document).on('click', '.taskBoard__ticket', function(e){
			e.preventDefault();

			$.ajax({
				type: "GET",
				url : '{{ route('client.ticket.preview') }}?ticketId='+$(this).attr('id'),
				success : function(data){
					$modal.html(data).foundation('open');
				},
				async:true
			});
		});
	</script>
@stop