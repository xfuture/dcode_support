@extends('layouts.client')

@section('controlPanelHeader')
	<a class="buttonAction edit" data-href="{{ route('client.project.create') }}"><span>+</span></a>
	<h3>Projects</h3>
@endsection

@section('controlPanelBody')
	{!! Form::open(['route' => 'client.project', 'method' => 'GET', 'id' => 'searchForm']) !!}
	{!! Form::hidden('export', '0') !!}
	{!! Form::hidden('sort', (\Request::get('sort') !== null ? \Request::get('sort') : 'created_at')) !!}
	{!! Form::hidden('sortDir', (\Request::get('sortDir') == 'asc' ? 'asc' : 'desc')) !!}

			<!-- hr / -->

	<div class="options">
		{!! Form::label('sort', 'Sort By') !!}
		{!! Form::select('sort', ['code' => 'Code', 'name' => 'Name', 'client' => 'Client', 'created_at' => 'Created At'], (\Request::get('sort') !== null ? \Request::get('sort') : 'created_at')) !!}
	</div>

	<br />

	<div class="search">
		{!! Form::text('code', \Request::get('code'), ['placeholder' => 'Project Code']) !!}
	</div>
	<div class="search">
		{!! Form::text('name', \Request::get('name'), ['placeholder' => 'Name']) !!}
	</div>
	<div>
		{!! Form::checkbox('active', '0', \Request::get('active')) !!}
		{!! Form::label('active', 'Show Archived Projects') !!}
	</div>

	<div>
		{!! Form::submit('Search', ['class' => 'buttonAction']) !!}
		<a class="buttonClear" href="{{ route('client.project') }}">Clear</a>
	</div>
	{!! Form::close() !!}
@endsection

@section('content')
	<div class="cards">
		@if ($projects->count() > 0)

@foreach ($projects as $project)
				<div class="cardContainer">
					<div class="card">
						<!-- a href="{{ route('client.project.show', $project->id) }}" -->
							<header>
								<h3>{{ $project->name }} {!! ($project->active == '1' ? '<span class="badge success"></span>' : '') !!}</h3>
								<span>{{ $project->code }}</span>
							</header>
							<div class="cardBody">
								<div class="cardBody__item">
									<label>Client</label>
									{{ DisplayService::displayClient($project->client) }}
								</div>

								<div class="cardBody__item">
									<label>Name</label>
									{{ $project->name }}
								</div>

								<div class="cardBody__item">
									<label>Created</label>
									{{ date("d/m/Y", strtotime($project->created_at)) }}
								</div>

								<div class="cardBody__item">
									<label>Added By</label>
									{{ DisplayService::displayUser($project->user) }}
								</div>

								<div class="cardBody__item">
									<label>Next Milestone</label>
									@if ($project->nextMilestone() != null)
										{{ $project->nextMilestone()->name }}
										<span class="label small {{ ($project->nextMilestone()->scheduled < time() ? 'alert' : 'success') }}">{{ DisplayService::displayDate($project->nextMilestone()->scheduled) }}</span>
									@else
										NA
									@endif
								</div>

								<div class="cardBody__item">
									<label>Status</label>
									@if ($project->latestUpdate() != null)
										<span class="{{ $project->latestUpdate()->projectStatus->class }}">{{ DisplayService::displayProjectStatus($project->latestUpdate()->projectStatus) }}</span><br />
										@if ($project->latestUpdate()->comments()->count() > 0)
											<small>{{ DisplayService::displayExcerpt($project->latestUpdate()->comments()->orderBy('created_at', 'asc')->first()->comment) }}</small>
										@endif
									@else
										NA
									@endif
								</div>
							</div>
						</a>
						<footer>
							<a class="button round" href="{{ route('client.ticket', ['project_id' => $project->id]) }}" aria-describedby="ticketCount">Tickets <span class="badge alert" id="ticketCount">{{ $project->activeTickets()->count() }}</span></a>
						</footer>
					</div>
				</div>
			@endforeach

			<div class="cards__pagination">
				{!! $projects->appends(\Request::except('page'))->render() !!}
			</div>
		@else
			<div class="cards__empty">
				<h3>No projects</h3>
				<p>No projects ...</p>
			</div>
		@endif
	</div>
@endsection