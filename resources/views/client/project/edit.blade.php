@extends('layouts.modal')

@section('header')
	<h1>Add Project</h1>
@stop

@section('content')
	<div class="warnMessage"></div>
	<div class="main form formModal">

			{!! Form::open(['route' => 'client.project.store', 'id'=>'storeProjectForm']) !!}

		<fieldset>
			<div>
				{!! Form::hidden('client_id', \Auth::user()->client_id) !!}
				{!! DisplayService::displayClient(\Auth::user()->client()->first()) !!}
			</div>
			<?php /**
			<div>
				{!! Form::hidden('code','Dummy code') !!}

			</div>
	 		**/ ?>
			<div>
				{!! Form::label('name', 'Project name:') !!}
				{!! Form::text('name') !!}
			</div>
			<div>
				{{-- Assuming active --}}
				{!! Form::hidden('active', '1') !!}
			</div>

		</fieldset>

		<div class="formAction">
			{!! Form::submit('Save', ['class' => 'buttonAction']) !!}
			<a class="buttonClear" href="{{ URL::previous() }}">Cancel</a>
		</div>

		{!! Form::close() !!}
	</div>
@stop

@section('javascript')
	<script type="application/javascript">
			bindErrorAjaxModal('#editModal #storeProjectForm', '{{ route('client.project.store') }}', 'POST');
	</script>
@stop


