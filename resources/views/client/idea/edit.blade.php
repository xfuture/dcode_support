@extends('layouts.modal')

@section('header')
	<h1>Add Idea</h1>
@stop

@section('content')
	<div class="warnMessage"></div>
	<div class="main form formModal">
	@if (isset($idea))
		{!! Form::model($idea, ['route' => ['client.idea.update', $idea->id], 'method' => 'put', 'id'=>'updateIdeaForm']) !!}
	@else
		{!! Form::open(['route' => 'client.idea.store','id'=>'storeIdeaForm']) !!}
	@endif

	<fieldset>
		<legend>Add/Edit Idea Client</legend>

		<div>
			{!! Form::label('client_id', 'Client:') !!}
			{!! Form::hidden('client_id', \Auth::user()->client_id) !!}
			{!! DisplayService::displayClient(\Auth::user()->client()->first()) !!}
		</div>

		<div>
			{!! Form::label('project_id', 'Project:') !!}
			{!! Form::projectSelect('project_id', (isset($idea) ? $idea->project_id : null)) !!}
		</div>
	</fieldset>

		<fieldset>
			<legend>Add/Edit Idea Information</legend>

			<div>
				{!! Form::label('name', 'Name:') !!}
				{!! Form::text('name') !!}
			</div>

			<div>
				{!! Form::label('comment', 'Comment:') !!}
				{!! Form::textarea('comment') !!}
			</div>
		</fieldset>
		<div class="formAction">
		{!! Form::submit('Save', ['class' => 'buttonAction']) !!}
		<a class="buttonClear" href="{{ URL::previous() }}">Cancel</a>
	</div>

	{!! Form::close() !!}

	</div>
@stop

@section('javascript')
	<script type="application/javascript">
        @if (isset($idea))
            bindErrorAjaxModal('#editModal #updateIdeaForm', '{{ route('client.idea.update', ['id'=>$idea->id]) }}', 'POST');
        @else
            bindErrorAjaxModal('#editModal #storeIdeaForm', '{{ route('client.idea.store') }}', 'POST');
		@endif
	</script>
@stop

