@extends('layouts.modal')

@section('header')
	@include('_partials.idea.header', ['idea' => $idea])
@stop

@section('content')
	<article>
		<div>
			<fieldset>
				<legend>Details</legend>

				<div>
					<label>Created At</label>
					{{ $idea->created_at }}
				</div>
			</fieldset>

			<fieldset>
				<legend>Description</legend>
				{!! nl2br($idea->comment) !!}
			</fieldset>
		</div>

		<div>
			<h3>Comments:</h3>

			@foreach ($idea->comments as $comment)
				@include('_partials.comment.show', ['comment' => $comment])
			@endforeach

			{!! Form::open(['route' => ['client.idea.comment', $idea->id]]) !!}
			@include('_partials.comment.edit')
			{!! Form::close() !!}
		</div>
	</article>
@stop