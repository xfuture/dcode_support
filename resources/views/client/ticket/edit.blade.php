@extends('layouts.modal')

@section('header')
	<h1>Add Ticket</h1>
@stop

@section('content')
	<div class="main form formModal">
		@if (isset($ticket))
			{!! Form::model($ticket, ['route' => ['client.ticket.update', $ticket->id], 'method' => 'put','id'=>'updateTicketForm']) !!}
		@else
			{!! Form::open(['route' => 'client.ticket.store','id'=>'storeTicketForm']) !!}
		@endif

		<fieldset>
			<legend>Add/Edit Ticket Client</legend>

			<div>
				{!! Form::label('client_id', 'Client:') !!}
				{!! Form::hidden('client_id', \Auth::user()->client_id) !!}
				{!! DisplayService::displayClient(\Auth::user()->client()->first()) !!}
			</div>

			<div>
				{!! Form::label('project_id', 'Project:') !!}
				{!! Form::projectSelect('project_id', (isset($ticket) ? $ticket->project_id : null)) !!}
			</div>
		</fieldset>

		<fieldset>
			<legend>Add/Edit Ticket Information</legend>

			<div>
				{!! Form::label('name', 'Name:') !!}
				{!! Form::text('name') !!}
			</div>

			<div>
				{!! Form::label('comment', 'Comment:') !!}
				{!! Form::textarea('comment') !!}
			</div>

			<div>
				{!! Form::label('date_due', 'Date Due:') !!}
				{!! Form::datePicker('date_due', (isset($ticket) ? $ticket->date_due : null)) !!}
			</div>
		</fieldset>

		{!! Form::hidden('assigned_user_id') !!}
		{!! Form::hidden('internal', '0') !!}

		<div class="formAction">
			{!! Form::submit('Save', ['class' => 'buttonAction']) !!}
			<a class="buttonClear" href="{{ URL::previous() }}">Cancel</a>
		</div>

		{!! Form::close() !!}
	</div>
@stop
@section('javascript')
	<script type="application/javascript">
        @if (isset($idea))
            bindErrorAjaxModal('#editModal #updateTicketForm', '{{ route('client.ticket.update', ['id'=>$ticket->id]) }}', 'POST');
        @else
            bindErrorAjaxModal('#editModal #storeTicketForm', '{{ route('client.ticket.store') }}', 'POST');
		@endif
	</script>
@stop