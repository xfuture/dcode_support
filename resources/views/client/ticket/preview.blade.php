@extends('layouts.modal')

@section('header')

	<div>
		<h1>{{ $ticket->name }}</h1>
	</div>

@stop

@section('content')

	<article>
		<div>
			<fieldset>
				<legend>Details</legend>

				<div>
					<label>Current Status</label>

					@if (isset($ticket))
						{!! Form::model($ticket,['route' => ['client.ticket.status'],'method' => 'post']) !!}


							{!!Form::hidden('id', $ticket->id) !!}

					{{--Define the drop down with a arbritary name "statusSelect" to get the selected option in TicketController--}}

							{!!  Form::statusSelect('statusSelect', ($ticket->currentStatus() != null ? $ticket->currentStatus()->status_id : null))!!}


							<div class="formAction">
								{!! Form::submit('Save', ['class' => 'buttonAction']) !!}
							</div>
						{!! Form::close() !!}
					@endif
				</div>

				<div>
					<label>Date Due</label>

					{{ \DCODESupport\Services\DisplayService::displayDate($ticket->date_due) }}
				</div>

				<div>
					<label>Created At</label>
					{{ $ticket->created_at }}

				</div>
			</fieldset>

			<fieldset>
				<legend>Comment</legend>
				{!! nl2br($ticket->comment) !!}
			</fieldset>
		</div>

		<div>
			<a name="comment"> &nbsp; </a>
			<fieldset>
				<legend>Comments:</legend>

				@if ($ticket->comments()->where('internal', '0')->count() > 0)
					@foreach ($ticket->comments()->where('internal', '0')->get() as $comment)
						@include('_partials.comment.show', ['comment' => $comment])
					@endforeach
				@else
					<div class="missing">
						<h3>No Comments</h3>
						<p>No comments have been added to this ticket.</p>
					</div>
				@endif

				{!! Form::open(['route' => ['client.ticket.comment', $ticket->id]]) !!}
				@include('_partials.comment.edit')
				{!! Form::close() !!}
			</fieldset>
		</div>
	</article>

	<aside>
		<fieldset>
			<legend>People</legend>

			<div>
				<label>Assigned To</label>
				@if ($ticket->assignedUser != null)
					Assigned
				@else
					Un-Assigned
				@endif
			</div>

			<div>
				<label>Reported By</label>
				{{ DisplayService::displayUser($ticket->user) }}
			</div>
		</fieldset>

		<fieldset>
			<legend>Dates</legend>

			<div>
				<label>Created At</label>
				{{ $ticket->created_at }}
			</div>

			<div>
				<label>Updated At</label>
				{{ $ticket->updated_at }}
			</div>
		</fieldset>
	</aside>

@endsection
{{--
@section('javascript')
	<script type="text/javascript">
		listID="div_source1";
		$(listID).change(function(e){
            $.ajax({
                type:"GET",
                url:"Html::statusSelect($ticket)",
                data:$(listID).serialize(),
                dataType:"json",
                success:function(data){
                    $each(data.projectStatusSelect,function(obj){
                        var div_data="<option value="+obj.value+">"+obj.text+"</option>";
                        $(div_data).appendTo('#ch_user1');
                    });
				}
            });
		})

	</script>

@endsection
--}}