@extends('layouts.client')

@section('controlPanelHeader')
	<h3>{{$ticket->project->name}}</h3>
@endsection

@section('controlPanelBody')
	<div class="controlPanel__content white">
		<div>
			<label>Client</label>
			{{ \DCODESupport\Services\DisplayService::displayClient($ticket->project->client) }}
		</div>

		<hr/>

		<div>
			<label>Start</label>
			{{ \DCODESupport\Services\DisplayService::displayDate(strtotime($ticket->project->created_at)) }}
		</div>

		<hr/>

		<div>
			<label>User</label>
			{{ \DCODESupport\Services\DisplayService::displayUser($ticket->project->user) }}
		</div>

		<hr/>

		<div>
			<label>Status</label>
			@if ($ticket->project->latestUpdate() != null)
				<span class="{{ $ticket->project->latestUpdate()->projectStatus->class }}">{{ DisplayService::displayProjectStatus($ticket->project->latestUpdate()->projectStatus) }}</span>
				<br/>
				@if ($ticket->project->latestUpdate()->comments()->count() > 0)
					<small>{{ DisplayService::displayExcerpt($ticket->project->latestUpdate()->comments()->orderBy('created_at', 'asc')->first()->comment) }}</small>
				@endif
			@else
				NA
			@endif
		</div>
	</div>
@endsection

@section('content')
	<div style="width: 100%">
		<div style="width: 100%; background: white; padding: 30px">
			<div>
				<h3>{{$ticket->name}}</h3>
				<div>
					{!! Form::model($ticket,['route' => ['client.ticket.status'],'method' => 'post']) !!}
					{!!Form::hidden('id', $ticket->id) !!}
					{{--Define the drop down with a arbritary name "statusSelect" to get the selected option in TicketController--}}
					<label>Change Status</label>
					{!!  Form::statusSelect('statusSelect', ($ticket->currentStatus() != null ? $ticket->currentStatus()->status_id : null))!!}
					<div class="formAction">
						{!! Form::submit('Save Status', ['class' => 'buttonAction']) !!}
					</div>
					{!! Form::close() !!}
				</div>
			</div>

			<!-- Details -->
			<fieldset>
				<legend>Details</legend>
				<div class="wrapper">
					<div class="rectangle">
						<div>
							{!! nl2br($ticket->comment) !!}
						</div>
					</div>
					<hr style="margin: 0; border: transparent"/>
					<div class="rectangle">
						<fieldset>
							<legend>People</legend>
							<div>
								<h5>Assigned To</h5>
								@if ($ticket->assignedUser != null)
									Assigned
								@else
									Un-Assigned
								@endif
							</div>
							<div>
								<h5>Reported By</h5>
								{{ DisplayService::displayUser($ticket->user) }}
							</div>

						</fieldset>
						<fieldset>
							<legend>Dates</legend>
							<div>
								<h5>Created At</h5>
								{{ $ticket->created_at }}
							</div>
							<br/>

							<div>
								<h5>Updated At</h5>
								{{ $ticket->updated_at }}
							</div>
						</fieldset>

					</div>
				</div>
			</fieldset>
		</div>
		<hr style="margin: 0; height: 15px; background-color: #f0f0f0; border: transparent"/>
		<div style="width: 100%; background: white; padding: 30px">
			<fieldset>
				<a name="comment"> &nbsp; </a>
				<legend>Comments
						<a class="buttonAction" href="#comment"><span class="fa fa-comment-o"></span></a>
				</legend>
				<div style="padding: 15px">
					@foreach ($ticket->comments as $comment)
						@include('_partials.comment.show', ['comment' => $comment])
					@endforeach

					{!! Form::open(['route' => ['client.ticket.comment', $ticket->id]]) !!}
					@include('_partials.comment.edit')
					{!! Form::close() !!}
				</div>
			</fieldset>
		</div>
	</div>
@stop