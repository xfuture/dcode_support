@extends('layouts.client')

@section('header')
	<h1>Tickets</h1>
@endsection

@section('content')
	<div class="admin__refine">
		{!! Form::open(['route' => 'client.ticket', 'method' => 'GET', 'id' => 'searchForm']) !!}
		{!! Form::hidden('export', '0') !!}
		{!! Form::hidden('sort', (\Request::get('sort') !== null ? \Request::get('sort') : 'created_at')) !!}
		{!! Form::hidden('sortDir', (\Request::get('sortDir') == 'asc' ? 'asc' : 'desc')) !!}

		<div class="search">
			<div>
				{!! Form::text('name', \Request::get('name'), ['placeholder' => 'Search ...']) !!}
			</div>
			<div>
				{!! Form::projectSelect('project_id', \Request::get('project_id')) !!}
			</div>
			<div>
				{!! Form::statusSelect('status_id', \Request::get('status_id')) !!}
			</div>
			<div>
				{!! Form::submit('Search', ['class' => 'buttonAction']) !!}
				<!-- a class="exportButton">Export</a -->
				<a class="buttonClear" href="{{ route('client.ticket') }}">Clear</a>
			</div>
		</div>
		{!! Form::close() !!}
	</div>

	<div class="admin__table">
		@if ($tickets->count() > 0)
			<table>
				<thead>
				<?php
				$columns = array(
						array(
								'id'    => 'client',
								'value' => 'Client',
								'sort'  => true,
								'width' => '100'
						),
						array(
								'id'    => 'project',
								'value' => 'Project',
								'sort'  => true,
								'width' => '100'
						),
						array(
								'id'    => 'name',
								'value' => 'Name',
								'sort'  => true,
								'width' => '150'
						),
						array(
								'id'    => 'date_due',
								'value' => 'Date Due',
								'sort'  => true,
								'width' => '75'
						),
						array(
								'id'    => 'assigned_user',
								'value' => 'Assigned User',
								'sort'  => true,
								'width' => '100'
						),
						array(
								'id'    => 'user',
								'value' => 'User',
								'sort'  => true,
								'width' => '100'
						),
						array(
								'id'    => 'created_at',
								'value' => 'Created',
								'sort'  => true,
								'width' => '100'
						)
				);
				?>

				@include('_partials.layout.tableHeader', ['columns' => $columns, 'action' => 4])
				</thead>

				<tbody>
				@foreach ($tickets as $ticket)
					<tr>
						<td>{{ DisplayService::displayClient($ticket->client) }}</td>
						<td>{{ DisplayService::displayProjectCode($ticket->project) }} {{ DisplayService::displayProject($ticket->project) }}</td>
						<td>
							<a href="{{ route('client.ticket.show', $ticket->id) }}">{{ $ticket->name }}</a>
							<span class="summary">{{ DisplayService::displayExcerpt($ticket->comment) }}</span>
						</td>
						<td>
							@if ($ticket->date_due != null && $ticket->date_due > 0)
								<span class="label {{ ($ticket->date_due < time() ? 'alert' : 'success') }}">{{ DisplayService::displayDate($ticket->date_due) }}</span>
							@endif
						</td>
						<td>
							@if ($ticket->assignedUser != null)
								Assigned
							@else
								Un-Assigned
							@endif
						</td>
						<td>{{ DisplayService::displayuser($ticket->user) }}</td>
						<td>{{ date("d/m/Y", strtotime($ticket->created_at)) }}</td>
						<td><span class="fa fa-comment-o"></span> {{ $ticket->comments()->count() }}</td>
						<td>{!! DisplayService::displayTicketStatus($ticket->currentStatus()) !!}</td>

						@if (\Auth::id() == $ticket->user->id)
							<td class="alignCenter"><a href="{{ route('client.ticket.edit', $ticket->id) }}">Edit</a></td>
							<td class="alignCenter">{!! \Html::delete('client.ticket.destroy', $ticket->id, 'Delete') !!}</td>
						@else
							<td colspan="2"> &nbsp; </td>
						@endif
					</tr>
				@endforeach
				</tbody>
			</table>

			<div class="admin__table--pagination">
				{!! $tickets->appends(\Request::except('page'))->render() !!}
			</div>
		@else
			<div class="admin__table--empty">
				<h3>No tickets</h3>
				<p>No tickets ...</p>
			</div>
		@endif
	</div>
@endsection