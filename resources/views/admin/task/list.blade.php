@extends('layouts.admin')

@section('controlPanelHeader')
	<a class="buttonAction edit" href="{{ route('admin.ticket.create') }}"><span>+</span></a>
	<h3>Tasks</h3>
@endsection

@section('controlPanelBody')

{!! Form::open(['route' => 'admin.task', 'method' => 'GET', 'id' => 'searchForm']) !!}
	{!! Form::hidden('export', '0') !!}
	{!! Form::hidden('sort', (\Request::get('sort') !== null ? \Request::get('sort') : 'created_at')) !!}
	{!! Form::hidden('sortDir', (\Request::get('sortDir') == 'asc' ? 'asc' : 'desc')) !!}

	<!-- hr / -->

	<div class="options">
		{!! Form::label('client_id', 'Client') !!}
		{!! Form::clientSelect('client_id', \Request::get('client_id')) !!}
	</div>
	<hr />

	<div class="options">
		{!! Form::label('project_id', 'Project') !!}
		{!! Form::projectSelect('project_id', \Request::get('project_id')) !!}
	</div>
	<hr />

	<div class="options">
		{!! Form::label('assigned_user_id', 'Assigned To') !!}
		{!! Form::assignedUserSelect('assigned_user_id', \Request::get('assigned_user_id')) !!}
	</div>
	<hr />

	<div class="search">
		{!! Form::text('name', \Request::get('name'), ['placeholder' => 'Task Name']) !!}
	</div>
	<br/>

	<div>
		{!! Form::submit('Search', ['class' => 'buttonAction']) !!}
		<a class="buttonClear" href="{{ route('client.project') }}">Clear</a>
	</div>
{!! Form::close() !!}
@endsection

@section('content')
	<script>
		$(function() {
			$( ".taskBoard__column" ).sortable({
				connectWith: ".taskBoard__column",
				handle: ".taskBoard__ticket--container",
				cancel: ".portlet-toggle",
				placeholder: "portlet-placeholder ui-corner-all",

				receive: function( event, ui ) {
					var ticketId = ui.item.context.id;
					var statusId = event.target.id;

					$.ajax({
						type: "POST",
						url : "/admin/ticket/status",
						data: { ticketId: ticketId, statusId: statusId },
						success : function(data){
						    var pending = $('.taskBoard__column.pending.ui-sortable .taskBoard__ticket').length;
						    var outstanding = $('.taskBoard__column.outstanding.ui-sortable .taskBoard__ticket').length;
						    var completed = $('.taskBoard__column.completed.ui-sortable .taskBoard__ticket').length;
						    var approved = $('.taskBoard__column.approved.ui-sortable .taskBoard__ticket').length;

						    //Change count pending
							$('.taskBoard__header--column.pending').html('Pending' + ' ' + pending);
							//Change count outstanding
                            $('.taskBoard__header--column.outstanding').html('Outstanding' + ' ' + outstanding);
							//Change count completed
                            $('.taskBoard__header--column.completed').html('Completed' + ' ' + completed);
							//Change count approved
                            $('.taskBoard__header--column.approved').html('Approved' + ' ' + approved);

							$('#notice .message').html('Saved ...');
							$('#notice').slideDown();
							setTimeout(function(){
								$('#notice').slideUp();
							}, 2000);
						},
						async:true
					});
				}
			});

			$( ".portlet" )
					.addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" )
					.find( ".portlet-header" )
					.addClass( "ui-widget-header ui-corner-all" )
					.prepend( "<span class='ui-icon ui-icon-minusthick portlet-toggle'></span>");

			$( ".portlet-toggle" ).click(function() {
				var icon = $( this );
				icon.toggleClass( "ui-icon-minusthick ui-icon-plusthick" );
				icon.closest( ".portlet" ).find( ".portlet-content" ).toggle();
			});
		});
	</script>

	<div class="row" style="width:100%;max-width:100%;">
		<div class="large-12 columns">
			<div class="columns">
				<div class="taskBoard__header">
					<div class="taskBoard__header--column pending">Pending {{ $pendingTickets->count() }}</div>
					<div class="taskBoard__header--column outstanding">Outstanding {{ $outstandingTickets->count() }}</div>
					<div class="taskBoard__header--column completed">Completed {{ $completedTickets->count() }}</div>
					<div class="taskBoard__header--column approved">Approved {{ $approvedTickets->count() }}</div>
				</div>

				<div class="taskBoard">
					<div class="taskBoard__column pending" id="0">
						@foreach ($pendingTickets as $ticket)
							@include('_partials.task.card', ['ticket' => $ticket])
						@endforeach

					</div>

					<div class="taskBoard__column outstanding" id="{{ \DCODESupport\Models\Status::where('name', 'Outstanding')->first()->id }}">
						@foreach ($outstandingTickets as $ticket)
							@include('_partials.task.card', ['ticket' => $ticket])
						@endforeach

					</div>

					<div class="taskBoard__column completed" id="{{ \DCODESupport\Models\Status::where('name', 'Completed')->first()->id }}">
						@foreach ($completedTickets as $ticket)
							@include('_partials.task.card', ['ticket' => $ticket])
						@endforeach

					</div>

					<div class="taskBoard__column approved" id="{{ \DCODESupport\Models\Status::where('name', 'Approved')->first()->id }}">
						@foreach ($approvedTickets as $ticket)
							@include('_partials.task.card', ['ticket' => $ticket])
						@endforeach

					</div>
				</div>
			</div>
		</div>
	</div>


@endsection

@section('javascript')
	<script>
		var $modal = $('#ticketModal');

		$(document).on('click', '.taskBoard__ticket', function(e){
			e.preventDefault();

			$.ajax({
				type: "GET",
				url : '{{ route('admin.ticket.preview') }}?ticketId='+$(this).attr('id'),
				success : function(data){
					$modal.html(data).foundation('open');
				},
				async:true
			});
		});
	</script>

	<script type="text/javascript" >

        function toSeconds(s) {
            var p = s.split(':');
            return parseInt(p[0], 10) * 3600 + parseInt(p[1], 10) * 60 + parseInt(p[2], 10);
        }

        function fill(s, digits) {
            s = s.toString();
            while (s.length < digits) s = '0' + s;
            return s;
        }

        Date.prototype.days=function(to){

            return  Math.round(Math.abs((this.getTime() - to.getTime())/(24*60*60*1000)))

        }
        function secondsToHMS(secs) {
            function z(n){return (n<10?'0':'') + n;}
            var sign = secs < 0? '-':'';
            secs = Math.abs(secs);
            return sign + z(secs/3600 |0) + ':' + z((secs%3600) / 60 |0) + ':' + z(secs%60);
        }

        function hmsToSeconds(s) {
            var b = s.split(':');
            return b[0]*3600 + b[1]*60 + (+b[2] || 0);
        }


        function update(){
            $(".elapsed").each(function() {

                var convertTime = new Date($(this).attr('data-val'));
				var currentdate = new Date();

				//console.log("start Time",$(this).attr('data-val'));
                //console.log("now time",currentdate);

                    var daysdiff = new Date(convertTime).days(new Date(currentdate));
                    if(daysdiff==0){
                        var nowTime=currentdate.getHours()+":"+currentdate.getMinutes()+":"+currentdate.getSeconds();
                        var startTime=convertTime.getHours()+":"+convertTime.getMinutes()+":"+convertTime.getSeconds();
                        var countDown= secondsToHMS(hmsToSeconds(nowTime) - hmsToSeconds(startTime));
                    }
                    else{
                        var nowTime=24*daysdiff+currentdate.getHours()+":"+currentdate.getMinutes()+":"+currentdate.getSeconds();
                        var startTime=convertTime.getHours()+":"+convertTime.getMinutes()+":"+convertTime.getSeconds();
                        var countDown= secondsToHMS(hmsToSeconds(nowTime) - hmsToSeconds(startTime));
                    }

                    $(this).html(countDown);

				$(".elapsed").css({
                    "display": "inline-block",
                    }).delay(100).animate({
					"background-color": "##6ab63c",
                    "display": "inline-block",
                    "border-radius": "5px"}, 1500, function () {
                    $(".elapsed").animate({
                        "background-color": "#fff",
                    },0.1);
                })
            });
            window.setTimeout("update()", 1000);

		}
		update();


	</script>
@stop