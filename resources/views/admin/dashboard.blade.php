@extends('layouts.admin')

@section('content')
	<h1>Dashboard</h1>

	<div class="activityLog">
		@foreach ($incompleteMilestones as $projectMilestone)
			<div class="activity">
				<div class="activity__icon badge">
					<span class="dicon-messages"></span>
				</div>

				<div class="activity__comment">
					<div class="activity__comment--date">
						{{ \DCODESupport\Services\DisplayService::displayDate($projectMilestone->scheduled) }}<br />
						<small>{{ ($projectMilestone->scheduled > time() ? 'Ontime' : 'Overdue') }}</small>
					</div>

					<h4><a href="{{ route('admin.project.show', $projectMilestone->project_id) }}">{!! $projectMilestone->name !!}</a></h4>
					<div> ... </div>

					<div class="activity__comment--meta">
						<label>Project:</label>
						{{ \DCODESupport\Services\DisplayService::displayProject($projectMilestone->project) }}

						&nbsp;

						<label>Created By:</label>
						{{ \DCODESupport\Services\DisplayService::displayUser($projectMilestone->user) }}
					</div>
				</div>
			</div>
		@endforeach
	</div>
@endsection
{{--
@section('javascript')

	<link rel="stylesheet" href="/css/gantt.css" />
	<script src="js/jquery.fn.gantt.js"></script>
	<script type="application/javascript">
/*
        jQuery(function () {

            var data = [
                { "name": " TestName ","desc": "&rarr; TestDesc"  ,"values": [{"id": "testID", "from": "/Date(1320182000000)/", "to": "/Date(1320401600000)/", "desc": "Id: T1<br/>", "label": " test Label", "customClass": "testClass", "dep": "b4"}]},
                { "name": " TestName ","desc": "&rarr; TestDesc"  ,"values": [{"id": "testID", "from": "/Date(1320182000000)/", "to": "/Date(1320301600000)/", "desc": "Id: T1<br/>Name:   test A", "label": " test Label", "customClass": "testClass", "dep": "b4"}]},
                { "name": " Step B ","desc": "&rarr; step C"  ,"values": [{"id": "b1", "from": "/Date(1320601600000)/", "to": "/Date(1320870400000)/", "desc": "Id: 1<br/>Name:   Step B", "label": " Step B", "customClass": "ganttOrange", "dep": "b2"}]},
                { "name": " Step C ","desc": "&rarr; step D"  ,"values": [{"id": "b2", "from": "/Date(1321192000000)/", "to": "/Date(1321500400000)/", "desc": "Id: 2<br/>Name:   Step C", "label": " Step C", "customClass": "ganttGreen", "dep": "b3"}]},
                { "name": " Step D ","desc": "&rarr; step E"  ,"values": [{"id": "b3", "from": "/Date(1320302400000)/", "to": "/Date(1320551600000)/", "desc": "Id: 3<br/>Name:   Step D", "label": " Step D", "dep": "b4"}]},
                { "name": " Step E ","desc": "&mdash;"        ,"values": [{"id": "b4", "from": "/Date(1320802400000)/", "to": "/Date(1321994800000)/", "desc": "Id: 4<br/>Name:   Step E", "label": " Step E", "customClass": "ganttRed"}]},
                { "name": " Step F ","desc": "&rarr; step B"  ,"values": [{"id": "b5", "from": "/Date(1320192000000)/", "to": "/Date(1320401600000)/", "desc": "Id: 5<br/>Name:   Step F", "label": " Step F", "customClass": "ganttOrange", "dep": "b1"}]},
                { "name": " Step G ","desc": "&rarr; step C"  ,"values": [{"id": "b6", "from": "/Date(1320401600000)/", "to": "/Date(1320570400000)/", "desc": "Id: 6<br/>Name:   Step G", "label": " Step G", "customClass": "ganttGreen", "dep": "b8"}]},
                { "name": " Step H ","desc": "&rarr; Step J"  ,"values": [{"id": "b7", "from": "/Date(1321192000000)/", "to": "/Date(1321500400000)/", "desc": "Id: 7<br/>Name:   Step H", "label": " Step H", "dep": "b9"}]},
                { "name": " Step I ","desc": "&rarr; Step H"  ,"values": [{"id": "b8", "from": "/Date(1320302400000)/", "to": "/Date(1320551600000)/", "desc": "Id: 8<br/>Name:   Step I", "label": " Step I", "customClass": "ganttRed", "dep": "b7"}]},
                { "name": " Step J ","desc": "&mdash;"        ,"values": [{"id": "b9", "from": "/Date(1320802400000)/", "to": "/Date(1321994800000)/", "desc": "Id: 9<br/>Name:   Step J", "label": " Step J", "customClass": "ganttOrange"}]}
            ];

            $(".gantt").gantt({source: data, navigate: 'scroll', scale: 'days', maxScale: 'weeks', minScale: 'hours'});

        });
*/
//var render=$(".ganttData").attr('data-json');
//var renderObject=[{ 'name': ' TestName ','desc': '&rarr; TestDesc'  ,'values': [{'id': 'testID', 'from': '/Date(1320182000000)/', 'to': '/Date(1320401600000)/', 'desc': 'Id: T1<br/>', 'label': ' test Label', 'customClass': 'testClass', 'dep': 'b4'}]}];
//var milestone=$(".ganttData").attr('data-milestone');

//console.log("render: ",render);
//console.log("renderObject: ",renderObject);

// WORKING GANTT CHART
/*
        jQuery(function () {

        $(".ganttData").gantt({


                  source: $(this).attr("data-json"),
				  navigate:"scroll",
				  scale: "days",
                  minScale: "hours",
                  maxScale: "weeks",
                  onItemClick: function(data) {
                      alert("Item clicked - show some details");
                  },
                  onAddClick: function(dt, rowId) {
                      alert("Empty space clicked - add an item!");
                  },
                  onRender: function() {
                      console.log("chart rendered");
                      console.log('this.source:',this.source);
                      console.log('$(this).attr("data-json")',$(this).attr('data-json'));

                  }
              });
        });

*/

            var testing=JSON.parse($(".ganttData").attr('data-json'));
			// console.log('read from attribute:',JSON.parse(testing));

			//var myObject=testing.replaceAll("'",'"');
			//var myObject= JSON.parse(testing);
			//console.log('myObject:',myObject);

			 // var rowTesting=[{ "name": " TestName ","desc": "&rarr; TestDesc"  ,"values": [{"id": "testID", "from": "/Date(1320182000000)/", "to": "/Date(1320401600000)/", "desc": "Id: T1<br/>", "label": " test Label", "customClass": "testClass", "dep": "b4"}]}];
			// console.log('rowTesting:',rowTesting);


            $("#ganttData").gantt({
                source:testing,
				navigate: 'scroll',
				scale: 'days',
				maxScale: 'weeks',
				minScale: 'hours',
                onRender: function() {
                    console.log("chart rendered");
                    console.log(this.source);

                }
            });
	</script>
@stop
--}}