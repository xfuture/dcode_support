@extends('layouts.modal')

@section('header')
	<h1>Add/Edit Idea</h1>
@stop

@section('content')
	<div class="warnMessage"></div>
	<div class="main form formModal">
		@if (isset($idea))
			{!! Form::model($idea, ['route' => ['admin.idea.update', $idea->id], 'method' => 'put', 'id' => 'updateIdeaForm']) !!}
		@else
			{!! Form::open(['route' => 'admin.idea.store', 'id'=>'storeIdeaForm']) !!}
		@endif

		<fieldset>
			<legend>Add/Edit Idea</legend>

			<div>
				{!! Form::label('client_id', 'Client:') !!}
				{!! Form::clientSelect('client_id', (isset($idea) ? $idea->client_id : null)) !!}
			</div>

			<div>
				{!! Form::label('project_id', 'Project:') !!}
				{!! Form::projectSelect('project_id', (isset($idea) ? $idea->project_id : null)) !!}
			</div>

			<div>
				{!! Form::label('name', 'Idea name:') !!}
				{!! Form::text('name') !!}
			</div>

			<div>
				{!! Form::label('comment', 'Idea comment:') !!}
				{!! Form::textarea('comment') !!}
			</div>
		</fieldset>

		<div class="formAction">
			{!! Form::submit('Save', ['class' => 'buttonAction']) !!}
			&nbsp;
			<a class="buttonClear" href="{{ URL::previous() }}">Cancel</a>
		</div>

		{!! Form::close() !!}
	</div>
@stop

@section('javascript')
	<script type="application/javascript">
        @if (isset($ticket))
            bindErrorAjaxModal('#editModal #updateIdeaForm', '{{ route('admin.idea.update', ['id'=>$idea->id]) }}', 'POST');
        @else
            bindErrorAjaxModal('#editModal #storeIdeaForm', '{{ route('admin.idea.store') }}', 'POST');
		@endif
	</script>
@stop