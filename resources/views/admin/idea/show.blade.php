@extends('layouts.modal')

@section('header')
	@include('_partials.idea.header', ['idea' => $idea])
@stop

@section('content')
	<article>
		<div>
			<fieldset>
				<legend>Details</legend>

				<div>
					<label>Created At</label>
					{{ $idea->created_at }}
				</div>
			</fieldset>

			<fieldset>
				<legend>Comment</legend>
				{!! nl2br($idea->comment) !!}
			</fieldset>
		</div>

		<div>
			<h3>Comments:</h3>

			@foreach ($idea->comments as $comment)
				@include('_partials.comment.show', ['comment' => $comment])
			@endforeach

			{!! Form::open(['route' => ['admin.idea.comment', $idea->id]]) !!}
			@include('_partials.comment.edit')
			{!! Form::close() !!}
		</div>
	</article>

	{{--<aside>--}}
		{{--<fieldset>--}}
			{{--<legend>People</legend>--}}

			{{--<div>--}}
				{{--<label>Reported By</label>--}}
				{{--{{ DisplayService::displayUser($idea->user) }}--}}
			{{--</div>--}}
		{{--</fieldset>--}}

		{{--<fieldset>--}}
			{{--<legend>Dates</legend>--}}

			{{--<div>--}}
				{{--<label>Created At</label>--}}
				{{--{{ $idea->created_at }}--}}
			{{--</div>--}}

			{{--<div>--}}
				{{--<label>Updated At</label>--}}
				{{--{{ $idea->updated_at }}--}}
			{{--</div>--}}
		{{--</fieldset>--}}
	{{--</aside>--}}
@stop