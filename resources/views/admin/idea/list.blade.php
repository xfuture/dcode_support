@extends('layouts.admin')

@section('controlPanelHeader')
    <a class="buttonAction edit" data-href="{{ route('admin.idea.create') }}"><span>+</span></a>
    <h3>Ideas</h3>
@endsection

@section('controlPanelBody')
    {!! Form::open(['route' => 'admin.idea', 'method' => 'GET', 'id' => 'searchForm']) !!}
    {!! Form::hidden('export', '0') !!}
    {!! Form::hidden('sort', (\Request::get('sort') !== null ? \Request::get('sort') : 'created_at')) !!}
    {!! Form::hidden('sortDir', (\Request::get('sortDir') == 'asc' ? 'asc' : 'desc')) !!}

    <div>
        <div class="options">
            {!! Form::label('client_id', 'Client') !!}
            {!! Form::clientSelect('client_id', \Request::get('client_id')) !!}
        </div>
        <hr/>

        <div class="options">
            {!! Form::label('project_id', 'Project') !!}
            {!! Form::projectSelect('project_id', \Request::get('project_id')) !!}
        </div>
        <hr/>
        <div class="search">
            {!! Form::text('name', \Request::get('name'), ['placeholder' => 'Idea Name']) !!}
        </div>
        <br/>

        <div>
            {!! Form::submit('Search', ['class' => 'buttonAction']) !!}
            <a class="buttonClear" href="{{ route('client.project') }}">Clear</a>
        </div>
    </div>
    {!! Form::close() !!}
@endsection

@section('content')
    <div class="admin__table">
        @if ($ideas->count() > 0)
            <table>
                <tbody>
                @foreach ($ideas as $idea)

                    <tr>
                        <td width="10px">
                            <button class="vote-btn {{ $idea->hasUserVoted() > 0 ? 'complete': ''}}" data-href-vote="{{ route('admin.ideavote.vote')}}" data-ideaId='{{ $idea->id }}'>
                                <span class="icon"></span>
                                <span class="vote-count">{{$idea->ideaVotes()->get()->count()}}</span>
                            </button>
                        </td>
                        <td>
                            <h5><a class="edit" href="{{ route('admin.idea.show', $idea->id) }}">{{ $idea->name }}</a></h5>
                            <div>{{ $idea->comment }}</div>
                        </td>

                        <td class="alignCenter" width="10px"><a class="edit" href="{{ route('admin.idea.edit', $idea->id) }}"><span
                                        class="dicon-edit"></span></a></td>
                        <td class="alignCenter"
                            width="10px">{!! \Html::delete('admin.idea.destroy', $idea->id, '<span class="dicon-delete"></span>') !!}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>

            <div class="admin__table--pagination">
                {!! $ideas->appends(\Request::except('page'))->render() !!}
            </div>
        @else
            <div class="admin__table--empty">
                <h3>No ideas</h3>
                <p>No ideas ...</p>
            </div>
        @endif
    </div>
@endsection