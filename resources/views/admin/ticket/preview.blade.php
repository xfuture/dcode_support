@extends('layouts.modal')

@section('header')
    <h1>{{ $ticket->name }}</h1>
@endsection

@section('content')
    <form>
        <div class="row collapse">
            <div class="large-6 columns">
                <label>Client</label>
                {{ \DCODESupport\Services\DisplayService::displayClient($ticket->client) }}
            </div>

            <div class="large-6 columns">
                <label>Project</label>
                {{ \DCODESupport\Services\DisplayService::displayProject($ticket->project) }}
            </div>
        </div>

        <div class="row collapse">
            <div class="large-6 columns">
                <label>Assigned User</label>
                {{ \DCODESupport\Services\DisplayService::displayUser($ticket->assignedUser) }}
            </div>

            <div class="large-6 columns">
                <label>Date Due</label>
                {!! Form::datePicker('date_due', (isset($ticket) ? $ticket->date_due : null)) !!}
                {{--{{ \DCODESupport\Services\DisplayService::displayDate($ticket->date_due) }}--}}
            </div>
        </div>
    </form>

    <div>
        <label>Comment</label>
        {!! nl2br($ticket->comment) !!}
    </div>
    <hr/>

    <div>
        <div>
            <label>Attached Files</label>
            {!! Form::open(['url' => route('admin.file.store'), 'class' => 'dropzone', 'files'=>true, 'id'=>'fileUpload']) !!}
            {!! Form::close() !!}
            <ul id="listUpload">
                @foreach ($ticket->files as $file)
                    <li>
                        <a style=" color: #1baad2; text-decoration: underline;" href="{{ \DCODESupport\Services\AWS\AWSService::getSignedURL(\DCODESupport\Services\File\FileService::getTaskAttachedFilesURL() . $file->hash_name . '/' . $file->original_name, 1200) }}">{{ $file->original_name  }}</a>
                        <a class="dicon-delete" style="padding: 10px"></a>
                    </li>
                @endforeach
            </ul>
        </div>
    </div>
    <hr/>

    <div class="activityLog">
        {!! Form::open(['route' => ['admin.ticket.comment', $ticket->id]]) !!}
        @include('_partials.comment.edit')
        {!! Form::close() !!}

        @foreach ($ticket->comments as $comment)
            @include('_partials.comment.show', ['comment' => $comment])
        @endforeach
    </div>
@stop

@section('javascript')
    <script type="application/javascript">
        bindFileUploadForm('#fileUpload', {ticket_id : '{{$ticket->id}}'}, '{{ \DCODESupport\Services\File\FileService::getTaskAttachedFilesURL() }}');
    </script>
@endsection