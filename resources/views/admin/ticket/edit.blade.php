@extends('layouts.modal')

@section('header')
	<h1>Add Ticket</h1>
@stop

@section('content')
	<div class="warnMessage"></div>
	<div class="main form formModal">
		@if (isset($ticket))
			{!! Form::model($ticket, ['route' => ['admin.ticket.update', $ticket->id], 'method' => 'put', 'id'=>'updateTickerForm']) !!}
		@else
			{!! Form::open(['route' => 'admin.ticket.store', 'id'=>'storeTickerForm']) !!}
		@endif

		<fieldset>
			<div>{{ $errors->first('name') }}</div>
			<legend>Add/Edit Ticket Client</legend>

			<div class="row">
				<div class="large-6 columns">
					{!! Form::label('client_id', 'Client:') !!}
					@if(isset($client_id))
						{!! Form::clientSelect('client_id', $client_id) !!}
					@else
						{!! Form::clientSelect('client_id', (isset($ticket) ? $ticket->client_id : null)) !!}
					@endif
				</div>

				<div class="large-6 columns">
					{!! Form::label('project_id', 'Project:') !!}
					@if(isset($project_id))
						{!! Form::projectSelect('project_id', $project_id) !!}
					@else
						{!! Form::projectSelect('project_id', (isset($ticket) ? $ticket->project_id : null)) !!}
					@endif
				</div>
			</div>
		</fieldset>

		<fieldset>

			<legend>Add/Edit Ticket Information</legend>

			<div>
				{!! Form::label('name', 'Name:') !!}
				{!! Form::text('name') !!}
			</div>

			<div>
				{!! Form::label('comment', 'Comment:') !!}
				{!! Form::textarea('comment') !!}
			</div>

			<div>
				{!! Form::label('estimated_time', 'Estimated Time:') !!}
				{!! Form::number('estimated_time',null,['class' => 'form-control','step'=>'any']) !!}
			</div>

			<div>
				{!! Form::label('date_due', 'Date Due:') !!}
				{!! Form::datePicker('date_due', (isset($ticket) ? $ticket->date_due : null)) !!}
			</div>
		</fieldset>

		<fieldset>
			<legend>Add/Edit Ticket Assigned User</legend>

			<div>
				{!! Form::label('assigned_user_id', 'Assigned User:') !!}
				{!! Form::assignedUserSelect('assigned_user_id', (isset($ticket) ? $ticket->assigned_user_id : null), (isset($ticket) ? $ticket->project : null)) !!}
			</div>
		</fieldset>

		<fieldset>
			<legend>Access</legend>

			<div>
				{!! Form::hidden('internal', '0') !!}
				{!! Form::checkbox('internal', '1') !!}
				{!! Form::label('internal', 'Internal to DCODE') !!}
			</div>
		</fieldset>

		<div class="formAction">


			{!! Form::submit('Save', ['class' => 'buttonAction']) !!}

			{{--use java script to validate the form on Submit--}}



			<a class="buttonClear" id="buttonClear" href="{{ URL::previous() }}">Cancel</a>
		</div>

		{!! Form::close() !!}


	</div>

@stop

@section('javascript')
	<script type="application/javascript">
        @if (isset($ticket))
            bindErrorAjaxModal('#editModal #updateTickerForm', '{{ route('admin.ticket.update', ['id'=>$ticket->id]) }}', 'POST');
        @else
            bindErrorAjaxModal('#editModal #storeTickerForm', '{{ route('admin.ticket.store') }}', 'POST');
		@endif
	</script>
@stop