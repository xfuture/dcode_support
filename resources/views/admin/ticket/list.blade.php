@extends('layouts.admin')

@section('controlPanelHeader')
    <a class="buttonAction edit" href="{{ route('admin.ticket.create') }}"><span>+</span></a>
    <h3>Tickets</h3>
@endsection

@section('controlPanelBody')
    {!! Form::open(['route' => 'admin.ticket', 'method' => 'GET', 'id' => 'searchForm']) !!}
    {!! Form::hidden('export', '0') !!}
    {!! Form::hidden('sort', (\Request::get('sort') !== null ? \Request::get('sort') : 'created_at')) !!}
    {!! Form::hidden('sortDir', (\Request::get('sortDir') == 'asc' ? 'asc' : 'desc')) !!}

    <!-- hr / -->

    <div class="options">
        {!! Form::label('client_id', 'Client') !!}
        {!! Form::clientSelect('client_id', \Request::get('client_id')) !!}
    </div>
    <hr/>

    <div class="options">
        {!! Form::label('project_id', 'Project') !!}
        {!! Form::projectSelect('project_id', \Request::get('project_id')) !!}
    </div>
    <hr/>

    <div class="options">
        {!! Form::label('assigned_user_id', 'Assigned User') !!}
        {!! Form::assignedUserSelect('assigned_user_id', \Request::get('assigned_user_id')) !!}
    </div>
    <hr/>

    <div class="options">
        {!! Form::label('status_id', 'Status') !!}
        {!! Form::statusSelect('status_id', \Request::get('status_id')) !!}
    </div>
    <hr/>

    <div class="search">
        {!! Form::text('name', \Request::get('name'), ['placeholder' => 'Ticket Name']) !!}
    </div>
    <div>
        {!! Form::checkbox('archived', \DCODESupport\Models\Status::where('name', 'Archived')->first()->id, \Request::get('archived')) !!}
        {!! Form::label('archived', 'Show Archived Tasks') !!}
    </div>
    <br/>

    <div>
        {!! Form::submit('Search', ['class' => 'buttonAction']) !!}
        <a class="buttonClear" href="{{ route('admin.ticket') }}">Clear</a>
    </div>
    {!! Form::close() !!}
@endsection

@section('content')
    <div>
        <ul class="alert alert-danger">
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    <div class="admin__table">
        @if ($tickets->count() > 0)
            <table>
                <thead>
                <?php
                $columns = array(
                    array(
                        'id' => 'name',
                        'value' => 'Ticket Description',
                        'sort' => true,
                        'width' => '150'
                    ),
                    array(
                        'id' => 'project',
                        'value' => 'Project Name / Client Name',
                        'sort' => true,
                        'width' => '100'
                    ),
                    array(
                        'id' => 'assigned_user',
                        'value' => 'Assigned To',
                        'sort' => true,
                        'width' => '100'
                    ),
                    array(
                        'id' => 'created_at',
                        'value' => 'Date Created',
                        'sort' => true,
                        'width' => '100'
                    ),
                    array(
                        'id' => 'due_date',
                        'value' => 'Date Due',
                        'sort' => true,
                        'width' => '75'
                    ),
                    array(
                        'id' => '',
                        'value' => 'Status',
                        'sort' => false,
                        'width' => '100'
                    )
                );
                ?>

                @include('_partials.layout.tableHeader', ['columns' => $columns, 'action' => 3])
                </thead>

                <tbody>
                @foreach ($tickets as $ticket)
                    <tr>
                        <td>
                            <a href="{{ route('admin.ticket.show', $ticket->id) }}">{{ $ticket->name }}</a>
                            <span class="summary">{{ DisplayService::displayExcerpt($ticket->comment) }}</span>
                        </td>
                        <td>
                            {{ DisplayService::displayProjectCode($ticket->project) }} {{ DisplayService::displayProject($ticket->project) }}
                            <span class="summary">{{ !empty($ticket->client) ? DisplayService::displayClient($ticket->client) : '' }}</span>
                        </td>
                        <td>{{ DisplayService::displayuser($ticket->assignedUser) }}</td>
                        <td>{{ date("d/m/Y", strtotime($ticket->created_at)) }}</td>
                        <td>
                            @if ($ticket->date_due != null && $ticket->date_due > 0)
                                <span class="{{ ($ticket->date_due < time() ? 'alert' : 'success') }}">{{ DisplayService::displayDate($ticket->date_due) }}</span>
                            @endif
                        </td>
                        <td>{!! \Html::statusSelect($ticket) !!}</td>
                        {{--<td>{{$ticket}}</td>--}}

                        <td class="alignCenter" width="20"><a class="edit"
                                                              href="{{ route('admin.timesheet.create', $ticket->id) }}"><span
                                        class="fa fa-clock-o"></span></a></td>

                        @if (\Auth::user()->admin == '1' || \Auth::id() == $ticket->user_id)
                            <td class="alignCenter" width="20"><a class="edit"
                                                                  href="{{ route('admin.ticket.edit', $ticket->id) }}"><span
                                            class="dicon-edit"></span></a></td>
                            <td class="alignCenter"
                                width="20">{!! \Html::delete('admin.ticket.destroy', $ticket->id, '<span class="dicon-delete"></span>') !!}</td>
                        @else
                            <td colspan="2"> &nbsp; </td>
                        @endif
                    </tr>
                @endforeach
                </tbody>

            </table>

            <div class="admin__table--pagination">
                {!! $tickets->appends(\Request::except('page'))->render() !!}
            </div>
        @else
            <div class="admin__table--empty">
                <h3>No tickets</h3>
                <p>No tickets ...</p>
            </div>
        @endif
    </div>
@endsection