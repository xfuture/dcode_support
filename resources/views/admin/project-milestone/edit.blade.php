@extends('layouts.modal')

@section('header')
	@include('_partials.project.header', ['project' => $project])
@stop

@section('content')
	<div class="warnMessage"></div>
	<div class="main form formModal">
		@if (isset($projectMilestone))
			{!! Form::model($projectMilestone, ['route' => ['admin.project-milestone.update', $projectMilestone->id], 'method' => 'put', 'id'=>'updateProjectMilestoneForm']) !!}
		@else
			{!! Form::open(['route' => ['admin.project-milestone.save', $project->id], 'id'=>'saveProjectMilestoneForm']) !!}
		@endif

		<fieldset>
			<legend>Add/Edit Milestone</legend>

			<div>
				{!! Form::label('name', 'Name:') !!}
				{!! Form::text('name') !!}
			</div>

			<div>
				{!! Form::label('scheduled', 'Scheduled:') !!}
				{!! Form::datePicker('scheduled', (isset($projectMilestone) ? $projectMilestone->scheduled : null)) !!}
			</div>

			<div>
				{!! Form::label('complete', 'Complete:') !!}
				{!! Form::datePicker('complete', (isset($projectMilestone) ? $projectMilestone->complete : null)) !!}
			</div>
		</fieldset>

		<div class="formAction">
			{!! Form::submit('Save', ['class' => 'buttonAction']) !!}
			&nbsp;
			<a class="buttonClear" href="{{ URL::previous() }}">Cancel</a>
		</div>

		{!! Form::close() !!}
	</div>
@stop

@section('javascript')
	<script type="application/javascript">
        @if (isset($projectMilestone))
            bindErrorAjaxModal('#editModal #updateProjectMilestoneForm', '{{ route('admin.project-milestone.update', ['id'=>$projectMilestone->id]) }}', 'POST');
        @else
            bindErrorAjaxModal('#editModal #saveProjectMilestoneForm', '{{ route('admin.project-milestone.save', ['id'=>$project->id]) }}', 'POST');
		@endif
	</script>
@stop