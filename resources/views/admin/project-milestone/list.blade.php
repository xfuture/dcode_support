@extends('layouts.admin')

@section('controlPanelHeader')
    <a class="buttonAction edit" data-href="{{ route('admin.project.edit', $project->id) }}"><span>+</span></a>
    <h3>{{ $project->name }}</h3>
@endsection

@section('controlPanelBody')
    <div class="controlPanel__content white">
        <div>
            <label>Client</label>
            {{ \DCODESupport\Services\DisplayService::displayClient($project->client) }}
        </div>

        <hr/>

        <div>
            <label>Start</label>
            {{ \DCODESupport\Services\DisplayService::displayDate(strtotime($project->created_at)) }}
        </div>

        <hr/>

        <div>
            <label>User</label>
            {{ \DCODESupport\Services\DisplayService::displayUser($project->user) }}
        </div>

        <hr/>

        <div>
            <label>Status</label>
            @if ($project->latestUpdate() != null)
                <span class="{{ $project->latestUpdate()->projectStatus->class }}">{{ DisplayService::displayProjectStatus($project->latestUpdate()->projectStatus) }}</span>
                <br/>
                @if ($project->latestUpdate()->comments()->count() > 0)
                    <small>{{ DisplayService::displayExcerpt($project->latestUpdate()->comments()->orderBy('created_at', 'asc')->first()->comment) }}</small>
                @endif
            @else
                NA
            @endif
        </div>
    </div>
@endsection

@section('controlPanelHeaderSub-1')
    <a class="buttonAction edit" data-href="{{ route('admin.project-milestone.add', $project->id) }}"><span>+</span></a>
    <h3>Milestones</h3>
@endsection

@section('controlPanelBodySub-1')
        {!! Form::open(['route' => ['admin.project-milestone', $project->id], 'method' => 'GET', 'id' => 'searchForm']) !!}
        {!! Form::hidden('export', '0') !!}
        {!! Form::hidden('sort', (\Request::get('sort') !== null ? \Request::get('sort') : 'created_at')) !!}
        {!! Form::hidden('sortDir', (\Request::get('sortDir') == 'asc' ? 'asc' : 'desc')) !!}

        <div>
            <div class="search">
                {!! Form::text('name', \Request::get('name'), ['placeholder' => 'Name']) !!}
            </div>
            <br/>

            <div>
                {!! Form::submit('Search', ['class' => 'buttonAction']) !!}
                {{--{!! Form::submit('Export', ['class' => 'buttonAction']) !!}--}}
                <a class="buttonClear" href="{{ route('client.project') }}">Clear</a>
            </div>
        </div>

        {!! Form::close() !!}
@endsection

@section('content')
    <div class="admin__table">
        @if ($projectMilestones->count() > 0)
            <table>
                <thead>
                <?php
                $columns = array(
                    array(
                        'id' => 'name',
                        'value' => 'Name',
                        'sort' => true,
                        'width' => '100'
                    ),
                    array(
                        'id' => 'scheduled_date',
                        'value' => 'Scheduled',
                        'sort' => true,
                        'width' => '100'
                    ),
                    array(
                        'id' => 'complete_date',
                        'value' => 'Complete',
                        'sort' => true,
                        'width' => '100'
                    ),
                    array(
                        'id' => 'user',
                        'value' => 'Added By',
                        'sort' => false,
                        'width' => '150'
                    ),
                    array(
                        'id' => 'created_at',
                        'value' => 'Created',
                        'sort' => true,
                        'width' => '75'
                    )
                );
                ?>

                @include('_partials.layout.tableHeader', ['columns' => $columns, 'action' => 2])
                </thead>

                <tbody>
                @foreach ($projectMilestones as $projectMilestone)
                    <tr>
                        <td><a class="edit"
                               href="{{ route('admin.project-milestone.edit', $projectMilestone->id) }}">{{ $projectMilestone->name }}</a>
                        </td>
                        <td>{{ DisplayService::displayDate($projectMilestone->scheduled) }}</td>
                        <td>{{ DisplayService::displayDate($projectMilestone->complete) }}</td>
                        <td>{{ DisplayService::displayUser($projectMilestone->user) }}</td>
                        <td>{{ date("d/m/Y", strtotime($projectMilestone->created_at)) }}</td>

                        @if (\Auth::user()->admin == '1')
                            <td class="alignCenter"
                                width="20">{!! \Html::delete('admin.project-milestone.destroy', $projectMilestone->id, '<span class="dicon-delete"></span>') !!}</td>
                        @else
                            <td colspan="2"> &nbsp; </td>
                        @endif

                    </tr>
                @endforeach
                </tbody>
            </table>

            <div class="admin__table--pagination">
                {!! $projectMilestones->appends(\Request::except('page'))->render() !!}
            </div>
        @else
            <div class="admin__table--empty">
                <h3>No project milestones</h3>
                <p>No project milestones ...</p>
            </div>
        @endif
    </div>
@endsection