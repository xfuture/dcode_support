@extends('layouts.admin')

@section('controlPanelHeader')
    <a class="buttonAction edit" data-href="{{ route('admin.client.edit', $client->id) }}"><span>+</span></a>
    <h3>{{ $client->name }}</h3>
@endsection

@section('controlPanelBody')
    <div class="controlPanel__content white">
        <div>
            <label>Name</label>
            {{ \DCODESupport\Services\DisplayService::displayClient($client) }}
        </div>

        <hr/>

        <div>
            <label>Shote Name</label>
            {{ $client->short_name }}
        </div>

        <hr/>

        <div>
            <label>Start</label>
            {{ \DCODESupport\Services\DisplayService::displayDate(strtotime($client->created_at)) }}
        </div>
    </div>
@endsection

@section('controlPanelHeaderSub-1')
    <a class="buttonAction edit" data-href="{{ route('admin.project.add', $client->id) }}"><span>+</span></a>
    <h3>Projects</h3>
@endsection

@section('controlPanelBodySub-1')
    {!! Form::open(['route' => ['admin.client.show', $client->id], 'method' => 'GET', 'id' => 'searchForm']) !!}
    {!! Form::hidden('export', '0') !!}
    {!! Form::hidden('sort', (\Request::get('sort') !== null ? \Request::get('sort') : 'created_at')) !!}
    {!! Form::hidden('sortDir', (\Request::get('sortDir') == 'asc' ? 'asc' : 'desc')) !!}

    <div class="options">
        {!! Form::label('sort', 'Sort By') !!}
        {!! Form::select('sort', ['code' => 'Code', 'name' => 'Name', 'client' => 'Client', 'created_at' => 'Created At'], (\Request::get('sort') !== null ? \Request::get('sort') : 'created_at')) !!}
    </div>
    <hr/>

    <div class="search">
        {!! Form::text('code', \Request::get('code'), ['placeholder' => 'Project Code']) !!}
    </div>

    <div class="search">
        {!! Form::text('name', \Request::get('name'), ['placeholder' => 'Project Name']) !!}
    </div>
    <div>
        {!! Form::checkbox('active', '0', \Request::get('active')) !!}
        {!! Form::label('active', 'Show Archived Projects') !!}
    </div>
    <br/>
    <div>
        {!! Form::submit('Search', ['class' => 'buttonAction']) !!}
        <a class="buttonClear" href="{{ route('client.project') }}">Clear</a>
    </div>

    {!! Form::close() !!}
@endsection

@section('content')
    <div style="width: 100%">
        <div>
            <ul class="tabs" data-tabs id="client-tabs">
                <li class="tabs-title is-active">
                    <a aria-selected="true" href="#panel1">Projects</a>
                </li>
                <li class="tabs-title">
                    <a href="#panel2">Users</a>
                </li>
            </ul>
        </div>

        <div class="tabs-content" data-tabs-content="client-tabs">
            <div class="tabs-panel is-active" id="panel1" style="background: #f0f0f0">
                <div class="cards">
                    @if ($projects->count() > 0)

                        @foreach ($projects as $project)
                            <div class="cardContainer">
                                <div class="card">
                                    <div>
                                        <header>
                                            @if (\Auth::user()->admin == '1')
                                                <menu>
                                                    <a class="edit"
                                                       data-href="{{ route('admin.project.edit', $project->id) }}"><span
                                                                class="dicon-edit"></span></a>
                                                    {!! \Html::delete('admin.project.destroy', $project->id, '<span class="dicon-delete"></span>') !!}
                                                    <br/>
                                                    <span class="fa fa-clock-o"></span> {{ number_format($project->outstandingTimesheetHours(), 2) }}
                                                    <a href="{{ route('admin.project.access', $project->id) }}"><span
                                                                class="fa fa-users"></span></a>
                                                </menu>
                                            @endif

                                            <a href="{{ route('admin.project.show', $project->id) }}">
                                                <h3>{{ $project->name }} {!! ($project->active == '1' ? '<span class="badge success">Y</span>' : '') !!}</h3>
                                            </a>
                                            <span>{{ $project->code }}</span>
                                        </header>
                                        <div class="cardBody">
                                            <div class="cardBody__item">
                                                <label>Client</label>
                                                @if($project->client == null)
                                                    NA
                                                @endif
                                                {{ DisplayService::displayClient($project->client) }}
                                            </div>

                                            <div class="cardBody__item">
                                                <label>Name</label>
                                                @if($project->name == null)
                                                    NA
                                                @endif
                                                {{ $project->name }}
                                            </div>

                                            <div class="cardBody__item">
                                                <label>Created</label>
                                                {{ date("d/m/Y", strtotime($project->created_at)) }}
                                            </div>

                                            <div class="cardBody__item">
                                                <label>Added By</label>
                                                @if($project->user == null)
                                                    NA
                                                @endif
                                                {{ DisplayService::displayUser($project->user) }}
                                            </div>

                                            <div class="cardBody__item">
                                                <label>Next Milestone</label>
                                                @if ($project->nextMilestone() != null)
                                                    {{ $project->nextMilestone()->name }}
                                                    <span class="label small {{ ($project->nextMilestone()->scheduled < time() ? 'alert' : 'success') }}">{{ DisplayService::displayDate($project->nextMilestone()->scheduled) }}</span>
                                                @else
                                                    NA
                                                @endif
                                            </div>

                                            <div class="cardBody__item">
                                                <label>Status</label>
                                                @if ($project->latestUpdate() != null)
                                                    <span class="{{ $project->latestUpdate()->projectStatus->class }}">{{ DisplayService::displayProjectStatus($project->latestUpdate()->projectStatus) }}</span>
                                                    <br/>
                                                    @if ($project->latestUpdate()->comments()->count() > 0)
                                                        <small>{{ DisplayService::displayExcerpt($project->latestUpdate()->comments()->orderBy('created_at', 'asc')->first()->comment) }}</small>
                                                    @endif
                                                @else
                                                    NA
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <footer>
                                        <a class="button round"
                                           href="{{ route('admin.project-milestone', $project->id) }}">Milestones</a>
                                        <a class="button round"
                                           href="{{ route('admin.project-update', $project->id) }}">Updates</a>
                                        <a class="button round"
                                           href="{{ route('admin.ticket', ['project_id' => $project->id]) }}"
                                           aria-describedby="ticketCount">Tickets <span class="badge alert"
                                                                                        id="ticketCount">{{ $project->activeTickets()->count() }}</span></a>
                                    </footer>
                                </div>
                            </div>
                        @endforeach

                        <div class="cards__pagination">
                            {!! $projects->appends(\Request::except('page'))->render() !!}
                        </div>
                    @else
                        <div class="cards__empty">
                            <h3>No projects</h3>
                            <p>No projects ...</p>
                        </div>
                    @endif
                </div>
            </div>

            <div class="tabs-panel" id="panel2">
                @if ($client->clientUsers->count() > 0)
                    @php
                        $users = $client->clientUsers;
                    @endphp
                    <div class="admin__table">
                        <table>
                            <thead>
                            <?php
                            $columns = array(
                                array(
                                    'id' => 'last_name',
                                    'value' => 'Name',
                                    'sort' => false,
                                    'width' => '300'
                                ),
                                array(
                                    'id' => 'company',
                                    'value' => 'Company',
                                    'sort' => false,
                                    'width' => '200'
                                ),
                                array(
                                    'id' => 'position',
                                    'value' => 'Position',
                                    'sort' => false,
                                    'width' => '200'
                                ),
                                array(
                                    'id' => 'location',
                                    'value' => 'Location',
                                    'sort' => false,
                                    'width' => '200'
                                ),
                                array(
                                    'id' => 'phone',
                                    'value' => 'Phone',
                                    'sort' => false,
                                    'width' => '200'
                                ),
                                array(
                                    'id' => 'internal',
                                    'value' => 'Internal',
                                    'sort' => false,
                                    'width' => '100'
                                ),
                                array(
                                    'id' => 'access',
                                    'value' => 'Access',
                                    'sort' => false,
                                    'width' => '100'
                                ),
                                array(
                                    'id' => 'manager',
                                    'value' => 'Manager',
                                    'sort' => false,
                                    'width' => '100'
                                ),
                                array(
                                    'id' => 'admin',
                                    'value' => 'Admin',
                                    'sort' => false,
                                    'width' => '100'
                                ),
                                array(
                                    'id' => 'created_at',
                                    'value' => 'Created',
                                    'sort' => false,
                                    'width' => '100'
                                )
                            );
                            ?>

                            @include('_partials.layout.tableHeader', ['columns' => $columns, 'action' => 2])
                            </thead>

                            <tbody>
                            @foreach ($users as $user)
                                <tr>
                                    <td>
                                        <a href="{{ route('admin.note', $user->id) }}">{{ DisplayService::displayUser($user) }}</a>
                                    </td>
                                    <td>{{ DisplayService::displayClient($user->client()->first()) }}</td>
                                    <td>{{ $user->position }}</td>
                                    <td>{{ $user->location }}</td>
                                    <td>{{ $user->phone }}</td>
                                    <td>{{ DisplayService::displayCheckbox($user->internal) }}</td>
                                    <td>{{ DisplayService::displayCheckbox($user->access) }}</td>
                                    <td>{{ DisplayService::displayCheckbox($user->manager) }}</td>
                                    <td>{{ DisplayService::displayCheckbox($user->admin) }}</td>
                                    <td>{{ date("d/m/Y", strtotime($user->created_at)) }}</td>
                                    <td class="alignCenter"><a class="edit"
                                                               href="{{ route('admin.user.edit', $user->id) }}"><span
                                                    class="dicon-edit"></span></a></td>
                                    <td class="alignCenter">{!! \Html::delete('admin.user.destroy', $user->id, '<span class="dicon-delete"></span>') !!}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                @else
                    <div class="cards__empty">
                        <h3>No users</h3>
                        <p>No users ...</p>
                    </div>
                @endif
            </div>
        </div>
    </div>
@endsection