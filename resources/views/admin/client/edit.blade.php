@extends('layouts.modal')

@section('header')
	<h1>Add / Edit Client</h1>
@stop

@section('content')
	<div class="warnMessage"></div>
	<div class="main form formModal">
		@if (isset($client))
			{!! Form::model($client, ['route' => ['admin.client.update', $client->id], 'method' => 'put', 'id'=>'updateClientForm']) !!}
		@else
			{!! Form::open(['route' => 'admin.client.store', 'id'=>'storeClientForm']) !!}
		@endif

		<fieldset>
			<div>
				{!! Form::label('name', 'Client name:') !!}
				{!! Form::text('name') !!}
			</div>

			<div>
				{!! Form::label('short_name', 'Short name:') !!}
				{!! Form::text('short_name') !!}
			</div>
		</fieldset>

		<div class="formAction">
			{!! Form::submit('Save', ['class' => 'buttonAction']) !!}
			&nbsp;
			<a class="buttonClear" href="{{ URL::previous() }}">Cancel</a>
		</div>

		{!! Form::close() !!}
	</div>
@stop

@section('javascript')
	<script type="application/javascript">
        @if (isset($client))
            bindErrorAjaxModal('#editModal #updateClientForm', '{{ route('admin.client.update', ['id'=>$client->id]) }}', 'POST');
        @else
            bindErrorAjaxModal('#editModal #storeClientForm', '{{ route('admin.client.store') }}', 'POST');
		@endif
		//Generate short name
		$('#name').on('change keyup input paste', function(){
			if($('#short_name').is(":empty")){
//                $('#short_name').val($('#name').val().match(/\b\w/g).join('').toUpperCase());
                $('#short_name').val($('#name').val().replace(/\s/g,''));
			}
		})
	</script>
@stop