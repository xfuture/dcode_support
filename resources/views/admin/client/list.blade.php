@extends('layouts.admin')

@section('controlPanelHeader')
	<a class="buttonAction edit" data-href="{{ route('admin.client.create') }}"><span>+</span></a>
	<h3>Clients</h3>
@endsection

@section('controlPanelBody')
	{!! Form::open(['route' => 'admin.client', 'method' => 'GET', 'id' => 'searchForm']) !!}
		{!! Form::hidden('export', '0') !!}
		{!! Form::hidden('sort', (\Request::get('sort') !== null ? \Request::get('sort') : 'created_at')) !!}
		{!! Form::hidden('sortDir', (\Request::get('sortDir') == 'asc' ? 'asc' : 'desc')) !!}

		<div class="options">
			{!! Form::label('sort', 'Sort By') !!}
			{!! Form::select('sort', ['name' => 'Name', 'created_at' => 'Created At'], (\Request::get('sort') !== null ? \Request::get('sort') : 'created_at')) !!}
		</div>
		<hr/>

		<div class="search">
			{!! Form::text('short_name', \Request::get('short_name'), ['placeholder' => 'Search Short Name ...']) !!}
		</div>

		<div class="search">
			{!! Form::text('name', \Request::get('name'), ['placeholder' => 'Search Name ...']) !!}
		</div>
		<br/>

		<div>
			{!! Form::submit('Search', ['class' => 'buttonAction']) !!}
			<a class="buttonClear" href="{{ route('admin.client') }}">Clear</a>
		</div>

	{!! Form::close() !!}
@endsection

@section('content')
	<div class="cards">
		@if ($clients->count() > 0)

			@foreach ($clients as $client)
				<div class="cardContainer">
					<div class="card">
						<header>
							<menu>
								<a class="edit" data-href="{{ route('admin.client.edit', $client->id) }}"><span class="dicon-edit"></span></a>
								{!! \Html::delete('admin.client.destroy', $client->id, '<span class="dicon-delete"></span>') !!}
							</menu>

							<a href="{{ route('admin.client.show', $client->id) }}"><h3>{{ $client->name }}</h3></a>
						</header>
						<div class="cardBody">
							<div class="cardBody__item">
								<label>Created</label>
								{{ date("d/m/Y", strtotime($client->created_at)) }}
							</div>
	
							<div class="cardBody__item">
								<label>Added By</label>
								{{ DisplayService::displayUser($client->user) }}
							</div>
						</div>
	
						<footer>
	
						</footer>
					</div>
				</div>
			@endforeach

			<div class="cards__pagination">
				{!! $clients->appends(\Request::except('page'))->render() !!}
			</div>
		@else
			<div class="cards__empty">
				<h3>No clients</h3>
				<p>No clients ...</p>
			</div>
		@endif
	</div>
@endsection
