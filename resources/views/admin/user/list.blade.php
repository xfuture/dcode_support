@extends('layouts.admin')

@section('controlPanelHeader')
    <a class="buttonAction edit" href="{{ route('admin.user.create') }}"><span>+</span></a>
    <h3>Users</h3>
@endsection

@section('controlPanelBody')
    {!! Form::open(['route' => 'admin.user', 'method' => 'GET', 'id' => 'searchForm']) !!}
    {!! Form::hidden('export', '0') !!}
    {!! Form::hidden('sort', (\Request::get('sort') !== null ? \Request::get('sort') : 'created_at')) !!}
    {!! Form::hidden('sortDir', (\Request::get('sortDir') == 'asc' ? 'asc' : 'desc')) !!}

    <div class="search">
        {!! Form::text('name', \Request::get('name'), ['placeholder' => 'User Name']) !!}
    </div>
    <br/>

    <div>
        {!! Form::submit('Search', ['class' => 'buttonAction']) !!}
        <a class="buttonClear" href="{{ route('admin.user') }}">Clear</a>
    </div>
    {!! Form::close() !!}
@endsection

@section('content')
    <div class="admin__table">
        @if ($users->count() > 0)
            <table>
                <thead>
                <?php
                $columns = array(
                    array(
                        'id' => 'last_name',
                        'value' => 'Name',
                        'sort' => true,
                        'width' => '300'
                    ),
                    array(
                        'id' => 'company',
                        'value' => 'Company',
                        'sort' => true,
                        'width' => '200'
                    ),
                    array(
                        'id' => 'position',
                        'value' => 'Position',
                        'sort' => true,
                        'width' => '200'
                    ),
                    array(
                        'id' => 'location',
                        'value' => 'Location',
                        'sort' => true,
                        'width' => '200'
                    ),
                    array(
                        'id' => 'phone',
                        'value' => 'Phone',
                        'sort' => true,
                        'width' => '200'
                    ),
                    array(
                        'id' => 'internal',
                        'value' => 'Internal',
                        'sort' => true,
                        'width' => '100'
                    ),
                    array(
                        'id' => 'access',
                        'value' => 'Access',
                        'sort' => true,
                        'width' => '100'
                    ),
                    array(
                        'id' => 'manager',
                        'value' => 'Manager',
                        'sort' => true,
                        'width' => '100'
                    ),
                    array(
                        'id' => 'admin',
                        'value' => 'Admin',
                        'sort' => true,
                        'width' => '100'
                    ),
                    array(
                        'id' => 'created_at',
                        'value' => 'Created',
                        'sort' => true,
                        'width' => '100'
                    )
                );
                ?>

                @include('_partials.layout.tableHeader', ['columns' => $columns, 'action' => 2])
                </thead>

                <tbody>
                @foreach ($users as $user)
                    <tr>
                        <td><a href="{{ route('admin.note', $user->id) }}">{{ DisplayService::displayUser($user) }}</a></td>
                        <td>{{ DisplayService::displayClient($user->client()->first()) }}</td>
                        <td>{{ $user->position }}</td>
                        <td>{{ $user->location }}</td>
                        <td>{{ $user->phone }}</td>
                        <td>{{ DisplayService::displayCheckbox($user->internal) }}</td>
                        <td>{{ DisplayService::displayCheckbox($user->access) }}</td>
                        <td>{{ DisplayService::displayCheckbox($user->manager) }}</td>
                        <td>{{ DisplayService::displayCheckbox($user->admin) }}</td>
                        <td>{{ date("d/m/Y", strtotime($user->created_at)) }}</td>
                        <td class="alignCenter"><a class="edit" href="{{ route('admin.user.edit', $user->id) }}"><span
                                        class="dicon-edit"></span></a></td>
                        <td class="alignCenter">{!! \Html::delete('admin.user.destroy', $user->id, '<span class="dicon-delete"></span>') !!}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>

            <div class="admin__table--pagination">
                {!! $users->appends(\Request::except('page'))->render() !!}
            </div>
        @else
            <div class="admin__table--empty">
                <h3>No users</h3>
                <p>No users ...</p>
            </div>
        @endif
    </div>
@endsection