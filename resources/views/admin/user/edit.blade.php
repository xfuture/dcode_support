@extends('layouts.modal')

@section('header')
    <h1>Add/Edit User</h1>
@stop

@section('content')
    <div class="warnMessage"></div>
    <div class="main form formModal">
        @if (isset($user))
            {!! Form::model($user, ['route' => ['admin.user.update', $user->id], 'method' => 'put', 'id' => 'updateUserForm']) !!}
        @else
            {!! Form::open(['route' => 'admin.user.store', 'id' => 'storeUserForm']) !!}
        @endif

        <fieldset>
            <legend>User</legend>

            <div class="row">
                <div class="large-6 columns">
                    {!! Form::label('first_name', 'First name:') !!}
                    {!! Form::text('first_name') !!}
                </div>

                <div class="large-6 columns">
                    {!! Form::label('last_name', 'Last name:') !!}
                    {!! Form::text('last_name') !!}
                </div>
            </div>

            <div>
                {!! Form::label('email', 'Email:') !!}
                {!! Form::text('email') !!}
            </div>

            <div>
                {!! Form::label('password', 'Password:') !!}
                {!! Form::password('password') !!}
            </div>

            <div>
                {!! Form::label('client_id', 'Client:') !!}
                {!! Form::clientSelect('client_id', (isset($user) ? $user->client_id : null)) !!}
            </div>
        </fieldset>

        <fieldset>
            <legend>Contact Information</legend>

            <div>
                    {!! Form::label('position', 'Position:') !!}
                    {!! Form::text('position') !!}
            </div>

            <div>
                {!! Form::label('location', 'Location:') !!}
                {!! Form::text('location') !!}
            </div>

            <div>
                {!! Form::label('phone', 'Phone:') !!}
                {!! Form::text('phone') !!}
            </div>
        </fieldset>

        <fieldset>
            <legend>Permissions</legend>

            <div class="row">
                <div class="large-4 columns">
                    {!! Form::hidden('internal', 0) !!}
                    {!! Form::checkbox('internal') !!}
                    {!! Form::label('internal', 'Internal to DCODE') !!}
                </div>

                <div class="large-4 columns">
                    {!! Form::hidden('access', 0) !!}
                    {!! Form::checkbox('access') !!}
                    {!! Form::label('access', 'Access') !!}
                </div>

                <div class="large-4 columns">
                    {!! Form::hidden('manager', 0) !!}
                    {!! Form::checkbox('manager') !!}
                    {!! Form::label('manager', 'Manager') !!}
                </div>

                <div class="large-4 columns">
                    {!! Form::hidden('admin', 0) !!}
                    {!! Form::checkbox('admin') !!}
                    {!! Form::label('admin', 'Admin') !!}
                </div>
            </div>
        </fieldset>

        <div class="formAction">
            {!! Form::submit('Save', ['class' => 'buttonAction']) !!}
            <a class="buttonClear" href="{{ URL::previous() }}">Cancel</a>
        </div>

        {!! Form::close() !!}
    </div>
@stop

@section('javascript')
    <script type="application/javascript">
        @if (isset($user))
            bindErrorAjaxModal('#editModal #updateUserForm', '{{ route('admin.user.update', ['id'=>$user->id]) }}', 'POST');
        @else
            bindErrorAjaxModal('#editModal #storeUserForm', '{{ route('admin.user.store') }}', 'POST');
        @endif
    </script>
@stop