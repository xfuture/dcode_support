@extends('layouts.admin')

@section('controlPanelHeader')
	<a class="buttonAction edit" data-href="{{ route('admin.project.edit', $project->id) }}"><span>+</span></a>
	<h3>{{ $project->name }}</h3>
@endsection

@section('controlPanelBody')
	<div class="controlPanel__content white">
		<div>
			<label>Client</label>
			{{ \DCODESupport\Services\DisplayService::displayClient($project->client) }}
		</div>

		<hr />
		
		<div>
			<label>Start</label>
			{{ \DCODESupport\Services\DisplayService::displayDate(strtotime($project->created_at)) }}
		</div>

		<hr />

		<div>
			<label>User</label>
			{{ \DCODESupport\Services\DisplayService::displayUser($project->user) }}
		</div>

		<hr />

		<div>
			<label>Status</label>
			@if ($project->latestUpdate() != null)
				<span class="{{ $project->latestUpdate()->projectStatus->class }}">{{ DisplayService::displayProjectStatus($project->latestUpdate()->projectStatus) }}</span><br />
				@if ($project->latestUpdate()->comments()->count() > 0)
					<small>{{ DisplayService::displayExcerpt($project->latestUpdate()->comments()->orderBy('created_at', 'asc')->first()->comment) }}</small>
				@endif
			@else
				NA
			@endif
		</div>

		<section class="header">
			<h3>Milestones</h3>
		</section>

		<div>
			@foreach ($project->projectMilestones as $projectMilestone)
				<div>
					{{ $projectMilestone->name }}
					<div>
						<small>
							@if(!empty($project->nextMilestone()))
								<span class="right {{ ($project->nextMilestone()->scheduled < time() ? 'alert' : 'success') }}">{{ ($projectMilestone->complete != null ? 'Complete' : 'Outstanding') }}</span>
							@endif
							{{ \DCODESupport\Services\DisplayService::displayDate($projectMilestone->scheduled) }}
						</small>
					</div>
				</div>

				<hr />
			@endforeach
		</div>
	</div>
@endsection

@section('content')
	<div class="activityLog">
		<header>
			<div class="row">
				<div class="columns shrink">
					<h3>Updates</h3>
				</div>
				<div class="columns shrink">
					<div>Tasks <span class="success badge"><i class="dicon-tasks"></i></span></div>
					<div>Messages <span class="dicon-messages"></span></div>
					<div>Milestones <span class="dicon-milestone"></span></div>
				</div>
			</div>
		</header>

		@foreach ($project->projectUpdates()->orderBy('created_at', 'desc')->get() as $projectUpdate)
			@if ($projectUpdate->comments()->count() > 0)
				@foreach ($projectUpdate->comments as $comment)
					<div class="activity">
						<div class="activity__icon badge">
							<span class="dicon-messages"></span>
						</div>

						<div class="activity__comment">
							<div class="activity__comment--date">
								{{ \DCODESupport\Services\DisplayService::displayDate(strtotime($comment->created_at)) }}<br />
								<small>{{ $projectUpdate->created_at->diffForHumans() }}</small>
							</div>

							<h4>{{ \DCODESupport\Services\DisplayService::displayProjectStatus($projectUpdate->projectStatus) }}</h4>
							<div>{{ $comment->comment }}</div>

							<div class="activity__comment--meta">
								<label>Created By:</label>
								{{ \DCODESupport\Services\DisplayService::displayUser($comment->user) }}
							</div>
						</div>
					</div>
				@endforeach
			@else
				<div class="activity">
					<span class="dicon-messages"></span>

					<div class="activity__comment">
						<div class="activity__comment--date">
							{{ \DCODESupport\Services\DisplayService::displayDate(strtotime($projectUpdate->created_at)) }}<br />
							<small>{{ $projectUpdate->created_at->diffForHumans() }}</small>
						</div>

						<h4>{{ \DCODESupport\Services\DisplayService::displayProjectStatus($projectUpdate->projectStatus) }}</h4>

						<div>
							<label>Created By:</label>
							{{ \DCODESupport\Services\DisplayService::displayUser($projectUpdate->user) }}
						</div>
					</div>
				</div>
			@endif
		@endforeach
	</div>

	<div class="taskPanel">
		<header>
			<div class="row collapse">
				<div class="columns shrink">
					<h3>Tasks</h3>
				</div>
				<div class="columns">
					<menu>
						<a href="#">Pending <span class="badge">{{ $project->pendingTickets()->count() }}</span></a>
						<a href="#">Outstanding <span class="alert badge">{{ $project->outstandingTickets()->count() }}</span></a>
						<a href="#">Completed <span class="success badge">{{ $project->completedTickets()->count() }}</span></a>
						<a href="#">My Tasks <span class="warning badge">{{ $project->myTickets()->count() }}</span></a>
					</menu>
				</div>
				@if($project->active)
					<div class="columns shrink">
						<a class="buttonAction edit" data-href="{{ route('admin.ticket.create') }}" data-store='{"client_id":"{{$project->client_id}}", "project_id":"{{$project->id}}"}'><span>+</span></a>
					</div>
				@endif
			</div>
		</header>
		<table>
			<thead>
				<tr>
					<th>Task Description</th>
					<th>Assigned To</th>
					<th>Due Date</th>
					<th>Status</th>
					<th colspan="2"><!-- --></th>
				</tr>
			</thead>
			<tbody>
				@foreach ($project->tickets as $ticket)
					<tr>
						<td>{{ $ticket->name }}</td>
						<td>{{ \DCODESupport\Services\DisplayService::displayUser($ticket->assignedUser) }}</td>
						<td>{{ \DCODESupport\Services\DisplayService::displayDate($ticket->date_due) }}</td>
						<td>{!! \Html::statusSelect($ticket) !!}</td>

						@if (\Auth::user()->admin == '1' || \Auth::id() == $ticket->user_id)
							<td class="alignCenter"><a href="{{ route('admin.ticket.edit', $ticket->id) }}"><span class="icon-edit"></span></a></td>
							<td class="alignCenter">{!! \Html::delete('admin.ticket.destroy', $ticket->id, '<span class="icon-delete"></span>') !!}</td>
						@else
							<td colspan="2"> &nbsp; </td>
						@endif
					</div>
				@endforeach
			</tbody>
		</table>
	</div>
@endsection