@extends('layouts.admin')

@section('controlPanelHeader')
	<a class="buttonAction edit" data-href="{{ route('admin.project.create') }}"><span>+</span></a>
	<h3>Projects</h3>
@endsection

@section('controlPanelBody')
	{!! Form::open(['route' => 'admin.project', 'method' => 'GET', 'id' => 'searchForm']) !!}
	{!! Form::hidden('export', '0') !!}
	{!! Form::hidden('sort', (\Request::get('sort') !== null ? \Request::get('sort') : 'created_at')) !!}
	{!! Form::hidden('sortDir', (\Request::get('sortDir') == 'asc' ? 'asc' : 'desc')) !!}

	<!-- hr / -->

	<div class="options">
		{!! Form::label('sort', 'Sort By') !!}
		{!! Form::select('sort', ['code' => 'Code', 'name' => 'Name', 'client' => 'Client', 'created_at' => 'Created At'], (\Request::get('sort') !== null ? \Request::get('sort') : 'created_at')) !!}
	</div>
	<hr />


	<div class="options">
		{!! Form::label('client_id', 'Client') !!}
		{!! Form::clientSelect('client_id', \Request::get('client_id')) !!}
	</div>
	<hr />

	<div class="search">
		{!! Form::text('code', \Request::get('code'), ['placeholder' => 'Project Code']) !!}
	</div>

	<div class="search">
		{!! Form::text('name', \Request::get('name'), ['placeholder' => 'Project Name']) !!}
	</div>
	<div>
		{!! Form::checkbox('active', '0', \Request::get('active')) !!}
		{!! Form::label('active', 'Show Archived Projects') !!}
	</div>
	<br/>
	<div>
		{!! Form::submit('Search', ['class' => 'buttonAction']) !!}
		<a class="buttonClear" href="{{ route('client.project') }}">Clear</a>
	</div>
	{!! Form::close() !!}
@endsection

@section('content')
	<div class="cards">
		@if ($projects->count() > 0)

			@foreach ($projects as $project)
				<div class="cardContainer">
					<div class="card">
						<div>
							<header>
								@if (\Auth::user()->admin == '1')
									<menu>
										<a class="edit" data-href="{{ route('admin.project.edit', $project->id) }}"><span class="dicon-edit"></span></a>
										{!! \Html::delete('admin.project.destroy', $project->id, '<span class="dicon-delete"></span>') !!}<br />
										<span class="fa fa-clock-o"></span> {{ number_format($project->outstandingTimesheetHours(), 2) }}
										<a href="{{ route('admin.project.access', $project->id) }}"><span class="fa fa-users"></span></a>
									</menu>
								@endif

								<a href="{{ route('admin.project.show', $project->id) }}"><h3>{{ $project->name }} {!! ($project->active == '1' ? '<span class="badge success">Y</span>' : '') !!}</h3></a>
								<span>{{ $project->code }}</span>
							</header>
							<div class="cardBody">
								<div class="cardBody__item">
									<label>Client</label>
									@if($project->client == null)
										NA
									@endif
									{{ DisplayService::displayClient($project->client) }}
								</div>

								<div class="cardBody__item">
									<label>Name</label>
									@if($project->name == null)
										NA
									@endif
									{{ $project->name }}
								</div>

								<div class="cardBody__item">
									<label>Created</label>
									{{ date("d/m/Y", strtotime($project->created_at)) }}
								</div>

								<div class="cardBody__item">
									<label>Added By</label>
									@if($project->user == null)
										NA
									@endif
									{{ DisplayService::displayUser($project->user) }}
								</div>

								<div class="cardBody__item">
									<label>Next Milestone</label>
									@if ($project->nextMilestone() != null)
										{{ $project->nextMilestone()->name }}
										<span class="label small {{ ($project->nextMilestone()->scheduled < time() ? 'alert' : 'success') }}">{{ DisplayService::displayDate($project->nextMilestone()->scheduled) }}</span>
									@else
										NA
									@endif
								</div>

								<div class="cardBody__item">
									<label>Status</label>
									@if ($project->latestUpdate() != null)
										<span class="{{ $project->latestUpdate()->projectStatus->class }}">{{ DisplayService::displayProjectStatus($project->latestUpdate()->projectStatus) }}</span><br />
										@if ($project->latestUpdate()->comments()->count() > 0)
											<small>{{ DisplayService::displayExcerpt($project->latestUpdate()->comments()->orderBy('created_at', 'asc')->first()->comment) }}</small>
										@endif
									@else
										NA
									@endif
								</div>
							</div>
						</div>
						<footer>
							<a class="button round" href="{{ route('admin.project-milestone', ['project_id' => $project->id]) }}">Milestones <span class="badge alert" id="milestoneCount">{{ $project->activeMilestones()->count() }}</span> </a>
							<a class="button round" href="{{ route('admin.project-update', $project->id) }}">Updates</a>
							<a class="button round" href="{{ route('admin.ticket', ['project_id' => $project->id]) }}" aria-describedby="ticketCount">Tickets <span class="badge alert" id="ticketCount">{{ $project->activeTickets()->count() }}</span></a>
						</footer>
					</div>
				</div>
			@endforeach

			<div class="cards__pagination">
				{!! $projects->appends(\Request::except('page'))->render() !!}
			</div>
		@else
			<div class="cards__empty">
				<h3>No projects</h3>
				<p>No projects ...</p>
			</div>
		@endif
	</div>
@endsection