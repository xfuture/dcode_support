@extends('layouts.modal')

@section('header')
    <h1>Add / Edit Project</h1>
@stop

@section('content')
    <div class="warnMessage"></div>
    <div class="main form formModal">
        @if (isset($project))
            {!! Form::model($project, ['route' => ['admin.project.update', $project->id], 'method' => 'put', 'id'=>'updateProjectForm']) !!}
        @else
            {!! Form::open(['route' => 'admin.project.store', 'id'=>'storeProjectForm']) !!}
        @endif

        <fieldset>
            <div>
                {!! Form::label('client_id', 'Client:') !!}
                @if (isset($client))
                    {!! Form::clientSelect('client_id', $client->id, true) !!}
                @else
                    {!! Form::clientSelect('client_id', (isset($project) ? $project->client_id : null)) !!}
                @endif
            </div>

            <div>
                {!! Form::label('code', 'Project code:') !!}
                {!! Form::text('code') !!}
            </div>

            <div>
                {!! Form::label('name', 'Project name:') !!}
                {!! Form::text('name') !!}
            </div>
            <div>
                {!! Form::label('bugsnag_id', 'Project Bugsnag URL:') !!}
                {!! Form::text('bugsnag_id') !!}
            </div>

            <div>
                {!! Form::label('internal_status', 'Internal Status') !!}
                {!! Form::projectInternalStatusSelect('internal_status', (isset($project) ? $project->internal_Status : null)) !!}
            </div>

            <div>
                {!! Form::hidden('active', '0') !!}
                {!! Form::checkbox('active') !!}
                {!! Form::label('active', 'Active') !!}
            </div>
        </fieldset>

        <div class="formAction">
            {!! Form::submit('Save', ['class' => 'buttonAction']) !!}
            <a class="buttonClear" href="{{ URL::previous() }}">Cancel</a>
        </div>

        {!! Form::close() !!}
    </div>
@stop

@section('javascript')
    <script type="application/javascript">
        @if (isset($project))
            bindErrorAjaxModal('#editModal #updateProjectForm', '{{ route('admin.project.update', ['id'=>$project->id]) }}', 'POST');
        @else
            bindErrorAjaxModal('#editModal #storeProjectForm', '{{ route('admin.project.store') }}', 'POST');
        //Generate project name when client is selected
        generateProjectCode('#editModal #storeProjectForm', '#client_id', '#code', '{{ route('admin.project.projectcode') }}', 'POST');
        @endif
        //TO do generate the bugsnag_url by grabing the project name
    </script>
@stop