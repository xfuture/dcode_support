@extends('layouts.admin')

@section('controlPanelHeader')
	<h3>{{$project->name}}</h3>
@endsection

@section('controlPanelBody')
	<div class="controlPanel__content white">
		<div>
			<label>Client</label>
			{{ \DCODESupport\Services\DisplayService::displayClient($project->client) }}
		</div>

		<hr/>

		<div>
			<label>Start</label>
			{{ \DCODESupport\Services\DisplayService::displayDate(strtotime($project->created_at)) }}
		</div>

		<hr/>

		<div>
			<label>User</label>
			{{ \DCODESupport\Services\DisplayService::displayUser($project->user) }}
		</div>

		<hr/>

		<div>
			<label>Status</label>
			@if ($project->latestUpdate() != null)
				<span class="{{ $project->latestUpdate()->projectStatus->class }}">{{ DisplayService::displayProjectStatus($project->latestUpdate()->projectStatus) }}</span>
				<br/>
				@if ($project->latestUpdate()->comments()->count() > 0)
					<small>{{ DisplayService::displayExcerpt($project->latestUpdate()->comments()->orderBy('created_at', 'asc')->first()->comment) }}</small>
				@endif
			@else
				NA
			@endif
		</div>
	</div>
@endsection

@section('content')
	<div class="main form">

		{!! Form::open(['route' => ['admin.project.user', $project->id]]) !!}
			<div>
				{!! Form::userSelect('userId') !!}
			</div>
			<div>
				{!! Form::submit('Add User') !!}
			</div>
		{!! Form::close() !!}

		<fieldset>
			<legend>Internal Users:</legend>

			@foreach ($internalUsers as $user)
				@include('_partials.user.access', ['user' => $user])
			@endforeach
		</fieldset>

		<fieldset>
			<legend>Client Users:</legend>

			@if (count($clientUsers) > 0)
				@foreach ($clientUsers as $user)
					@include('_partials.user.access', ['user' => $user])
				@endforeach
			@endif
		</fieldset>

		<fieldset>
			<legend>Current Project Users:</legend>

			@foreach ($projectUsers->get() as $projectUser)
				@include('_partials.user.access', ['user' => $projectUser->accessUser, 'projectUser' => $projectUser])
			@endforeach
		</fieldset>

	</div>
@stop

@section('javascript')

@endsection