@extends('layouts.admin')

@section('controlPanelHeader')
    <a class="buttonAction edit" data-href="{{ route('admin.user.edit', $user->id) }}"><span>+</span></a>
    <h3>{{ \DCODESupport\Services\DisplayService::displayUser($user) }}</h3>
@endsection

@section('controlPanelBody')
    <div class="controlPanel__content white">
        <div>
            <label>Company</label>
            {{ DisplayService::displayClient($user->client()->first()) }}
        </div>
        <hr/>

        <div>
            <label>Position</label>
            {{ $user->position }}
        </div>
        <hr/>

        <div>
            <label>Location</label>
            {{ $user->location }}
        </div>
        <hr/>

        <div>
            <label>Phone</label>
            {{ $user->phone }}
        </div>
        <hr/>

        <div>
            <label>Permission</label>
            {{ empty($user->internal) ? '' : 'Internal,' }}
            {{ empty($user->access) ? '' : 'Access,' }}
            {{ empty($user->manager) ? '' : 'Manager,' }}
            {{ empty($user->admin) ? '' : 'Admin,' }}
        </div>
    </div>
@endsection

@section('controlPanelHeaderSub-1')
    <a class="buttonAction edit" href="{{ route('admin.note.create', $user->id) }}"><span>+</span></a>
    <h3>Notes</h3>
@endsection

@section('controlPanelBodySub-1')
    {!! Form::open(['route' => ['admin.note', $user->id], 'method' => 'GET', 'id' => 'searchForm']) !!}
    {!! Form::hidden('export', '0') !!}
    {!! Form::hidden('sort', (\Request::get('sort') !== null ? \Request::get('sort') : 'created_at')) !!}
    {!! Form::hidden('sortDir', (\Request::get('sortDir') == 'asc' ? 'asc' : 'desc')) !!}

    <div class="options">
        {!! Form::projectSelect('project_id', \Request::get('project_id')) !!}
    </div>
    <hr/>

    <div class="options">
        {!! Form::noteTypeSelect('note_type_id', \Request::get('note_type_id')) !!}
    </div>
    <hr/>

    <div class="search">
        {!! Form::text('message', \Request::get('message'), ['placeholder' => 'Message']) !!}
    </div>
    <br/>
    <div>
        {!! Form::submit('Search', ['class' => 'buttonAction']) !!}
        <a class="buttonClear" href="{{ route('admin.user') }}">Clear</a>
    </div>
    {!! Form::close() !!}
@endsection

@section('content')
    @php
            @endphp
    <div style="width: 100%">
        <div>
            <ul class="tabs" data-tabs id="user-tabs">
                <li class="tabs-title is-active">
                    <a aria-selected="true" href="#panel1">Notes</a>
                </li>
                <li class="tabs-title">
                    <a href="#panel2">Projects</a>
                </li>
            </ul>
        </div>

        <div class="tabs-content" data-tabs-content="user-tabs">
            <div class="tabs-panel is-active" id="panel1">
                <div class="admin__table">
                    @if ($notes->count() > 0)
                        <table>
                            <thead>
                            <?php
                            $columns = array(
                                array(
                                    'id' => 'created_at',
                                    'value' => 'Created',
                                    'sort' => true,
                                    'width' => '200'
                                ),
                                array(
                                    'id' => 'project',
                                    'value' => 'Project',
                                    'sort' => true,
                                    'width' => '200'
                                ), array(
                                    'id' => 'ticket',
                                    'value' => 'Ticket',
                                    'sort' => false,
                                    'width' => '200'
                                ), array(
                                    'id' => 'note_type',
                                    'value' => 'Note Type',
                                    'sort' => true,
                                    'width' => '200'
                                ),
                                array(
                                    'id' => 'message',
                                    'value' => 'Message',
                                    'sort' => false,
                                    'width' => '400'
                                )
                            );
                            ?>

                            @include('_partials.layout.tableHeader', ['columns' => $columns, 'action' => 2])
                            </thead>

                            <tbody>
                            @foreach ($notes as $note)
                                @php
                                    $project = \DCODESupport\Models\Project\Project::find($note->project_id);
                                    $ticket = \DCODESupport\Models\Ticket\Ticket::find($note->ticket_id);
                                    $noteType = \DCODESupport\Models\Note\NoteType::find($note->note_type_id);
                                @endphp
                                <tr>
                                    <td>{{ date("d/m/Y H:i", strtotime($note->created_at)) }}</td>
                                    @if(!empty($project))
                                        <td>
                                            <a href="{{ route('admin.project.show', $project->id) }}">{{ $project->name }}</a>
                                        </td>
                                    @else
                                        <td>N/A</td>
                                    @endif

                                    @if(!empty($ticket))
                                        <td>
                                            <a href="{{ route('admin.ticket.show', $ticket->id) }}">{{ $ticket->name }}</a>
                                        </td>
                                    @else
                                        <td>N/A</td>
                                    @endif

                                    @if(!empty($noteType))
                                        <td>{{ $noteType->name }}</td>
                                    @else
                                        <td>N/A</td>
                                    @endif

                                    <td>{{ $note->message }}</td>

                                    <td class="alignCenter"><a class="edit"
                                                               href="{{ route('admin.note.edit', $note->id) }}"><span
                                                    class="dicon-edit"></span></a></td>
                                    <td class="alignCenter">{!! \Html::delete('admin.note.destroy', $note->id, '<span class="dicon-delete"></span>') !!}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>

                        <div class="admin__table--pagination">
                            {!! $notes->appends(\Request::except('page'))->render() !!}
                        </div>
                    @else
                        <div class="admin__table--empty">
                            <h3>No notes</h3>
                            <p>No notes ...</p>
                        </div>
                    @endif
                </div>
            </div>

            <div class="tabs-panel" id="panel2" style="background: #f0f0f0">
                <div class="cards">
                    @if ($user->projectUsers->count() > 0)

                        @foreach ($user->projectUsers as $projectUser)
                            @php
                                $projects[] =  $project = $projectUser->project;
                            @endphp
                            @if(!empty($project))
                                <div class="cardContainer">
                                    <div class="card">
                                        <div>
                                            <header>
                                                @if (\Auth::user()->admin == '1')
                                                    <menu>
                                                        <a class="edit"
                                                           data-href="{{ route('admin.project.edit', $project->id) }}"><span
                                                                    class="dicon-edit"></span></a>
                                                        {!! \Html::delete('admin.project.destroy', $project->id, '<span class="dicon-delete"></span>') !!}
                                                        <br/>
                                                        <span class="fa fa-clock-o"></span> {{ number_format($project->outstandingTimesheetHours(), 2) }}
                                                        <a href="{{ route('admin.project.access', $project->id) }}"><span
                                                                    class="fa fa-users"></span></a>
                                                    </menu>
                                                @endif

                                                <a href="{{ route('admin.project.show', $project->id) }}">
                                                    <h3>{{ $project->name }} {!! ($project->active == '1' ? '<span class="badge success">Y</span>' : '') !!}</h3>
                                                </a>
                                                <span>{{ $project->code }}</span>
                                            </header>
                                            <div class="cardBody">
                                                <div class="cardBody__item">
                                                    <label>Client</label>
                                                    @if($project->client == null)
                                                        NA
                                                    @endif
                                                    {{ DisplayService::displayClient($project->client) }}
                                                </div>

                                                <div class="cardBody__item">
                                                    <label>Name</label>
                                                    @if($project->name == null)
                                                        NA
                                                    @endif
                                                    {{ $project->name }}
                                                </div>

                                                <div class="cardBody__item">
                                                    <label>Created</label>
                                                    {{ date("d/m/Y", strtotime($project->created_at)) }}
                                                </div>

                                                <div class="cardBody__item">
                                                    <label>Added By</label>
                                                    @if($project->user == null)
                                                        NA
                                                    @endif
                                                    {{ DisplayService::displayUser($project->user) }}
                                                </div>

                                                <div class="cardBody__item">
                                                    <label>Next Milestone</label>
                                                    @if ($project->nextMilestone() != null)
                                                        {{ $project->nextMilestone()->name }}
                                                        <span class="label small {{ ($project->nextMilestone()->scheduled < time() ? 'alert' : 'success') }}">{{ DisplayService::displayDate($project->nextMilestone()->scheduled) }}</span>
                                                    @else
                                                        NA
                                                    @endif
                                                </div>

                                                <div class="cardBody__item">
                                                    <label>Status</label>
                                                    @if ($project->latestUpdate() != null)
                                                        <span class="{{ $project->latestUpdate()->projectStatus->class }}">{{ DisplayService::displayProjectStatus($project->latestUpdate()->projectStatus) }}</span>
                                                        <br/>
                                                        @if ($project->latestUpdate()->comments()->count() > 0)
                                                            <small>{{ DisplayService::displayExcerpt($project->latestUpdate()->comments()->orderBy('created_at', 'asc')->first()->comment) }}</small>
                                                        @endif
                                                    @else
                                                        NA
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                        <footer>
                                            <a class="button round"
                                               href="{{ route('admin.project-milestone', $project->id) }}">Milestones</a>
                                            <a class="button round"
                                               href="{{ route('admin.project-update', $project->id) }}">Updates</a>
                                            <a class="button round"
                                               href="{{ route('admin.ticket', ['project_id' => $project->id]) }}"
                                               aria-describedby="ticketCount">Tickets <span class="badge alert"
                                                                                            id="ticketCount">{{ $project->activeTickets()->count() }}</span></a>
                                        </footer>
                                    </div>
                                </div>
                            @endif
                        @endforeach
                    @else
                        <div class="cards__empty">
                            <h3>No projects</h3>
                            <p>No projects ...</p>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>


@endsection