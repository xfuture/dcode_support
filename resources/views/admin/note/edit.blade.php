@extends('layouts.modal')

@section('header')
    <h1>Add/Edit Note</h1>
@stop

@section('content')
    <div class="warnMessage"></div>
    <div class="main form formModal">
        @if (isset($note))
            {!! Form::model($note, ['route' => ['admin.note.update', $note->id], 'method' => 'post', 'id' => 'updateNoteForm']) !!}
        @else
            {!! Form::open(['route' => ['admin.note.store', $user->id], 'id' => 'storeNoteForm']) !!}
        @endif

        <fieldset>
            <legend>Note</legend>
            <div>
                {!! Form::label('project_id', 'Project:') !!}
                {!! Form::projectSelect('project_id') !!}
            </div>

            <div>
                {!! Form::label('project_id', 'Note Type:') !!}
                {!! Form::noteTypeSelect('note_type_id') !!}
            </div>

            <div>
                {!! Form::label('message', 'Message:') !!}
                {!! Form::textarea('message') !!}
            </div>
        </fieldset>

        <fieldset>
            <legend style="width: 100%;">
                Related Task
                <a id="addTask">
                    <span style="color: #1baad2" id="addTaskButton">Add</span>
                </a>
            </legend>
            <div id="taskContent" style="display: none">
                {!! Form::hidden('add_task', 0) !!}

                <hr style="margin: 0 0 10px 0;"/>
                @php
                    if(isset($note)){
                        $ticket = \DCODESupport\Models\Ticket\Ticket::find($note->ticket_id);
                    }
                @endphp

                <div>
                    {!! Form::label('client_id', 'Client:') !!}

                    {!! Form::clientSelect('client_id', (isset($ticket) ? $note->user->client_id : null)) !!}
                </div>

                <div>
                    {!! Form::label('name', 'Name:') !!}
                    {!! Form::text('name', empty($ticket) ? null : $ticket->name) !!}
                </div>

                <div>
                    {!! Form::label('comment', 'Comment:') !!}
                    {!! Form::textarea('comment', empty($ticket) ? null : $ticket->comment) !!}
                </div>

                <div>
                    {!! Form::label('estimated_time', 'Estimated Time:') !!}
                    {!! Form::number('estimated_time', empty($ticket) ? null : $ticket->estimated_time,['class' => 'form-control','step'=>'any']) !!}
                </div>

                <div>
                    {!! Form::label('date_due', 'Date Due:') !!}
                    {!! Form::datePicker('date_due', empty($ticket) ? null : $ticket->date_due) !!}
                </div>

                <div>
                    {!! Form::label('assigned_user_id', 'Assigned User:') !!}
                    {!! Form::assignedUserSelect('assigned_user_id', (empty($ticket) ? null : $ticket->assigned_user_id), (empty($ticket) ? null : $ticket->project)) !!}
                </div>

                <div>
                    {!! Form::hidden('internal', '0') !!}
                    {!! Form::checkbox('internal', '1') !!}
                    {!! Form::label('internal', 'Internal to DCODE') !!}
                </div>
                <hr style="margin: 0;"/>
            </div>
        </fieldset>

        <div class="formAction">
            {!! Form::submit('Save', ['class' => 'buttonAction']) !!}
            <a class="buttonClear" href="{{ URL::previous() }}">Cancel</a>
        </div>

        {!! Form::close() !!}
    </div>
@stop

@section('javascript')
    <script type="application/javascript">
        @if (isset($note))
            bindErrorAjaxModal('#editModal #updateNoteForm', '{{ route('admin.note.update', ['id'=>$note->id]) }}', 'POST');
        @else
            bindErrorAjaxModal('#editModal #storeNoteForm', '{{ route('admin.note.store', $user->id) }}', 'POST');
        @endif
        //Bind add task event
        $('#taskContent :input').attr("disabled", true); //Disable all hidden input of related task

        $('#addTask').on('click', function (e) {
            if ($('#taskContent').is(':visible')) {
                $('#taskContent').hide();
                $('#taskContent :input').attr("disabled", true);
                $('#taskContent :input[name=add_task]').val(0);
                $('#addTaskButton').html('Add');
            } else {
                $('#taskContent').show();
                $('#taskContent :input').attr("disabled", false);
                $('#taskContent :input[name=add_task]').val(1);
                $('#addTaskButton').html('Remove');
            }
            e.preventDefault();
        });

        @if (isset($note) && !empty($note->ticket_id))
            $('#addTask').trigger('click');
        @endif
    </script>
@stop