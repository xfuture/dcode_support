@extends('layouts.admin')

@section('controlPanelHeader')
    <a class="buttonAction edit" data-href="{{ route('admin.project.edit', $project->id) }}"><span>+</span></a>
    <h3>{{ $project->name }}</h3>
@endsection

@section('controlPanelBody')
    <div class="controlPanel__content white">
        <div>
            <label>Client</label>
            {{ \DCODESupport\Services\DisplayService::displayClient($project->client) }}
        </div>

        <hr/>

        <div>
            <label>Start</label>
            {{ \DCODESupport\Services\DisplayService::displayDate(strtotime($project->created_at)) }}
        </div>

        <hr/>

        <div>
            <label>User</label>
            {{ \DCODESupport\Services\DisplayService::displayUser($project->user) }}
        </div>

        <hr/>

        <div>
            <label>Status</label>
            @if ($project->latestUpdate() != null)
                <span class="{{ $project->latestUpdate()->projectStatus->class }}">{{ DisplayService::displayProjectStatus($project->latestUpdate()->projectStatus) }}</span>
                <br/>
                @if ($project->latestUpdate()->comments()->count() > 0)
                    <small>{{ DisplayService::displayExcerpt($project->latestUpdate()->comments()->orderBy('created_at', 'asc')->first()->comment) }}</small>
                @endif
            @else
                NA
            @endif
        </div>
    </div>
@endsection

@section('controlPanelHeaderSub-1')
    <a class="buttonAction edit" data-href="{{ route('admin.project-update.add', $project->id) }}"><span>+</span></a>
    <h3>Updates</h3>
@endsection

@section('controlPanelBodySub-1')
    {!! Form::open(['route' => ['admin.project-milestone', $project->id], 'method' => 'GET', 'id' => 'searchForm']) !!}
    {!! Form::hidden('export', '0') !!}
    {!! Form::hidden('sort', (\Request::get('sort') !== null ? \Request::get('sort') : 'created_at')) !!}
    {!! Form::hidden('sortDir', (\Request::get('sortDir') == 'asc' ? 'asc' : 'desc')) !!}

    <div>
        <div class="search">
            {!! Form::text('name', \Request::get('name'), ['placeholder' => 'Name']) !!}
        </div>
        <br/>

        <div>
            {!! Form::submit('Search', ['class' => 'buttonAction']) !!}
            {{--{!! Form::submit('Export', ['class' => 'buttonAction']) !!}--}}
            <a class="buttonClear" href="{{ route('client.project') }}">Clear</a>
        </div>
    </div>

    {!! Form::close() !!}
@endsection

@section('content')
    <div class="admin__table">
        @if ($projectUpdates->count() > 0)
            <table>
                <thead>
                <?php
                $columns = array(
                    array(
                        'id' => 'project_status',
                        'value' => 'Status',
                        'sort' => true,
                        'width' => '100'
                    ),
                    array(
                        'id' => 'comment',
                        'value' => 'Comment',
                        'sort' => false,
                        'width' => '150'
                    ),
                    array(
                        'id' => 'user',
                        'value' => 'Added By',
                        'sort' => false,
                        'width' => '150'
                    ),
                    array(
                        'id' => 'created_at',
                        'value' => 'Created',
                        'sort' => true,
                        'width' => '75'
                    )
                );
                ?>

                @include('_partials.layout.tableHeader', ['columns' => $columns, 'action' => 3])
                </thead>

                <tbody>
                @foreach ($projectUpdates as $projectUpdate)
                    <tr>
                        <td width="20"><a class="edit"
                                          href="{{ route('admin.project-update.show', $projectUpdate->id) }}">{{ DisplayService::displayProjectStatus($projectUpdate->projectStatus) }}</a>
                        </td>
                        <td width="20">{{ $projectUpdate->comment }}</td>
                        <td width="20">{{ DisplayService::displayUser($projectUpdate->user) }}</td>
                        <td width="20">{{ date("d/m/Y", strtotime($projectUpdate->created_at)) }}</td>

                        {{--						<td width="10" class="alignCenter"><a href="{{ route('admin.project-update.show', $projectUpdate->id) }}"><span class="fa fa-eye"></span></a></td>--}}
                        @if (\Auth::user()->admin == '1')
                            <td class="alignCenter" width="10"><a class="edit"
                                                                  href="{{ route('admin.project-update.edit', $projectUpdate->id) }}"><span
                                            class="dicon-edit"></span></a></td>
                            <td class="alignCenter"
                                width="10">{!! \Html::delete('admin.project-update.destroy', $projectUpdate->id, '<span class="dicon-delete"></span>') !!}</td>
                        @else
                            <td colspan="2"> &nbsp; </td>
                        @endif
                    </tr>
                @endforeach
                </tbody>
            </table>

            <div class="admin__table--pagination">
                {!! $projectUpdates->appends(\Request::except('page'))->render() !!}
            </div>
        @else
            <div class="admin__table--empty">
                <h3>No project updates</h3>
                <p>No project updates ...</p>
            </div>
        @endif
    </div>
@endsection