@extends('layouts.modal')

@section('header')
	@include('_partials.project.header', ['project' => $project])
@stop

@section('content')
	<div class="warnMessage"></div>
	<div class="main form formModal">
		@if (isset($projectUpdate))
			{!! Form::model($projectUpdate, ['route' => ['admin.project-update.update', $projectUpdate->id], 'method' => 'put', 'id' => 'updateProjectPForm']) !!}
		@else
			{!! Form::open(['route' => ['admin.project-update.store', $project->id], 'id' => 'saveProjectPForm']) !!}
		@endif

		<fieldset>
			<legend>Add/Edit Status</legend>

			<div>
				{!! Form::label('project_status_id', 'Project Status:') !!}
				{!! Form::projectStatusSelect('project_status_id', (isset($projectUpdate) ? $projectUpdate->project_status_is : null)) !!}
			</div>

			<div>
				{!! Form::label('comment', 'Comment:') !!}
				{!! Form::textarea('comment') !!}
			</div>
		</fieldset>

		<div class="formAction">
			{!! Form::submit('Save', ['class' => 'buttonAction']) !!}
			&nbsp;
			<a class="buttonClear" href="{{ URL::previous() }}">Cancel</a>
		</div>

		{!! Form::close() !!}
	</div>
@stop

@section('javascript')
	<script type="application/javascript">
        @if (isset($projectUpdate))
            bindErrorAjaxModal('#editModal #updateProjectPForm', '{{ route('admin.project-update.update', ['id'=>$projectUpdate->id]) }}', 'POST');
        @else
            bindErrorAjaxModal('#editModal #saveProjectPForm', '{{ route('admin.project-update.save', ['id'=>$project->id]) }}', 'POST');
		@endif
	</script>
@stop