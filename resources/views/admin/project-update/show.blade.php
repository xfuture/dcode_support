@extends('layouts.modal')

@section('header')
	@include('_partials.project.header', ['project' => $project])
@stop

@section('content')
	<article>
		<div>
			<fieldset>
				<legend>Details</legend>

				<div>
					<label>Current Status</label>
					{{ DisplayService::displayProjectStatus($projectUpdate->projectStatus) }}
				</div>

				<div>
					<label>Created At</label>
					{{ $projectUpdate->created_at }}
				</div>
			</fieldset>

            <fieldset>
                <legend>People</legend>

                <div>
                    <label>Reported By</label>
                    {{ DisplayService::displayUser($projectUpdate->user) }}
                </div>
            </fieldset>

			<fieldset>
				<legend>Comment</legend>

				<div>
					{{ $projectUpdate->comment }}
				</div>
			</fieldset>
        </div>

		<div>
			<h3>Comments:</h3>

			@foreach ($projectUpdate->comments as $comment)
				@include('_partials.comment.show', ['comment' => $comment])
			@endforeach

			{!! Form::open(['route' => ['admin.project-update.comment', $projectUpdate->id]]) !!}
			@include('_partials.comment.edit')
			{!! Form::close() !!}
		</div>
	</article>


@stop