@extends('layouts.modal')

@section('header')
	<h1>Add/Edit Timesheet</h1>
@stop

@section('content')
	<div class="warnMessage"></div>
	<div class="main form formModal">
		@if (isset($timesheet))
			{!! Form::model($timesheet, ['route' => ['admin.timesheet.update', $timesheet->id], 'method' => 'put', 'id'=>'updateTimesheetForm']) !!}
		@else
			{!! Form::open(['route' => 'admin.timesheet.store', 'id'=>'storeTimesheetForm']) !!}
		@endif

		{!! Form::hidden('ticket_id', (isset($ticket) && $ticket != null ? $ticket->id : null)) !!}

		<fieldset>
			<legend>Add/Edit Timesheet</legend>

			<div>
				{!! Form::label('project_id', 'Project:') !!}
				{!! Form::projectSelect('project_id', (isset($timesheet) ? $timesheet->project_id : null)) !!}
			</div>

			<div>
				{!! Form::label('comment', 'Comment:') !!}
				{!! Form::textArea('comment') !!}
			</div>

			<div class="row">
				<div class="large-6 columns">
					{!! Form::label('start', 'Start:') !!}
					{!! Form::text('start', (isset($timesheet) ? date("d-m-Y H:i:s", $timesheet->start) : date("d-m-Y H:i:s")), array('class'=>'datetimepicker', 'readonly', 'id'=>'start_datetime')) !!}
				</div>

				<div class="large-6 columns">
					{!! Form::label('stop', 'Stop:') !!}
					{!! Form::text('stop', (isset($timesheet) ? ($timesheet->stop != null ? date("d-m-Y H:i:s", $timesheet->stop) : date("d-m-Y H:i:s")) : null), array('class'=>'datetimepicker','readonly', 'id'=>'stop_datetime')) !!}
				</div>
			</div>

			<div>
				{!! Form::hidden('billed', 0) !!}
				{!! Form::checkbox('billed', 1) !!}
				{!! Form::label('billed', 'Billed') !!}
			</div>
		</fieldset>

		<div class="formAction">
			{!! Form::submit('Save', ['class' => 'buttonAction']) !!}
			<a class="buttonClear" href="{{ URL::previous() }}">Cancel</a>
		</div>

		{!! Form::close() !!}
	</div>
@stop

@section('javascript')
	<script type="application/javascript">
        @if (isset($timesheet))
            bindErrorAjaxModal('#editModal #updateTimesheetForm', '{{ route('admin.timesheet.update', ['id'=>$timesheet->id]) }}', 'POST');
        @else
            bindErrorAjaxModal('#editModal #storeTimesheetForm', '{{ route('admin.timesheet.store') }}', 'POST');
		@endif
	</script>
@stop