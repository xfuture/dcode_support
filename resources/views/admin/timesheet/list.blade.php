@extends('layouts.admin')

@section('controlPanelHeader')
	<a class="buttonAction edit" data-href="{{ route('admin.timesheet.create') }}"><span>+</span></a>
	<h3>Timesheets</h3>
@endsection

@section('controlPanelBody')
	{!! Form::open(['route' => 'admin.timesheet', 'method' => 'GET', 'id' => 'searchForm']) !!}
	{!! Form::hidden('export', '0') !!}
	{!! Form::hidden('sort', (\Request::get('sort') !== null ? \Request::get('sort') : 'created_at')) !!}
	{!! Form::hidden('sortDir', (\Request::get('sortDir') == 'asc' ? 'asc' : 'desc')) !!}

	<!-- hr / -->
	{{--<div class="options">--}}
		{{--{!! Form::label('sort', 'Sort By') !!}--}}
		{{--{!! Form::select('sort', ['code' => 'Code', 'name' => 'Name', 'client' => 'Client', 'created_at' => 'Created At'], (\Request::get('sort') !== null ? \Request::get('sort') : 'created_at')) !!}--}}
	{{--</div>--}}
	{{--<hr />--}}

	<div class="options">
		{!! Form::label('project_id', 'Project') !!}
		{!! Form::projectSelect('project_id', \Request::get('project_id')) !!}
	</div>
	<hr/>

	<div class="options">
		{!! Form::label('user_id', 'User') !!}
		{!! Form::userSelect('user_id', \Request::get('user_id')) !!}
	</div>
	<hr/>

	<div class="options">
		{!! Form::label('billed', 'Bill Status') !!}
		{!! Form::billStatusPicker('billed', \Request::get('billed')) !!}
	</div>
	<hr/>

	<br />

	<div>
		{!! Form::submit('Search', ['class' => 'buttonAction']) !!}
		<a class="buttonClear" href="{{ route('admin.project') }}">Clear</a>
	</div>
	{!! Form::close() !!}
@endsection

@section('content')

	<div class="admin__table">
		<div>
			<span style="font-weight: bold">Total hours: </span>
			<span style="padding: 0 50px 10px 0">{{ $totalHours }}</span>

			<span style="font-weight: bold">Total billed hours: </span>
			<span>{{ $billedHours }}</span>
		</div>

		@if ($timesheets->count() > 0)
			<table>
				<thead>
					<?php
						$columns = array(
							array(
									'id'    => 'project',
									'value' => 'Project',
									'sort'  => true,
									'width' => '400'
							),
							array(
									'id'    => 'start',
									'value' => 'Start',
									'sort'  => true,
									'width' => '200'
							),
							array(
									'id'    => 'stop',
									'value' => 'Stop',
									'sort'  => true,
									'width' => '200'
							),
							array(
									'id'    => 'hours',
									'value' => 'Hours',
									'sort'  => true,
									'width' => '70'
							),
							array(
									'id'    => 'billed',
									'value' => 'Billed',
									'sort'  => true,
									'width' => '70'
							),
							array(
									'id'    => 'user',
									'value' => 'Added By',
									'sort'  => true,
									'width' => '150'
							),
							array(
									'id'    => 'created_at',
									'value' => 'Created',
									'sort'  => true,
									'width' => '100'
							)
						);
					?>

					@include('_partials.layout.tableHeader', ['columns' => $columns, 'action' => 2])
				</thead>

				<tbody>
					@foreach ($timesheets as $timesheet)
						<tr>
							<td>{{ $timesheet->project->code }}: {{ $timesheet->project->name }}</td>
							<td>{{ date("d/m/Y H:i", $timesheet->start) }}</td>
							<td>{{ ($timesheet->stop != null ? date("d/m/Y H:i", $timesheet->stop) : '') }}</td>
							<td>{{ number_format($timesheet->hours, 2) }}</td>
							<td>{{ $timesheet->billed }}</td>
							<td>{{ DisplayService::displayUser($timesheet->user) }}</td>
							<td>{{ date("d/m/Y", strtotime($timesheet->created_at)) }}</td>
							<td class="alignCenter"><a class="edit" href="{{ route('admin.timesheet.edit', $timesheet->id) }}"><span class="dicon-edit"></span></a></td>
							<td class="alignCenter">{!! \Html::delete('admin.timesheet.destroy', $timesheet->id, '<span class="dicon-delete"></span>') !!}</td>
						</tr>
					@endforeach
				</tbody>
			</table>

			<div class="admin__table--pagination">
				{!! $timesheets->appends(\Request::except('page'))->render() !!}
			</div>
		@else
			<div class="admin__table--empty">
				<h3>No timesheets</h3>
				<p>No timesheets ...</p>
			</div>
		@endif
	</div>
@endsection