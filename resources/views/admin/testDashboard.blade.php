@extends('layouts.admin')

@section('content')
	<h1>Dashboard</h1>

	<div class="activityLog">

		@foreach($activeProjects as $project)

			@if ($project->projectMilestones()->count() > 0)
				<div class="activity">
					<div class="activity__icon badge">
						<span class="dicon-messages"></span>
					</div>
					<div class="activity__comment">
						<div  class="activity__comment--meta">
							<label>Project:</label>
							{{ \DCODESupport\Services\DisplayService::displayProject($project) }}
							<div>
								<label>User:</label>
								{{ \DCODESupport\Services\DisplayService::displayUser($project->user) }}
							</div>
							<div> ... </div>
						</div>
						<div>
							<?php
                            	$milestone = [];
							?>
							@foreach ($project->projectMilestones as $projectMilestone)
							{{--	{!! print_r($projectMilestone) !!}	--}}
								<?php
								// Convert the time to the format that is used.
								// Resources : https://github.com/taitems/jQuery.Gantt/issues/94
									$fromTime=$projectMilestone->created_at;
									$fromTime=strtotime($fromTime);
									$fromTime=$fromTime*1000;
									$toTime=($projectMilestone->scheduled)*1000;

									$milestoneItem[]=     ["id"=> "{$projectMilestone->id} ",
										"from"=> "/Date($fromTime)/",
										"to"=> "/Date($toTime)/",
										"desc"=> "Created By:{$projectMilestone->user->first_name }<br/>",
										"label"=> " {$projectMilestone->name }",
										"customClass"=> "testClass",
										"dep"=> "{$projectMilestone->project_id}"];

										$milestone[] = [
											"name"=> " {$projectMilestone->name } ",
											//"desc"=>"&rarr; TestDesc"  ,
											"values"=> $milestoneItem
											];
										?>

                                    <?php
                                    $milestoneItem = [];// This is the exact place to clear the milestoneItem array
                                    ?>

								@endforeach
								<a class="buttonAction edit" data-href="{{ route('admin.project-milestone.add', $project->id) }}"><span>+</span></a>
								<div class="ganttData"  data-json='{!! json_encode($milestone) !!}' project-url='{!! json_encode(route('admin.project.show', $projectMilestone->project_id)) !!}' add-milestone='{!! json_encode(route('admin.project-milestone.add', $project->id)) !!}'>NO Active Milestones</div>
						</div>

					</div>
				</div>
			@endif


		@endforeach

	</div>

@endsection


@section('javascript')

	<link rel="stylesheet" href="/css/gantt.css" />
	<script src="js/jquery.fn.gantt.js"></script>
	<script type="application/javascript">
        var list = $(".ganttData").map(function(){return $(this).attr("data-json");}) ;
        console.log('List:',list);
        //console.log(list.size());

/*

        jQuery(function () {
            //console.log('BEFORE calling gantt',input);
			//$(this).attr('data-val')
            $(".ganttData").gantt({
                source:JSON.parse(list[2]),
                navigate: 'scroll',
                scale: 'days',
                maxScale: 'weeks',
                minScale: 'hours',
                onRender: function() {
                    console.log("chart rendered");
                    console.log('ON RENDER',this.source);
                    console.log($(this).attr('data-json'));

                }
            });
        });

*/
        $(".ganttData").each(function() {
            var input=JSON.parse($(this).attr('data-json'));
            var url=JSON.parse($(this).attr('project-url'));
            var addmilestone=JSON.parse($(this).attr('add-milestone'));
            console.log(addmilestone);
            console.log('Test input',input);
            $(this).gantt({
                source:input,
                navigate: 'scroll',
                scale: 'days',
                maxScale: 'weeks',
                minScale: 'hours',
                onItemClick: function(data) {
                    window.location.href = url;
		        },
                onAddClick: function(dt, rowId) {
                    //window.location.href=addmilestone;
    		    },
                onRender: function() {
                    console.log("chart rendered");
                    console.log('ON RENDER',this.source);
                    //console.log($(this).attr('data-json'));
                }
            });
        });

/*

        var list = $(".ganttData").map(function(){return $(this).attr("data-json");}) ;
       console.log('List:',list);
        //console.log(list.size());

        var i;
        for(i=0;i<list.size();i++){
			console.log('Loop:',list[i]);
			var items=list[i];
            jQuery(function () {
                var input=JSON.parse(items);
                //console.log('BEFORE calling gantt',input);
                $(".ganttData").gantt({
                    source:input,
                    navigate: 'scroll',
                    scale: 'days',
                    maxScale: 'weeks',
                    minScale: 'hours',
                    onRender: function() {
                        console.log("chart rendered");
                        console.log('ON RENDER',this.source);
                    }
                });
            });
		}

/*

/*
        $(".ganttData").each(function() {
            var input=JSON.parse($(".ganttData").attr('data-json'));
            console.log('Test input',input);
		});
*/
/*

        $(".ganttData").each(function() {

            jQuery(function () {
                var input=JSON.parse($(".ganttData").attr('data-json'));
                console.log('BEFORE calling gantt',input);
                $(".ganttData").gantt({
                    source:input,
                    navigate: 'scroll',
                    scale: 'days',
                    maxScale: 'weeks',
                    minScale: 'hours',
                    onRender: function() {
                        console.log("chart rendered");
                        console.log('ON RENDER',this.source);
                    }
                });
            });

		});
		*/
	</script>
@stop
