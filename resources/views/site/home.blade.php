@extends('layouts.clean')

@section('content')
	<section class="main">
		<h1>Welcome to DCODE's Support System</h1>

		<menu>
			<a class="buttonAction" href="{{ route('client.dashboard') }}">Client Access</a>
			<a class="buttonAction" href="{{ route('admin.dashboard') }}">DCODE Team Access</a>
		</menu>
	</section>
@endsection