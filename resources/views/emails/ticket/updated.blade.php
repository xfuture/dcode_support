<div style="text-align: center; font-size: 25px; padding: 15px 1px 50px 10px; color: black">{{ ($ticket->project != null ? $ticket->project->name : '') }} {{ $ticket->name }}</div>

<table width="100%">
    <tbody>
    {{-- Blade if else statement --}}
    @if ($ticket->project != null)
        <tr>
            <td>
                <span class="detail">Project:</span>
                <span class="description">{{ DisplayService::displayProjectCode($ticket->project) }} {{ DisplayService::displayProject($ticket->project) }}</span>
            </td>
        </tr>
        <hr/>
    @endif
    <tr>
        <td>
            <span class="detail">Date Due:</span>
            <span class="description">{{ DisplayService::displayDate($ticket->date_due) }}</span>
        </td>
    </tr>
    <hr/>

    <tr>
        <td>
            <span class="detail">Reported By:</span>
            <span class="description">{{ DisplayService::displayuser($ticket->user) }}</span>
        </td>
    </tr>
    <hr/>

    <tr>
        <td>
            <span class="detail">Created At:</span>
            <span class="description">{{ date("d/m/Y", strtotime($ticket->created_at)) }}</span>
        </td>
    </tr>
    <hr/>

    <tr>
        <td>
            <span class="detail">Status:</span>
            <span class="description" style="color: #12AAD3; font-weight: bold">{{ DisplayService::displayTicketStatus($ticket->currentStatus()) }}</span>
        </td>
    </tr>
    <hr/>

    </tbody>
</table>

<br/>
<div style="text-align: center; color: black; padding-top: 10px; font-size: 15px">{!! nl2br($ticket->comment) !!}</div>

<div class="view-ticker">
    @if ($admin == true)
        <a href="{{ route('admin.ticket.show', $ticket->id) }}"><img src="{{ asset('assets/images/view_ticket.jpg') }}"
                                                                     height="30"/></a>
    @else
        <a href="{{ route('client.ticket.show', $ticket->id) }}"><img src="{{ asset('assets/images/view_ticket.jpg') }}"
                                                                      height="30"/></a>
    @endif
</div>
