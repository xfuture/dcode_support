<h1>New Comment - {{ $comment->commentable->project->name }}</h1>

Updated Status: {{ DisplayService::displayProjectStatus($comment->commentable->projectStatus) }}<br />
<br />

{!! nl2br($comment->comment) !!}

<br />
<br />
- {{ DisplayService::displayUser($comment->user) }}, {{ $comment->created_at }}
