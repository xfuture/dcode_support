<div style="text-align: center; font-size: 25px; padding: 15px 1px 50px 10px; color: black">New Comment - {{ $comment->commentable->project->name }} {{ $comment->commentable->name }}</div>

<table width="100%">
	<tbody>
	<tr>
		<td>
			<span class="detail">Project:</span>
			<span class="description">{{ DisplayService::displayProjectCode($comment->commentable->project) }} {{ DisplayService::displayProject($comment->commentable->project) }}</span>
		</td>
	</tr>
	<hr/>

	<tr>
		<td>
			<span class="detail">Date Due:</span>
			<span class="description">{{ DisplayService::displayDate($comment->commentable->date_due) }}</span>
		</td>
	</tr>
	<hr/>

	<tr>
		<td>
			<span class="detail">Reported By:</span>
			<span class="description">{{ DisplayService::displayuser($comment->commentable->user) }}</span>
		</td>
	</tr>
	<hr/>

	<tr>
		<td>
			<span class="detail">Created At:</span>
			<span class="description">{{ date("d/m/Y", strtotime($comment->commentable->created_at)) }}</span>
		</td>
	</tr>
	<hr/>

	<tr>
		<td>
			<span class="detail">Status:</span>
			<span class="description" style="color: #12AAD3; font-weight: bold">{{ DisplayService::displayTicketStatus($comment->commentable->currentStatus()) }}</span>
		</td>
	</tr>
	<hr/>

	</tbody>
</table>

<br />
{{--<div style="text-align: center; color: black; padding-top: 10px; font-size: 15px">{!! nl2br($comment->commentable->comment) !!}</div>--}}
<div style="text-align: center; color: black; padding-top: 10px; font-size: 15px">{!! nl2br($comment->comment) !!}</div>

<div class="view-ticker">
	@if ($admin == true)
		<a href="{{ route('admin.ticket.show', $comment->commentable->id) }}"><img src="{{ asset('assets/images/view_ticket.jpg') }}"
																	 height="30"/></a>
	@else
		<a href="{{ route('client.ticket.show', $comment->commentable->id) }}"><img src="{{ asset('assets/images/view_ticket.jpg') }}"
																	  height="30"/></a>
	@endif
</div>



{{--

<div class="view-ticker">
	@if ($admin == true)
		<a href="{{ route('admin.ticket.show', $comment->commentable->id) }}">View Ticket</a>
	@else
		<a href="{{ route('client.ticket.show', $comment->commentable->id) }}">View Ticket</a>
	@endif
</div>

<br />
<br />
--<br />

{!! nl2br($comment->comment) !!}

<br />
<br />
- {{ DisplayService::displayUser($comment->user) }}, {{ $comment->created_at }}
--}}