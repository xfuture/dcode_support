<html>
<head>
    <style>
        body {
            background: #f5f5f5;
            padding: 25px;
            text-align: center;
        }

        hr {
            height: 0;
            border: 0;
            border-top: .1px solid #bdbdbd;
            margin-bottom: 30px;
        }
        .description{
            float: right;
            color: black;
            font-size: 15px
        }
        .detail{
            float: left;
            color: #354052;
        }
        .view-ticker{
            text-align: center;
            padding: 40px 40px 0;
        }
        .ticket-bar{
            background-size: 100% 100%;
            background-image: url('{{ asset('assets/images/blue_bar.jpg') }}') !important;
            padding:15px;
            text-align:center;
            color: white;
        }
        .content{
            color: #b6b6b7;
            font-size:12px;
            padding:10px 60px 10px;
            text-align:left;
        }
        .footer{
            color:#b6b6b7;
            font-size:10px;
            padding:10px;
            text-align: center
        }
    </style>
</head>
<body style="font-family: 'Open Sans' !important;, sans-serif">
<table width="100%" height="100%" style="background:#f5f5f5; border-collapse: collapse" cellpadding="0" cellspacing="0">
    {{--Header content--}}
    <tr style="background: white">
        <td style="padding-left: 23%; padding-right: 23%">
            <img src="{{ asset('assets/images/dcode_blue.jpg') }}" style="float: left; padding: 20px 50px 15px;"
                 height="50px"/>
            <span style="float: right; padding: 38px 50px 10px;">Support Message</span>
        </td>
    </tr>

    {{--Body content--}}
    <tr>
        <td align="center" style="padding:25px 0; text-align:center;">
            <table align="center" cellpadding="0" cellspacing="0" width="600">
                <tbody>
                <tr class="ticket-bar">
                    <td style="height: 75px; font-size: 22px">Updated Ticket</td>
                </tr>
                <tr style="background-color: white">
                    <td colspan="2" class="content">
                        @yield('content')
                    </td>
                </tr>
                <tr>
                    <td>
                        <img src="{{ asset('assets/images/ticket_bg.jpg') }}" width="100%" height="80px" disabled="disable"/>
                    </td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>

    {{--Footer content--}}
    <tr>
        <td class="footer">
            <div style="margin: 0px 0px 20px">
                <img src="{{ asset('assets/images/dcode_grey.jpg') }}" height="35"/>
            </div>
            <div>This is an automated email from DCODE's Support System.</div>
            <div style="margin-top: 10px">Please do not respond to this message.</div>
        </td>
    </tr>
</table>
</body>
</html>