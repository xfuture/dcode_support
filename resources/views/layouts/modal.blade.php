<html>
<head>
</head>
<body class="modal">
<button class="close-button" data-close aria-label="Close modal" type="button">
	<span aria-hidden="true">&times;</span>
</button>

<header>
	@yield('header')
</header>

<section class="content">
	@yield('content')
</section>

<footer>
	@yield('footer')
</footer>

<script type="application/javascript">
	$(function() {
        $(".datepicker").datepicker({
            dateFormat: 'dd-mm-yy'
        });

        $(".datetimepicker").datetimepicker({
            timeFormat: 'HH:mm:ss',
		});
    });
</script>
@yield('javascript')
</body>
</html>