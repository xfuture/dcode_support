<!doctype html>
<html class="no-js" lang="en">
<head>
	<meta charset="utf-8"/>
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>

	<meta name="_token" content="{{ csrf_token() }}"/>

	<title>DCODE</title>

	<link rel="stylesheet" href="{{ elixir('css/admin.css') }}" />

	<script src="{{ elixir('js/app-start.js') }}"></script>
	
	<script type='text/javascript'>
		(function (d, t) {
			var bh = d.createElement(t), s = d.getElementsByTagName(t)[0];
			bh.type = 'text/javascript';
			bh.src = 'https://www.bugherd.com/sidebarv2.js?apikey=hvxlgbscayxjkzquxpqmjw';
			s.parentNode.insertBefore(bh, s);
		})(document, 'script');
	</script>
</head>
<body class="clean">
<?php /* @include('_partials.layout.googleAnalytics') */ ?>

@include('_partials.layout.flashNotice')

@if ($__env->yieldContent('content'))
	<section class="content">
		<?php /** {!! Breadcrumbs::renderIfExists() !!} **/ ?>

		@yield('content')
	</section>
@endif

<footer>
	Copyright &copy;2016 DCODE GROUP PTY LTD, All Rights Reserved.  Developed and managed by DCODE GROUP - Custom Web Application Developers.
</footer>

<div class="loading">
	<img src="{{ asset('img/loading.gif') }}" />
</div>

<script src="{{ elixir('js/app-stop.js') }}"></script>

@yield('javascript')
</body>
</html>