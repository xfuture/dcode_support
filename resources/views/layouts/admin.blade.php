<!doctype html>
<html class="no-js" lang="en">
<head>
    <meta charset="utf-8"/>
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

    <meta name="_token" content="{{ csrf_token() }}"/>

    <title>DCODE Support</title>

    <link rel="stylesheet" href="{{ elixir('css/admin.css') }}"/>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">

    <script src="{{ elixir('js/app-start.js') }}"></script>
    <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>

    <script type='text/javascript'>
        (function (d, t) {
            var bh = d.createElement(t), s = d.getElementsByTagName(t)[0];
            bh.type = 'text/javascript';
            bh.src = 'https://www.bugherd.com/sidebarv2.js?apikey=hvxlgbscayxjkzquxpqmjw';
            s.parentNode.insertBefore(bh, s);
        })(document, 'script');
    </script>
</head>
<body>
<?php /* @include('_partials.layout.googleAnalytics') */ ?>

@include('_partials.layout.flashNotice')

<header>
    <div class="top-bar row">
        <div class="top-bar-left columns">
            <ul class="menu">
                <li class="menu-text"><a href="/admin"><h1><img
                                    src="https://www.dcode.com.au/assets/images/dcode-logo-blue-notxt.png"/></h1></a>
                </li>

                @if (\Auth::user()->admin == '1')
                    <li><a class="{{ \Html::active('admin.client') }}" href="{{ route('admin.client') }}">Clients</a>
                    </li>
                @endif

                <li><a class="{{ \Html::active('admin.project') }}" href="{{ route('admin.project') }}">Projects</a>
                </li>
                <li><a class="{{ \Html::active('admin.task') }}" href="{{ route('admin.task') }}">Tasks</a></li>
                <li><a class="{{ \Html::active('admin.ticket') }}" href="{{ route('admin.ticket') }}">Tickets</a></li>

                @if (\Auth::user()->admin == '1')
                    <li><a class="{{ \Html::active('admin.idea') }}" href="{{ route('admin.idea') }}">Ideas</a></li>
                @endif

                <li><a class="{{ \Html::active('admin.timesheet') }}"
                       href="{{ route('admin.timesheet') }}">Timesheets</a></li>

                @if (\Auth::user()->admin == '1')
                    <li><a class="{{ \Html::active('admin.user') }}" href="{{ route('admin.user') }}">Users</a></li>
                @endif

                <li><a class="button edit" href="{{ route('admin.ticket.create') }}">+ Add Ticket</a></li>
                <li><a class="button edit" href="{{ route('admin.timesheet.create') }}">+ Add Timesheet</a></li>
            </ul>
        </div>

        <div class="top-bar-right columns">
            <ul class="menu">
                <li><a href="{{ route('account') }}">Account</a></li>
                <li><a href="/logout">Logout</a></li>
            </ul>
        </div>
    </div>

@if ($__env->yieldContent('header'))
    <!-- section class="header">
				@yield('header')
            </section -->
    @endif
</header>

@if ($__env->yieldContent('content'))
    <section class="contentContainer">
        <section class="controlPanel">
            <div>
                <header>
                    @yield('controlPanelHeader')
                </header>
                <section class="body">
                    @yield('controlPanelBody')
                </section>
            </div>
            @if(isset($projectMilestones) || isset($projectUpdates) || isset($panel2))
                <hr style="margin: 0; height: 15px; background-color: #f0f0f0;"/>

                <div>
                    <header>
                        @yield('controlPanelHeaderSub-1')
                    </header>
                    <section class="body">
                        @yield('controlPanelBodySub-1')
                    </section>
                </div>
            @endif
        </section>
        <section class="content">
            <?php /** {!! Breadcrumbs::renderIfExists() !!} **/ ?>

            @yield('content')
        </section>
    </section>
@endif

<footer>
    Copyright &copy;2016 DCODE GROUP PTY LTD, All Rights Reserved. Developed and managed by DCODE GROUP - Custom Web
    Application Developers.
</footer>

<div class="reveal" id="editModal" data-reveal data-overlay="true"></div>
<div class="xlarge reveal" id="ticketModal" data-reveal data-overlay="true"></div>

<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-body">
        <h4>Delete this Item?</h4>
        <p>When deleting this record, you are also deleting all data associated with this record. Are you sure that you
            want to delete this record?</p>
    </div>
    <div class="modal-footer">
        <button class="buttonAction" data-dismiss="modal" id="delete">Yes, Delete</button>
        <button class="buttonClear" data-dismiss="modal">No, Cancel</button>
    </div>
</div>

<div class="loading">
    <img src="{{ asset('img/loading.gif') }}"/>
</div>

<script src="{{ elixir('js/app-stop.js') }}"></script>

@yield('javascript')
</body>
</html>