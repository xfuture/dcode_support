@extends('layouts.account')

@section('content')
	<?php
	$user = \Auth::user();
	?>
	<div style="background: white; margin: 30px 50px; padding: 50px 50px 0;">
		<h3>My Account</h3>

		<div class="warnMessage"></div>
		<section class="main form">
			{!! Form::model($user,['route' => ['account.update',$user->id],'method'=>'POST','id'=>'updateUserForm']) !!}

			<fieldset>
				<legend>Reset Name</legend>
				<div style="padding: 15px">
					{!! Form::label('name','First Name') !!}
					{!! Form::text('first_name') !!}
					{!! Form::label('name','Last Name') !!}
					{!! Form::text('last_name') !!}
				</div>
			</fieldset>

			<fieldset>
				<legend>Reset Password</legend>

				<div style="padding: 15px">
					{!! Form::label('password', 'New Password') !!}
					{!! Form::password('password') !!}
				</div>

				<div class="formOptions">
					{!! Form::submit('Update Account') !!}
					&nbsp;
					<a class="buttonClear" href="{{ URL::previous() }}">Cancel</a>
				</div>
			</fieldset>
			{!! Form::close() !!}
		</section>
	</div>
@endsection