<div>
	{{ DisplayService::displayUser($user) }}

	@if (isset($projectUser))
		{!! \Html::delete('admin.project.cancel-access', $projectUser->id, 'Delete') !!}
	@else
		{!! Form::open(['route' => ['admin.project.user', $project->id]]) !!}
			{!! Form::hidden('userId', $user->id) !!}
			{!! Form::submit('+ Add') !!}
		{!! Form::close() !!}
	@endif
</div>