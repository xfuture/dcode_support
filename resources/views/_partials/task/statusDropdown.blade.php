<button class="statusSelect {{ strtolower(DisplayService::displayTicketStatus($ticket->currentStatus())) }}" type="button" data-close-on-click="true" data-toggle="status-dropdown-{{ $ticket->id }}" id="status-{{ $ticket->id }}">{{ DisplayService::displayTicketStatus($ticket->currentStatus()) }} <span class="fa fa-angle-down"></span></button>
<div class="dropdown-pane" id="status-dropdown-{{ $ticket->id }}" data-dropdown>
	<ul class="menu vertical">
		@foreach (\DCODESupport\Models\Status::orderBy('order', 'asc')->get() as $status)
			<li><a class="updateStatus" id="{{ $status->id }}" rel="{{ $ticket->id }}">{{ $status->name }}</a></li>
		@endforeach
	</ul>
</div>