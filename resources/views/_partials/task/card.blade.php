<div class="taskBoard__ticket" id="{{ $ticket->id }}">
	<div class="taskBoard__ticket--container">
		<header>
			{{ $ticket->name }}
		</header>
			{{ DisplayService::displayExcerpt($ticket->comment) }}
		{{--If it is a working ticket ,checks--}}
		@if ($ticket->workingTicket() != null)
			{{--Count down task time--}}
			<div>
				{{-- To check the DB stop value
			 	Start Time :{{ $ticket->workingTicket()}}<br />
			 	--}}
				Elapsed Time <div class="elapsed" data-val="{{ date("Y-m-d H:i:s", $ticket->workingTicket()) }}"></div>
			</div>
		@endif
		<footer>
			<div><span class="fa fa-building"></span> {{ DisplayService::displayClient($ticket->client) }}</div>
			<div><span class="fa fa-folder-o"></span> {{ DisplayService::displayProjectCode($ticket->project) }} {{ DisplayService::displayProject($ticket->project) }}</div>
			<div><span class="fa fa-calendar-o"></span> {!! ($ticket->date_due != null ? DisplayService::displayDate($ticket->date_due) : '<i>Not set</i>') !!}</div>
			<div><span class="fa fa-user"></span> {!! ($ticket->assignedUser != null ? DisplayService::displayUser($ticket->assignedUser) : '<i>Un-assigned</i>') !!}</div>
			<div>
				<span class="fa fa-clock-o"></span> Hours Spend : {{ $ticket->calculateTimeSpend() }} / {{ $ticket->estimated_time != null ? ($ticket->estimated_time) :'0 ' }} hr
			</div>
		</footer>
	</div>
</div>