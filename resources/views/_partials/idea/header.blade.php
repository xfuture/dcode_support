<div>
	<div style="color: white">{{ DisplayService::displayClient($idea->client) }} / {{ DisplayService::displayProjectCode($idea->project) }}: {{ DisplayService::displayProject($idea->project) }}</div>
	<h1>{{ $idea->name }}</h1>
</div>