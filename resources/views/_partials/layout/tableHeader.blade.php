@foreach ($columns as $column)
	@if ($column['sort'] === true)
		<th class="sort{{ (Request::get('sort') == $column['id'] ? (Request::get('sortDir') == 'asc' ? ' desc' : ' asc') : '') }}" width="{{ $column['width'] }}">
			<a class="sortButton" id="{{ $column['id'] }}" rel="{{ (Request::get('sortDir') == 'asc' ? 'desc' : 'asc') }}">
				{{ $column['value'] }}
			</a>
		</th>
	@else
		<th width="{{ $column['width'] }}">{{ $column['value'] }}</th>
	@endif
@endforeach

@if ($action !== null)
	<th class="action" colspan="{{ $action }}" width="20"><!-- --></th>
@endif