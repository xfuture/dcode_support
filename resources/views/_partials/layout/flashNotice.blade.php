@if(Session::has('flashNotice') || Session::has('flashNoticeError'))
	<section class="notice">
		@if(Session::has('flashNotice'))

			<div class="alert-box success" data-closable>
				{{ Session::get('flashNotice') }}
				<button class="close-button" aria-label="Dismiss alert" type="button" data-close>
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
		@endif

		@if(Session::has('flashNoticeError'))
				<div class="alert-box alert" data-closable>
					{{ Session::get('flashNoticeError') }}
					<button class="close-button" aria-label="Dismiss alert" type="button" data-close>
						<span aria-hidden="true">&times;</span>
					</button>
				</div>

		@endif
	</section>
@endif
