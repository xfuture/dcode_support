<div class="comment">
	{!! $comment->comment !!}

	<div class="comment__meta">
		{!! DisplayService::displayUser($comment->user) !!}
		&nbsp;
		<span class="fa fa-calendar-o"></span>
		{!! DisplayService::displayDate(strtotime($comment->created_at)) !!}
	</div>
</div>