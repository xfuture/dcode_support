<div>
    @if (\Auth::user()->internal == '1')
        {!! Form::checkbox('internal', '1') !!}
        {!! Form::label('Internal to DCODE') !!}
    @endif
	{!! Form::textarea('comment') !!}
</div>

<div>
	{!! Form::hidden('internal', '0') !!}

	{!! Form::submit('Save Comment') !!}
</div>