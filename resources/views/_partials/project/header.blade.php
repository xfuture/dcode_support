<div>
	<div style="color: white">{{ DisplayService::displayClient($project->client) }} / {{ DisplayService::displayProjectCode($project) }}: {{ DisplayService::displayProject($project) }}</div>
	<h1>{{ $project->name }}</h1>
</div>