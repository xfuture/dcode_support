$(document).ready(function() {
	$(document).foundation();
});

/* AJAX */
$.ajaxSetup({
	headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') },
	beforeSend: function() { $('.loading').show(); },
	complete: function() { $('.loading').hide(); }
});

/* Date Picker */
// $(function() {
// 	$(".datepicker").datepicker({
// 		dateFormat: 'dd-mm-yy'
// 	});
// });

/* Delete */
$('.confirm').on('click', function(e){
	var $form = $(this).closest('form');
	e.preventDefault();

	$('#deleteModal').modal({ backdrop: 'static', keyboard: false })
		.one('click', '#delete', function (e) {
			$form.trigger('submit');
		});
});

/* Edit */
$('.edit').click(function(e) {
	e.preventDefault();

	var $modal = $('#editModal');
	var url = $(this).attr('data-href');

	if (typeof url === "undefined") {
		url = $(this).attr('href');
	}

    //Get data through the modal
    var prevData = $(this).data('store');
    $.ajax({
        type: 'GET',
        url: url,
        data: prevData,
        success : function(data) {
            $modal.html(data).foundation('open');
        },
        async:true
    });
});

/* Notice */
$(document).ready(function(){
	setTimeout(function(){
		$('.notice').slideUp();
	}, 4000);
});

/* Select */
/*
$(document).ready(function(){
	$('.select2').select2();

	$('.select2-tags').select2({
		tags: true
	})
});
*/

/* Status */
$('.updateStatus').click(function(e){
	e.preventDefault();

	$.ajax({
		type: "POST",
		url : "/admin/ticket/status",
		data: { ticketId: $(this).attr('rel'), statusId: $(this).attr('id') },
		success : function(data){
			$('#status-'+data.id).html(data.currentStatus + '<span class="fa fa-angle-down"></span>').removeClass().addClass('statusSelect').addClass(data.currentStatus.toLowerCase());
			$('#status-dropdown-'+data.id).foundation('close');
		},
		async:true
	});
});

/* Table Export */
$('.exportButton').click(function(){
	$('input[name=export]').val(1);

	$('form#searchForm').submit();

	$('input[name=export]').val(0);
});

/* Table Sort */
$('.sortButton').click(function(){
	$('input[name=export]').val(0);

	var sort    = $(this).attr('id');
	var sortDir = $(this).attr('rel');
	$('input[name=sort]').val(sort);
	$('input[name=sortDir]').val(sortDir);

    $('form#searchForm').submit();
});
/* Vote button */
var VoteWidget= {
    settings: {
        $btn:     $('.vote-btn')
    },
    init: function() {
        VoteWidget.bind();
    },
    bind: function() {
        VoteWidget.settings.$btn.click(function(){
            VoteWidget.vote($(this));
            $(this).toggleClass('complete');
            // VoteWidget.toggleText($(this));

            return false;
        });
    },
    vote: function(btn) {
        $.ajax({
            type: 'POST',
            url: btn.attr('data-href-vote'),
            data: {idea_id : btn.attr('data-ideaId')},
            dataType: 'JSON',
            success: function(data) {
                var count = parseInt(data);
                btn.children("span.vote-count").html(data);
            }
        });
    }
    // decreaseCount: function(btn) {
    //     $.ajax({
    //         type: 'POST',
    //         url: $(this).attr('href-unvote'),
    //         data: {idea_id : btn.attr('data-ideaId')},
    //         success: function(html) {
    //             var current_count = btn.children("span.vote-count").html();
    //             var count = parseInt(current_count);
    //             count--;
    //             btn.children("span.vote-count").html(count);
    //         }
    //     });
    // }
    // toggleText: function(btn){
    //     var alt_text = btn.data('alt-text');
    //     var default_text = btn.data('default-text');
    //
    //     var $text_container = btn.children("span.vote-text");
    //     var current_text = $text_container.html();
    //     if (current_text == default_text) {
    //         $text_container.html(alt_text)
    //     } else {
    //         $text_container.html(default_text)
    //     }
    // }
}
VoteWidget.init();

/* Bind Ajax Submit for Modal */
function bindErrorAjaxModal(formId, url, method){
    //Get form from modal
    // formId = '#editModal ' +  formId;
    //Submit form by ajax
    $(formId).submit(function(e){
        $.ajax({
            type: method,
            url: url,
            data: $(formId).serialize(),
            dataType: 'JSON',
            success : function(data){
                location.href = location.href;
            },
            error: function(data){
                //Remove all old error messages
                $('.validateErrorMessage').remove();
                //Print all errors of validation
                var errors = data.responseJSON;
                var content = '';
                //Read all errors
                for (var prop in errors) {
                    if (errors.hasOwnProperty(prop)) {
                        $('.warnMessage').html('Sorry, we could not save your changes.  Please correct your errors below.');
                        $('<span class="validateErrorMessage"> *' + errors[prop][0] + '</span>').insertAfter($(formId + ' #' + prop));
                    }
                }
                //Show errors
                $('#validationErrors').html(content);
            },
            async:true
        });

        e.preventDefault();
    })
}
/* Generate project code when client change  */
function generateProjectCode(formId, clientId, projectCodeId, url, method){
    //Get form from modal
    clientId = formId + ' ' + clientId;
    projectCodeId = formId + ' ' + projectCodeId;
    //Using ajax to get project cpde
    $(clientId).change(function(e){
        $.ajax({
            type: method,
            url: url,
            data: {clientId : $(clientId).val()},
            dataType: 'JSON',
            success : function(data){
                $(projectCodeId).val(data);
            },
            async:true
        });
    })
}
/* Dropzone */
function bindFileUploadForm(formId, param, url){
    $(formId).dropzone({
        uploadMultiple: false,
        parallelUploads: 100,
        maxFilesize: 8,
        paramName: 'uploadFile',
        dictFileTooBig: 'Image is bigger than 8MB',
        dictDefaultMessage  :  "Drop File here or Click to upload Image",
        thumbnailWidth:'100',
        thumbnailHeight:'100',
        init: function() {
            this.on("sending", function(file, xhr, data) {
                $.each(param, function(key, value) {
                    data.append(key, value);
                });
            });
            this.on("success", function (file, response) {
                var assetLink = url + response.file.hash_name + '/' + response.file.original_name;
                var fileLink = '<a style=" color: #1baad2; text-decoration: underline;" href="' + response.signedUrl + '">' + response.file.original_name +'</a>';
                var delLink = '<a class="dicon-delete" style="padding: 10px"></a>';
                $('#listUpload').append('<li>' + fileLink + '</li>' +
                                        '<a class="dicon-delete" style="padding: 10px"></a>');
                this.removeAllFiles();
            });
        }
    });
}
/* Clear button for the searchFrom id */
$('#searchForm a.buttonClear').on('click', function(e){
    e.preventDefault();
    $('#searchForm :input:visible').val('');
    $('#searchForm').submit();
});
/*Change tab event foundation tabs*/


